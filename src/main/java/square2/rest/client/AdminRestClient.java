package square2.rest.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import square2.modules.model.ProfileBean;

/**
 * 
 * @author henkej
 *
 */
public class AdminRestClient {
  private static final Logger LOGGER = LogManager.getLogger(AdminRestClient.class);

  private final String uri;

  /**
   * @param uri the REST uri; this is the basic URL and will be extended by the methods
   */
  public AdminRestClient(String uri) {
    this.uri = uri;
  }

  /**
   * get profile from rest
   * 
   * @param username the username of the corresponding profile
   * @return the profile bean or null
   */
  public ProfileBean getProfile(String username) {
    // TODO: check for npe
    Client client = ClientBuilder.newClient();
    WebTarget target = client.target(uri).path("t_profile").queryParam("username", username);
    LOGGER.debug(target.getUri());
    Builder builder = target.request(MediaType.APPLICATION_JSON);
    ProfileBean bean = builder.get(ProfileBean.class);
    return bean;
  }
}
