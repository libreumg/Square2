package square2.json;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class FunctionFlagBean implements Serializable, Comparable<FunctionFlagBean> {
	private static final long serialVersionUID = 1L;

	private String name;
	private Boolean value;

	/**
	 * @param name the name
	 * @param value the value
	 */
	public FunctionFlagBean(String name, Boolean value) {
		super();
		this.name = name;
		this.value = value;
	}

	public String toString() {
		StringBuilder buf = new StringBuilder(name == null ? "" : name);
		buf.append(":").append(value);
		return buf.toString();
	}

	@Override
	public int compareTo(FunctionFlagBean o) {
		return name == null || o == null || o.getName() == null ? 0 : name.compareTo(o.getName());
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the value
	 */
	public Boolean getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(Boolean value) {
		this.value = value;
	}
}
