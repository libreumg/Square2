package square2.db.cache.vargroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import square2.modules.model.study.ListElement;
import square2.modules.model.vargroup.VargroupElementBean;
import square2.modules.model.vargroup.VargroupListElement;

/**
 * 
 * @author henkej
 *
 */
public class VargroupElementCache {
	private final List<VargroupElementBean> content;
	private Integer lastHierarchyPosition;
	private List<ListElement> ancestors;
	private List<VargroupListElement> nonVariableChildren;
	private List<ListElement> variableChildren;

	/**
	 * generate new variable group cache
	 * 
	 * @param content
	 *          of the cache
	 */
	public VargroupElementCache(List<VargroupElementBean> content) {
		this.content = content;
		this.lastHierarchyPosition = -1; // hack to make first loop run only once
	}

	/**
	 * update possibly dirty variables; replace chosen and dirty flag of them
	 * 
	 * @param possibleDirtyVariables
	 *          the possible dirty variables
	 */
	public void updatePossiblyDirtyVariables(List<ListElement> possibleDirtyVariables) {
		if (possibleDirtyVariables != null) {
			Map<Integer, Boolean> dirtyMap = new HashMap<>();
			for (ListElement element : possibleDirtyVariables) {
				if (element.getDirty()) {
					dirtyMap.put(element.getFkElement(), element.getChosen());
				}
			}
			for (VargroupElementBean bean : content) {
				if (dirtyMap.containsKey(bean.getKeyElement())) {
					bean.setChosen(dirtyMap.get(bean.getKeyElement()));
				}
			}
		}
	}

	/**
	 * count how many elements are under this tree that are not variables (field
	 * 0)<br>
	 * count how many elements are under this tree that are variables (field 1)<br>
	 * count how many chosen variables are under this tree (field 2) *
	 * 
	 * @param keyElement
	 *          reference point
	 * @return array of counts
	 */
	protected int[] getAmounts(Integer keyElement) {
		Set<Integer> found = new HashSet<>();
		found.add(keyElement);
		boolean redo = true;
		while (redo) {
			Iterator<VargroupElementBean> iterator = content.iterator();
			redo = false;
			while (iterator.hasNext()) {
				VargroupElementBean bean = iterator.next();
				if (found.contains(bean.getKeyElement())) {
					// still in list, loop on
				} else if (found.contains(bean.getKeyParentElement())) {
					found.add(bean.getKeyElement());
					redo = true;
				}
			}
		}
		int[] amounts = new int[] { -1, 0, 0 }; // -1 for self reference, otherwise, it's one too much
		for (VargroupElementBean bean : content) {
			if (found.contains(bean.getKeyElement())) {
				if (bean.getIsVariable()) {
					amounts[1] = amounts[1] + 1;
					if (bean.getChosen()) {
						amounts[2] = amounts[2] + 1;
					}
				} else {
					amounts[0] = amounts[0] + 1;
				}
			}
		}
		return amounts;
	}

	private void generateLists(Integer hierarchyPosition) {
		if (hierarchyPosition == null && lastHierarchyPosition == null) {
			// do nothing, this means equal
		} else if (hierarchyPosition == null && lastHierarchyPosition != null) {
			lastHierarchyPosition = hierarchyPosition;
			ancestors = new ArrayList<>();
			nonVariableChildren = new ArrayList<>();
			variableChildren = new ArrayList<>();
			for (VargroupElementBean bean : content) {
				if (bean.getKeyParentElement() == null && bean.getIsVariable()) {
					variableChildren.add(new ListElement(bean.getKeyElement(), bean.getTranslation(), bean.getName(),
							bean.getChosen(), bean.getDirty()));
				}
				if (bean.getKeyParentElement() == null && !bean.getIsVariable()) {
					int[] amounts = getAmounts(bean.getKeyElement());
					nonVariableChildren.add(new VargroupListElement(bean.getKeyElement(), bean.getTranslation(), bean.getName(),
							bean.getChosen(), bean.getDirty(), amounts[0], amounts[1], amounts[2]));
				}
			}
		} else if (!hierarchyPosition.equals(lastHierarchyPosition)) {
			lastHierarchyPosition = hierarchyPosition;
			ancestors = new ArrayList<>();
			nonVariableChildren = new ArrayList<>();
			variableChildren = new ArrayList<>();
			Set<Integer> found = new HashSet<>();
			for (VargroupElementBean bean : content) {
				if (bean.getKeyElement().equals(hierarchyPosition)) {
					found.add(bean.getKeyParentElement());
				}
			}
			content.sort((o1, o2) -> o1 == null ? 0 : o2.compareTo(o1));
			for (VargroupElementBean bean : content) {
				if (hierarchyPosition.equals(bean.getKeyParentElement()) && bean.getIsVariable()) {
					variableChildren.add(new ListElement(bean.getKeyElement(), bean.getTranslation(), bean.getName(),
							bean.getChosen(), bean.getDirty()));
				}
				if (hierarchyPosition.equals(bean.getKeyParentElement()) && !bean.getIsVariable()) {
					int[] amounts = getAmounts(bean.getKeyElement());
					nonVariableChildren.add(new VargroupListElement(bean.getKeyElement(), bean.getTranslation(), bean.getName(),
							bean.getChosen(), bean.getDirty(), amounts[0], amounts[1], amounts[2]));
				}
				if (found.contains(bean.getKeyElement())) {
					ancestors.add(
							new ListElement(bean.getKeyElement(), bean.getTranslation(), bean.getName(), bean.getChosen(), false));
					if (bean.getKeyParentElement() != null) {
						found.add(bean.getKeyParentElement());
					}
				} else if (bean.getKeyElement().equals(hierarchyPosition)) {
					ancestors.add(
							new ListElement(bean.getKeyElement(), bean.getTranslation(), bean.getName(), bean.getChosen(), false));
				}
			}
			ancestors.sort((o1, o2) -> o1 == null ? 0 : o1.compareByFkElementTo(o2));
		}
	}

	/**
	 * get all ancestors in correct order from hierarchyPosition
	 * 
	 * @param hierarchyPosition
	 *          position in hierarchy of where to seek from
	 * @return list of ancestors
	 */
	public List<ListElement> getAncestors(Integer hierarchyPosition) {
		generateLists(hierarchyPosition);
		return ancestors;
	}

	/**
	 * get all children of hierarchyPosition that are not variables
	 * 
	 * @param hierarchyPosition
	 *          position in hierarchy of where to seek from
	 * @return list of children that are not variables
	 */
	public List<VargroupListElement> getNonVariableChildren(Integer hierarchyPosition) {
		generateLists(hierarchyPosition);
		return nonVariableChildren;
	}

	/**
	 * get all children of hierarchyPosition that are variables
	 * 
	 * @param hierarchyPosition
	 *          position in hierarchy of where to seek from
	 * @return list of children that are variables
	 */
	public List<ListElement> getVariableChildren(Integer hierarchyPosition) {
		generateLists(hierarchyPosition);
		return variableChildren;
	}

	/**
	 * get all dirty variables; dirty variables are such that have a different state
	 * from the database than in the cache
	 * 
	 * @return list of dirty variables
	 */
	public List<VargroupElementBean> getAllDirtyVariables() {
		List<VargroupElementBean> list = new ArrayList<>();
		for (VargroupElementBean bean : content) {
			if (bean.isDirty()) {
				list.add(bean);
			}
		}
		return list;
	}

	/**
	 * add element to section
	 * 
	 * @param elementId
	 *          id of the element
	 */
	public void addToSelection(Integer elementId) {
		for (VargroupElementBean bean : content) {
			if (bean.getKeyElement().equals(elementId)) {
				bean.setChosen(true);
			}
		}
		for (ListElement bean : variableChildren) {
			if (bean.getFkElement().equals(elementId)) {
				bean.setChosen(true);
			}
		}
	}

	/**
	 * remove element from section
	 * 
	 * @param elementId
	 *          id of the element
	 */
	public void removeFromSelection(Integer elementId) {
		for (VargroupElementBean bean : content) {
			if (bean.getKeyElement().equals(elementId)) {
				bean.setChosen(false);
			}
		}
		for (ListElement bean : variableChildren) {
			if (bean.getFkElement().equals(elementId)) {
				bean.setChosen(false);
			}
		}
	}

	/**
	 * set all children (and their children) variables to chosen
	 * 
	 * @param elementId
	 *          the id of the element
	 * @param chosen
	 *          the flag to be chosen
	 */
	public void setChildrenVariables(Integer elementId, boolean chosen) {
		Set<Integer> found = new HashSet<>();
		found.add(elementId);
		boolean redo = true;
		while (redo) {
			Iterator<VargroupElementBean> iterator = content.iterator();
			redo = false;
			while (iterator.hasNext()) {
				VargroupElementBean bean = iterator.next();
				if (found.contains(bean.getKeyElement())) {
					// still in list, loop on
				} else if (found.contains(bean.getKeyParentElement())) {
					found.add(bean.getKeyElement());
					redo = true;
				}
			}
		}
		int[] amounts = new int[] { -1, 0, 0 }; // -1 for self reference, otherwise, it's one too much
		for (VargroupElementBean bean : content) {
			if (found.contains(bean.getKeyElement()) && bean.getIsVariable()) {
				amounts[1] = amounts[1] + 1;
				if (!bean.getChosen().equals(chosen)) {
					bean.setChosen(chosen);
				}
			}
		}
	}
}
