package square2.db.control.model;

/**
 * 
 * @author henkej
 *
 */
public class TableColumnBean {
	private final String tableName;
	private final String columnName;
	private final String dataType;
	private final Integer rowcount;

	/**
	 * generate new table column bean
	 * 
	 * @param tableName
	 *          name of the table
	 * @param columnName
	 *          name of the column
	 * @param dataType
	 *          datatype of the column
	 * @param rowcount
	 *          the row count
	 */
	public TableColumnBean(String tableName, String columnName, String dataType, Integer rowcount) {
		this.tableName = tableName;
		this.columnName = columnName;
		this.dataType = dataType;
		this.rowcount = rowcount;
	}

	/**
	 * get the table name
	 * 
	 * @return table name
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * get the column name
	 * 
	 * @return column name
	 */
	public String getColumnName() {
		return columnName;
	}

	/**
	 * get the data type
	 * 
	 * @return data type
	 */
	public String getDataType() {
		return dataType;
	}

	/**
	 * get the row count
	 * 
	 * @return the row count
	 */
	public Integer getRowcount() {
		return rowcount;
	}
}
