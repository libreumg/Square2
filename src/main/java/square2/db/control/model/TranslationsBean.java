package square2.db.control.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author henkej
 *
 */
public class TranslationsBean {
	private Map<String, Map<String, String>> isocodes;

	/**
	 * generate a new translations bean
	 * 
	 * @param locales
	 *          set of all available locales
	 */
	public TranslationsBean(Set<String> locales) {
		isocodes = new HashMap<>();
		for (String locale : locales) {
			isocodes.put(locale, new HashMap<>());
		}
	}

	/**
	 * get complete map of isocode
	 * 
	 * @param isocode
	 *          code to be used
	 * @return map of translations for this isocode or null if isocode is not found
	 */
	public Map<String, String> get(String isocode) {
		return isocodes.get(isocode);
	}

}
