package square2.db.control.model;

/**
 * 
 * @author henkej
 *
 */
public class LambdaResultWrapper {
	private Integer integer;
	private String string;
	private Boolean bool;

	/**
	 * set the integer value
	 * 
	 * @param amount
	 *          the value
	 */
	public void setInteger(Integer amount) {
		this.integer = amount;
	}

	/**
	 * get the integer value
	 * 
	 * @return the value
	 */
	public Integer getInteger() {
		return integer;
	}

	/**
	 * get the string value
	 * 
	 * @return the value
	 */
	public String getString() {
		return string;
	}

	/**
	 * set the string value
	 * 
	 * @param string
	 *          the value
	 */
	public void setString(String string) {
		this.string = string;
	}

	/**
	 * add amount to the integer
	 * 
	 * @param amount
	 *          value to be added to the integer variable
	 */
	public void addToInteger(int amount) {
		this.integer = integer == null ? amount : integer + amount;
	}

	/**
	 * @return the bool
	 */
	public Boolean getBool() {
		return bool;
	}

	/**
	 * @param bool
	 *          the bool
	 */
	public void setBool(Boolean bool) {
		this.bool = bool;
	}
}
