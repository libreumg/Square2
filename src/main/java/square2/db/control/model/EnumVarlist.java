package square2.db.control.model;

import org.jooq.exception.DataAccessException;

/**
 * 
 * @author henkej
 *
 */
public enum EnumVarlist {
	/**
	 * for true in the database
	 */
	TRUE(true, "true"),
	/**
	 * for false in the database
	 */
	FALSE(false, "false"),
	/**
	 * for null in the database
	 */
	NULL(null, "empty");

	private EnumVarlist(Boolean dbValue, String jsfValue) {
		this.dbValue = dbValue;
		this.jsfValue = jsfValue;
	}

	private final Boolean dbValue;
	private final String jsfValue;

	/**
	 * return enum from jsf value
	 * 
	 * @param value
	 *          value to be looked up
	 * @return the enum if found, null otherwise

	 *           if anything went wrong on database side
	 */
	public EnumVarlist fromJsf(String value) {
		if (value == null) {
			return EnumVarlist.NULL;
		} else if (value.trim().equals("")) {
			return EnumVarlist.NULL;
		} else if (EnumVarlist.NULL.getJsfValue().equals(value)) {
			return EnumVarlist.NULL;
		} else if (EnumVarlist.TRUE.jsfValue.equals(value)) {
			return EnumVarlist.TRUE;
		} else if (EnumVarlist.FALSE.jsfValue.equals(value)) {
			return EnumVarlist.FALSE;
		} else {
			throw new DataAccessException("no such enum found: " + value);
		}
	}

	/**
	 * return enum from db value
	 * 
	 * @param value
	 *          value to be looked up
	 * @return the enum if found, null otherwise
	 */
	public EnumVarlist fromDb(Boolean value) {
		if (value == null) {
			return EnumVarlist.NULL;
		} else if (value) {
			return EnumVarlist.TRUE;
		} else {
			return EnumVarlist.FALSE;
		}
	}

	/**
	 * @return the dbValue
	 */
	public Boolean getDbValue() {
		return dbValue;
	}

	/**
	 * @return the jsfValue
	 */
	public String getJsfValue() {
		return jsfValue;
	}
}
