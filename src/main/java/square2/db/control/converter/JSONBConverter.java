package square2.db.control.converter;

import java.util.List;

import org.jooq.tools.json.JSONValue;

/**
 * 
 * @author henkej
 *
 */
public class JSONBConverter {

  /**
   * check if value can be converted to JSONB
   * 
   * @param exceptions list for exceptions
   * @param value the value
   * @return true or false
   */
  public static boolean validateJSONB(List<Exception> exceptions, String value) {
    if (value == null) {
      return true;
    }
    try {
      JSONValue.parseWithException(value);
      return true;
    } catch (Exception e) {
      exceptions.add(e);
      return false;
    }
  }
}
