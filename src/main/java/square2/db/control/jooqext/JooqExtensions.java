package square2.db.control.jooqext;

import org.jooq.TableField;
import org.jooq.UpdateSetFirstStep;
import org.jooq.UpdateSetMoreStep;

import com.google.gson.JsonElement;

import de.ship.dbppsquare.square.enums.EnumControlvar;
import de.ship.dbppsquare.square.tables.records.TVariableRecord;

/**
 * 
 * @author henkej
 *
 */
public class JooqExtensions {

	/**
	 * add set tableField = value if addIt is true, set tableField = tableField else
	 * 
	 * @param ctx
	 *          the jooq context
	 * @param addIt
	 *          the condition
	 * @param fK_SCALE
	 *          the table field
	 * @param fkScale
	 *          the value
	 * @return the next jooq
	 */
	public UpdateSetMoreStep<?> setIf(UpdateSetFirstStep<?> ctx, Boolean addIt,
			TableField<TVariableRecord, Integer> fK_SCALE, Integer fkScale) {
		if (addIt) {
			return ctx.set(fK_SCALE, fkScale);
		} else {
			return ctx.set(fK_SCALE, fK_SCALE);
		}
	}

	/**
	 * add set tableField = value if addIt is true, set tableField = tableField else
	 * 
	 * @param ctx
	 *          the jooq context
	 * @param addIt
	 *          the condition
	 * @param field
	 *          the table field
	 * @param value
	 *          the value
	 * @return the next jooq
	 */
	public UpdateSetMoreStep<?> setIf(UpdateSetMoreStep<?> ctx, Boolean addIt,
			TableField<TVariableRecord, EnumControlvar> field, EnumControlvar value) {
		if (addIt) {
			return ctx.set(field, value);
		} else {
			return ctx.set(field, field);
		}
	}

	/**
	 * add set tableField = value if addIt is true, set tableField = tableField else
	 * 
	 * @param ctx
	 *          the jooq context
	 * @param addIt
	 *          the condition
	 * @param field
	 *          the table field
	 * @param value
	 *          the value
	 * @return the next jooq
	 */
	public UpdateSetMoreStep<?> setIf(UpdateSetMoreStep<?> ctx, Boolean addIt,
			TableField<TVariableRecord, JsonElement> field, JsonElement value) {
		if (addIt) {
			return ctx.set(field, value);
		} else {
			return ctx.set(field, field);
		}
	}

	/**
	 * add set tableField = value if addIt is true, set tableField = tableField else
	 * 
	 * @param ctx
	 *          the jooq context
	 * @param addIt
	 *          the condition
	 * @param field
	 *          the table field
	 * @param value
	 *          the value
	 * @return the next jooq
	 */
	public UpdateSetMoreStep<?> setIf(UpdateSetMoreStep<?> ctx, Boolean addIt, TableField<TVariableRecord, Integer> field,
			Integer value) {
		if (addIt) {
			return ctx.set(field, value);
		} else {
			return ctx.set(field, field);
		}
	}

	/**
	 * add set tableField = value if addIt is true, set tableField = tableField else
	 * 
	 * @param ctx
	 *          the jooq context
	 * @param addIt
	 *          the condition
	 * @param field
	 *          the table field
	 * @param value
	 *          the value
	 * @return the next jooq
	 */
	public UpdateSetMoreStep<?> setIf(UpdateSetMoreStep<?> ctx, Boolean addIt, TableField<TVariableRecord, String> field,
			String value) {
		if (addIt) {
			return ctx.set(field, value);
		} else {
			return ctx.set(field, field);
		}
	}

	/**
	 * add set tableField = value if addIt is true, set tableField = tableField else
	 * 
	 * @param ctx
	 *          the jooq context
	 * @param addIt
	 *          the condition
	 * @param field
	 *          the table field
	 * @param value
	 *          the value
	 * @return the next jooq
	 */
	public UpdateSetMoreStep<?> setIf(UpdateSetMoreStep<?> ctx, Boolean addIt, TableField<TVariableRecord, Boolean> field,
			Boolean value) {
		if (addIt) {
			return ctx.set(field, value);
		} else {
			return ctx.set(field, field);
		}
	}

}
