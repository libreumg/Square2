package square2.db.control;

import static de.ship.dbppsquare.square.Tables.T_ELEMENTSHIP;
import static de.ship.dbppsquare.square.Tables.T_GROUPPRIVILEGE;
import static de.ship.dbppsquare.square.Tables.T_LANGUAGEKEY;
import static de.ship.dbppsquare.square.Tables.T_PERSON;
import static de.ship.dbppsquare.square.Tables.T_PRIVILEGE;
import static de.ship.dbppsquare.square.Tables.T_RELEASE;
import static de.ship.dbppsquare.square.Tables.T_TRANSLATION;
import static de.ship.dbppsquare.square.Tables.T_USERPRIVILEGE;
import static de.ship.dbppsquare.square.Tables.V_GROUP;
import static de.ship.dbppsquare.square.Tables.V_LOGIN;
import static de.ship.dbppsquare.square.Tables.V_PRIVILEGE;
import static de.ship.dbppsquare.square.Tables.V_PRIVILEGES;
import static de.ship.dbppsquare.square.Tables.V_STUDY;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.CloseableDSLContext;
import org.jooq.DSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.DeleteWhereStep;
import org.jooq.Field;
import org.jooq.InsertOnDuplicateSetMoreStep;
import org.jooq.InsertResultStep;
import org.jooq.JSON;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Record4;
import org.jooq.Record5;
import org.jooq.Record7;
import org.jooq.Schema;
import org.jooq.SelectConditionStep;
import org.jooq.SelectJoinStep;
import org.jooq.SelectOnConditionStep;
import org.jooq.SelectOrderByStep;
import org.jooq.SelectSeekStep1;
import org.jooq.Table;
import org.jooq.UpdateConditionStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import de.ship.dbppsquare.square.enums.EnumAccesslevel;
import de.ship.dbppsquare.square.enums.EnumIsocode;
import de.ship.dbppsquare.square.tables.records.TGroupprivilegeRecord;
import de.ship.dbppsquare.square.tables.records.TLanguagekeyRecord;
import de.ship.dbppsquare.square.tables.records.TPrivilegeRecord;
import de.ship.dbppsquare.square.tables.records.TTranslationRecord;
import de.ship.dbppsquare.square.tables.records.TUserprivilegeRecord;
import square2.db.control.converter.EnumConverter;
import square2.db.control.model.LambdaResultWrapper;
import square2.db.control.model.TableColumnBean;
import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.GroupPrivilegeBean;
import square2.modules.model.ProfileBean;
import square2.modules.model.UserApplRightBean;
import square2.modules.model.admin.UserBean;
import square2.modules.model.admin.UsergroupBean;

/**
 * 
 * @author henkej
 *
 */
public abstract class JooqGateway {
	private static final Logger LOGGER = LogManager.getLogger(JooqGateway.class);

	private final SquareFacesContext facesContext;

	private Map<Integer, ProfileBean> cacheAllUsers;

	/**
	 * create a new jooq gateway. This class is abstract and should only be extended
	 * by other classes.
	 * 
	 * @param facesContext the context of this gateway
	 */
	public JooqGateway(SquareFacesContext facesContext) {
		this.facesContext = facesContext;
	}

	/**
	 * @return the faces context
	 */
	public SquareFacesContext getFacesContext() {
		return facesContext;
	}

	/**
	 * get right map
	 * 
	 * @return the privilege map
	 * @throws Exception if an error occurs
	 */
	public abstract Map<Integer, Map<Integer, AclBean>> getRightMap() throws Exception;

	/**
	 * get jooq context
	 * 
	 * @return jooq context
	 * @throws SQLException on sql errors
	 * @throws ClassNotFoundException on driver errors
	 */
	public CloseableDSLContext getJooq() throws ClassNotFoundException, SQLException {
		try {
			return facesContext.getJooq();
		} catch (Exception e) { // mask the standard error message that contains the connection string with the
														// password
			if (e.getMessage().startsWith("No suitable driver found for")) {
				LOGGER.error("postgresql library not found");
			} else {
				LOGGER.error(e.getMessage(), e);
			}
			throw new SQLException(
					"There is an error in your deployment; please inform the administrator about it, best with the current timestamp.");
		}
	}

	/**
	 * convert json string of type [{"usnr": 123, "acl": "rwx"}, ...] to map
	 * 
	 * @param json string with json content
	 * @return map from json content
	 */
	public Map<Integer, AclBean> getJsonMapFromString(JsonElement json) {
		if (json == null) {
			return new HashMap<>();
		}
		Map<Integer, AclBean> result = new HashMap<>();
		Iterator<JsonElement> i = json.getAsJsonArray().iterator();
		while (i.hasNext()) {
			JsonObject map = i.next().getAsJsonObject();
			Double usnr = map.get("usnr").getAsDouble();
			result.put(usnr.intValue(), new AclBean(map.get("acl").getAsString()));
		}
		return result;
	}

	/**
	 * get the unique name of the study for this element
	 * 
	 * @param jooq the dsl context
	 * @param id the id of the element
	 * @return unique name of the study element
	 */
	public String getStudyUniqueName(DSLContext jooq, Integer id) {
		SelectConditionStep<Record1<String>> sql = jooq
		// @formatter:off
			.select(V_STUDY.STUDY_UNIQUE_NAME)
			.from(V_STUDY)
			.where(V_STUDY.KEY_ELEMENT.eq(id));
		// @formatter:off
		LOGGER.debug("{}", sql.toString());
		return sql.fetchOne().get(V_STUDY.STUDY_UNIQUE_NAME);
	}

	/**
	 * get the unique name of the study for this element
	 * 
	 * @param jooq
	 *          the dsl context
	 * @param keyElement
	 *          the id of the element
	 * @return the unique name of the study
	 */
	public String getStudyUniqueNameOfElement(DSLContext jooq, Integer keyElement) {
		SelectConditionStep<Record1<String>> sql = jooq
		// @formatter:off
			.select(V_STUDY.STUDY_UNIQUE_NAME)
			.from(V_STUDY)
			.where(V_STUDY.KEY_ELEMENT.eq(keyElement));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		Record1<String> rec = sql.fetchOne();
		if (rec == null) {
			LOGGER.error("no result for getting the uniqueName of element {}; query was: {}", keyElement, sql.toString());
			throw new DataAccessException(getFacesContext().translate("error.study.getuniquename", keyElement));
		} else {
			return rec.get(V_STUDY.STUDY_UNIQUE_NAME);
		}
	}

	/**
	 * get the dn of the element if available, null otherwise
	 * 
	 * @param jooq the dsl context
	 * @param fkElement the id of the element
	 * @return dn of the element
	 */
	public String getDnOfElement(DSLContext jooq, Integer fkElement) {
		SelectConditionStep<Record1<String>> sql = jooq
		// @formatter:off
			.select(T_ELEMENTSHIP.DN)
			.from(T_ELEMENTSHIP)
			.where(T_ELEMENTSHIP.FK_ELEMENT.eq(fkElement));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		Record r = sql.fetchOne();
		return r != null ? r.get(T_ELEMENTSHIP.DN) : null;
	}

	/**
	 * get all users from db
	 * 
	 * @return a map of all users, indexed by the usnr
	 */
	public Map<Integer, ProfileBean> getAllUsers() {
		if (cacheAllUsers == null) {
			cacheAllUsers = new HashMap<>();
			try (SelectJoinStep<Record4<String, String, String, Integer>> sql = getJooq()
			// @formatter:off
				  .select(T_PERSON.FORENAME, 
				  				T_PERSON.SURNAME, 
				  				T_PERSON.USERNAME, 
				  				T_PERSON.USNR)
				  .from(T_PERSON);
				// @formatter:on
			) {
				LOGGER.debug("{}", sql.toString());
				for (Record r : sql.fetch()) {
					ProfileBean bean = new ProfileBean();
					bean.setForename(r.get(T_PERSON.FORENAME));
					bean.setSurname(r.get(T_PERSON.SURNAME));
					bean.setUsername(r.get(T_PERSON.USERNAME));
					bean.setUsnr(r.get(T_PERSON.USNR));
					cacheAllUsers.put(bean.getUsnr(), bean);
				}
			} catch (DataAccessException | ClassNotFoundException | SQLException e) {
				throw new DataAccessException(e.getMessage(), e);
			}
		}
		return cacheAllUsers;
	}

	/**
	 * convert array of usnrs to a list of profile beans
	 * 
	 * @param json the json object to be converted
	 * @return list of profile beans
	 */
	public List<ProfileBean> convertJsonToProfileBeans(JSON json) {
		String jsonString = json == null ? null : json.toString();
		Set<ProfileBean> jsonObjectUsers = new LinkedHashSet<>();
		if (jsonString != null) {
			JsonElement jsonObject = new JsonParser().parse(jsonString);
			Map<Integer, ProfileBean> allUsers = getAllUsers();
			if (jsonObject != null && jsonObject.isJsonArray()) {
				JsonArray jsonArray = jsonObject.getAsJsonArray();
				List<Integer> usnrs = new ArrayList<>();
				Iterator<JsonElement> i = jsonArray.iterator();
				while (i.hasNext()) {
					JsonElement e = i.next();
					if (e != null && !e.isJsonNull()) {
						usnrs.add(e.getAsInt());
					}
				}
				for (Integer usnr : usnrs) {
					ProfileBean bean = allUsers.get(usnr);
					if (bean != null) {
						jsonObjectUsers.add(bean);
					}
				}
			}
		}
		List<ProfileBean> list = new ArrayList<>();
		list.addAll(jsonObjectUsers);
		list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
		return list;
	}

	/**
	 * get privilege map
	 * 
	 * @param ctx context of jooq
	 * @return map of privileges
	 * 
	 * if anything went wrong on database side
	 */
	public Map<Integer, Map<Integer, AclBean>> getPrivilegeMap(DSLContext ctx) {
		Map<Integer, Map<Integer, AclBean>> result = new HashMap<>();
		SelectOnConditionStep<Record3<Integer, Integer, EnumAccesslevel>> sql = ctx
		// @formatter:off
			.select(T_USERPRIVILEGE.FK_USNR, 
							T_USERPRIVILEGE.FK_PRIVILEGE, 
							T_USERPRIVILEGE.SQUARE_ACL)
			.from(T_RELEASE)
			.leftJoin(T_USERPRIVILEGE).on(T_USERPRIVILEGE.FK_PRIVILEGE.eq(T_RELEASE.FK_PRIVILEGE));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		for (Record r : sql.fetch()) {
			Integer usnr = r.get(T_USERPRIVILEGE.FK_USNR);
			Map<Integer, AclBean> inner = result.get(usnr);
			if (inner == null) {
				inner = new HashMap<>();
				result.put(usnr, inner);
			}
			AclBean bean = new AclBean(r.get(T_USERPRIVILEGE.SQUARE_ACL));
			inner.put(r.get(T_USERPRIVILEGE.FK_PRIVILEGE), bean);
		}
		return result;
	}

	/**
	 * get privileges for user
	 * 
	 * @param privilegeId id of the privilege
	 * @return list of user application right beans
	 * 
	 * if anything went wrong on database side
	 */
	public List<UserApplRightBean> getUserPrivileges(Integer privilegeId) {
		List<UserApplRightBean> list = new ArrayList<>();
		try (SelectSeekStep1<Record5<EnumAccesslevel, String, String, String, Integer>, String> sql = getJooq()
		// @formatter:off
				.select(V_PRIVILEGE.ACL, 
								T_PERSON.FORENAME, 
								T_PERSON.SURNAME, 
								T_PERSON.USERNAME, 
								T_PERSON.USNR)
				.from(V_PRIVILEGE)
				.leftJoin(T_PERSON).on(V_PRIVILEGE.FK_USNR.eq(T_PERSON.USNR))
				.where(V_PRIVILEGE.FK_PRIVILEGE.eq(privilegeId))
				.orderBy(T_PERSON.USERNAME);
			// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				UserApplRightBean bean = new UserApplRightBean();
				bean.setAcl(r.get(V_PRIVILEGE.ACL));
				bean.setForename(r.get(T_PERSON.FORENAME));
				bean.setSurname(r.get(T_PERSON.SURNAME));
				bean.setUsername(r.get(T_PERSON.USERNAME));
				bean.setUsnr(r.get(T_PERSON.USNR));
				bean.setRightId(privilegeId);
				list.add(bean);
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return list;
	}

	/**
	 * remove all user privileges
	 * 
	 * @param jooq context to be used
	 * @param usnr of user
	 * @param privileges of user to be removed
	 * @return amount of affected tupels
	 * 
	 * if anything went wrong on database side
	 */
	public Integer removeUserPrivileges(DSLContext jooq, Integer usnr, Set<Integer> privileges) {
		DeleteConditionStep<TUserprivilegeRecord> sql = jooq
		// @formatter:off
			.deleteFrom(T_USERPRIVILEGE)
			.where(T_USERPRIVILEGE.FK_USNR.eq(usnr))
			.and(T_USERPRIVILEGE.FK_PRIVILEGE.in(privileges));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		return sql.execute();
	}

	/**
	 * remove user privilege
	 * 
	 * @param jooq context of jooq to be used
	 * @param group id of group privilege combi
	 * @param privilege of user privilege combi
	 * 
	 * if anything went wrong on database side
	 */
	public void removeGroupPrivilege(DSLContext jooq, Integer group, Integer privilege) {
		DeleteWhereStep<?> stmt = jooq.deleteFrom(T_GROUPPRIVILEGE);
		DeleteConditionStep<?> sql = stmt.where(DSL.trueCondition());
		if (group != null) {
			DeleteConditionStep<?> step = stmt.where(T_GROUPPRIVILEGE.FK_GROUP.eq(group));
			if (privilege != null) {
				sql = step.and(T_GROUPPRIVILEGE.FK_PRIVILEGE.eq(privilege));
			} else {
				sql = step;
			}
		} else if (privilege != null) {
			sql = stmt.where(T_GROUPPRIVILEGE.FK_PRIVILEGE.eq(privilege));
		}
		LOGGER.debug("{}", sql.toString());
		sql.execute();
	}

	/**
	 * remove user privilege
	 * 
	 * @param usnr of user privilege combi
	 * @param group id of group for privilege combi
	 * @param privileges of user privilege combis
	 * @return affected rows from the db
	 * 
	 * if anything went wrong on database side
	 */
	public Integer removeUserGroupPrivilege(Integer usnr, Integer group, Set<Integer> privileges) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(t -> {
				if (usnr != null) {
					DeleteConditionStep<TUserprivilegeRecord> sql = DSL.using(t)
					// @formatter:off
						.deleteFrom(T_USERPRIVILEGE)
						.where(T_USERPRIVILEGE.FK_USNR.eq(usnr))
						.and(T_USERPRIVILEGE.FK_PRIVILEGE.in(privileges));
					// @formatter:on
					LOGGER.debug("{}", sql.toString());
					lrw.addToInteger(sql.execute());
				}
				if (group != null) {
					DeleteConditionStep<TGroupprivilegeRecord> sql = DSL.using(t)
					// @formatter:off
						.deleteFrom(T_GROUPPRIVILEGE)
						.where(T_GROUPPRIVILEGE.FK_GROUP.eq(group))
						.and(T_GROUPPRIVILEGE.FK_PRIVILEGE.in(privileges));
					// @formatter:on
					LOGGER.debug("{}", sql.toString());
					lrw.addToInteger(sql.execute());
				}
			});
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return lrw.getInteger();
	}

	/**
	 * update the name of a privilege
	 * 
	 * @param jooq the dsl context
	 * @param name the new name of the privilege
	 * @param pk the id of the privilege
	 */
	public void updatePrivilegeName(DSLContext jooq, String name, Integer pk) {
		UpdateConditionStep<TPrivilegeRecord> sql = jooq
		// @formatter:off
			.update(T_PRIVILEGE)
			.set(T_PRIVILEGE.NAME, name)
			.where(T_PRIVILEGE.PK.eq(pk));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		sql.execute();
	}

	/**
	 * add rwx privileges for user on privilege
	 * 
	 * @param jooq the dsl context
	 * @param fkPrivilege the id of the privilege
	 * @param fkUsnr the id of the user
	 */
	public void addUserRWXPrivilege(DSLContext jooq, Integer fkPrivilege, Integer fkUsnr) {
		addUserGroupPrivilege(jooq, fkUsnr, null, fkPrivilege, new AclBean(EnumAccesslevel.rwx));
	}

	/**
	 * add user and/or group to T_USERPRIVILEGE and/or T_GROUPPRIVILEGE
	 * 
	 * @param usnr to be used
	 * @param group to be used
	 * @param privileges to be used
	 * @param acl to be used
	 * @return affected lines in db
	 * 
	 * if anything went wrong on database side
	 */
	public Integer addUserGroupPrivilege(Integer usnr, Integer group, List<Integer> privileges, AclBean acl) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(t -> {
				for (Integer privilege : privileges) {
					lrw.addToInteger(addUserGroupPrivilege(DSL.using(t), usnr, group, privilege, acl));
				}
			});
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return lrw.getInteger();
	}

	/**
	 * add user and/or group to T_USERPRIVILEGE and/or T_GROUPPRIVILEGE
	 * 
	 * @param jooq context to be used
	 * @param usnr to be used
	 * @param group to be used
	 * @param privilege to be used
	 * @param acl to be used
	 * @return affected lines in db
	 * 
	 * if anything went wrong on database side
	 */
	public Integer addUserGroupPrivilege(DSLContext jooq, Integer usnr, Integer group, Integer privilege, AclBean acl) {
		if (privilege == null) {
			throw new DataAccessException("privilege is null, aborted.");
		}
		Integer affected = 0;
		if (usnr != null) {
			InsertOnDuplicateSetMoreStep<TUserprivilegeRecord> sql = jooq
			// @formatter:off
				.insertInto(T_USERPRIVILEGE, 
										T_USERPRIVILEGE.FK_PRIVILEGE, 
										T_USERPRIVILEGE.FK_USNR,
										T_USERPRIVILEGE.SQUARE_ACL)
				.values(privilege, usnr, acl.getAcl())
				.onConflict(T_USERPRIVILEGE.FK_USNR, T_USERPRIVILEGE.FK_PRIVILEGE)
				.doUpdate()
				.set(T_USERPRIVILEGE.SQUARE_ACL, acl.getAcl());
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			affected += sql.execute();
		}
		// no else to allow both user and group adding at once
		if (group != null) {
			InsertOnDuplicateSetMoreStep<TGroupprivilegeRecord> sql = jooq
			// @formatter:off
				.insertInto(T_GROUPPRIVILEGE, 
										T_GROUPPRIVILEGE.FK_PRIVILEGE, 
										T_GROUPPRIVILEGE.FK_GROUP,
										T_GROUPPRIVILEGE.SQUARE_ACL)
				.values(privilege, group, acl.getAcl())
				.onConflict(T_GROUPPRIVILEGE.FK_GROUP, T_GROUPPRIVILEGE.FK_PRIVILEGE)
				.doUpdate()
				.set(T_GROUPPRIVILEGE.SQUARE_ACL, acl.getAcl());
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			affected += sql.execute();
		}
		return affected;
	}

	/**
	 * add privilege id and get it back
	 * 
	 * @param jooq context of jooq to be used
	 * @param name for new privilege; may be null if no unique can be named
	 * @return new pk of T_PRIVILEGE
	 */
	public Integer addAndGetPrivilegeId(DSLContext jooq, String name) {
		InsertResultStep<TPrivilegeRecord> sql = jooq
		// @formatter:off
			.insertInto(T_PRIVILEGE, 
									T_PRIVILEGE.NAME)
			.values(name)
			.onConflict(T_PRIVILEGE.NAME)
			.doUpdate() // doNothing would not return the pk
			.set(T_PRIVILEGE.NAME, name)
			.returning(T_PRIVILEGE.PK);
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		TPrivilegeRecord r = sql.fetchOne();
		LOGGER.debug("result set is {}", r.toString());
		return r.get(T_PRIVILEGE.PK);
	}

	/**
	 * get all square users that have no privilege in T_USERPRIVILEGE on
	 * privilegeId; the users might be part of a group that has still privileges by
	 * T_GROUPPRIVILEGE
	 * 
	 * @param privilegeId the id of the privilege
	 * @return the list of users
	 * 
	 * if anything went wrong on database side
	 */
	public List<UserBean> getAllSquareUsersWithoutUserPrivilege(Integer privilegeId) {
		try (CloseableDSLContext jooq = getJooq()) {
			return getAllSquareUsersWithoutUserPrivilege(jooq, privilegeId);
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all square users that have no privilege in T_USERPRIVILEGE on
	 * privilegeId; these users might have privileges by T_GROUPPRIVILEGE
	 *
	 * @param ctx jooq context to be used
	 * @param privilegeId the id of the privilege
	 * @return the list of users
	 * 
	 * if anything went wrong on database side
	 */
	public List<UserBean> getAllSquareUsersWithoutUserPrivilege(DSLContext ctx, Integer privilegeId) {
		List<UserBean> list = new ArrayList<>();
		SelectOrderByStep<Record4<Integer, String, String, String>> sql;
		SelectJoinStep<Record4<Integer, String, String, String>> step = ctx
		// @formatter:off
			.select(V_LOGIN.USNR, 
							V_LOGIN.FORENAME, 
							V_LOGIN.SURNAME, 
							V_LOGIN.USERNAME)
			.from(V_LOGIN);
		if (privilegeId != null) {
			sql = step.except(ctx
				.select(T_PERSON.USNR, 
								T_PERSON.FORENAME, 
								T_PERSON.SURNAME, 
								T_PERSON.USERNAME)
				.from(T_PERSON)
				.rightJoin(T_USERPRIVILEGE).on(T_USERPRIVILEGE.FK_USNR.eq(T_PERSON.USNR))
				.where(T_USERPRIVILEGE.FK_PRIVILEGE.eq(privilegeId)));
		} else {
			sql = step;
		}
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		for (Record r : sql.fetch()) {
			UserBean bean = new UserBean();
			bean.setForename(r.get(V_LOGIN.FORENAME));
			bean.setSurname(r.get(V_LOGIN.SURNAME));
			bean.setUsnr(r.get(V_LOGIN.USNR));
			bean.setUsername(r.get(V_LOGIN.USERNAME));
			list.add(bean);
		}
		list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
		return list;
	}

	/**
	 * get list of table columns for a schema
	 * 
	 * @param jooq the dsl context
	 * @param schemaName the name of the database schema
	 * @return list of table column beans
	 * 
	 * if anything went wrong on database side
	 */
	public List<TableColumnBean> getAllTableColumnsOfSchema(DSLContext jooq, String schemaName) {
		List<TableColumnBean> list = new ArrayList<>();
		Map<String, Integer> map = new HashMap<>();
		for (Schema schema : jooq.meta().getSchemas()) {
			if (schema.getName().equalsIgnoreCase(schemaName)) {
				for (Table<?> table : schema.getTables()) {
					Integer rowcount = map.get(table.getName());
					if (rowcount == null) {
						SelectJoinStep<Record1<Integer>> sql = jooq
						// @formatter:off
								.selectCount()
								.from(schema.getName() + "." + table.getName());
							// @formatter:on
						LOGGER.debug("{}", sql.toString());
						rowcount = jooq.fetchOne(sql).value1();
						map.put(table.getName(), rowcount);
					}
					for (Field<?> field : table.fields()) {
						list.add(
								new TableColumnBean(table.getName(), field.getName(), field.getDataType().getTypeName(), rowcount));
					}
				}
			}
		}
		return list;
	}

	/**
	 * add translation and return its key
	 * 
	 * @param jooq jooq context to be used
	 * @param fkPrivilege the id of the privilege
	 * @param translation value of translation due to isocode
	 * @param isocode language definition
	 * @return key of translation
	 * 
	 * if anything went wrong on database side
	 */
	public Integer addTranslation(DSLContext jooq, Integer fkPrivilege, String translation, String isocode) {
		InsertResultStep<TLanguagekeyRecord> sql = jooq
		// @formatter:off
			.insertInto(T_LANGUAGEKEY, 
									T_LANGUAGEKEY.FK_PRIVILEGE)
			.values(fkPrivilege)
			.returning(T_LANGUAGEKEY.PK);
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		Integer pk = sql.fetchOne().getPk();
		LOGGER.debug("new language key is {}", pk);
		if (translation != null) {
			upsertTranslation(jooq, pk, isocode, translation);
		}
		return pk;
	}

	/**
	 * add translations to database
	 * 
	 * @param jooq context to be used
	 * @param pkLang to be used if not null; generate a new one otherwise
	 * @param fkPrivilege to be used for new languagekeys; only set when pkLang is
	 * null
	 * @param translations map of isocode - value pairs, isocode is key
	 * @return pkLang
	 * 
	 * if anything went wrong on database side
	 */
	public Integer addTranslations(DSLContext jooq, Integer pkLang, Integer fkPrivilege,
			Map<String, String> translations) {
		if (pkLang == null) {
			InsertResultStep<TLanguagekeyRecord> sql = jooq
			// @formatter:off
				.insertInto(T_LANGUAGEKEY, 
										T_LANGUAGEKEY.FK_PRIVILEGE)
				.values(fkPrivilege)
				.returning(T_LANGUAGEKEY.PK);
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			pkLang = sql.fetchOne().getPk();
		}
		for (Entry<String, String> entry : translations.entrySet()) {
			upsertTranslation(jooq, pkLang, entry.getKey(), entry.getValue());
		}
		return pkLang;
	}

	/**
	 * upsert translation due to isocode and fkLang
	 * 
	 * @param jooq jooq context to be used
	 * @param fkLang key of translation
	 * @param isocode for translation definition
	 * @param translation value of translation
	 * @return number of affected database rows, should be 1
	 * 
	 * if anything went wrong on database side
	 */
	public Integer upsertTranslation(DSLContext jooq, Integer fkLang, String isocode, String translation) {
		if (translation != null && !translation.isBlank()) {
			EnumIsocode enumIsocode = new EnumConverter().getEnumIsocode(isocode);
			InsertOnDuplicateSetMoreStep<TTranslationRecord> sql = jooq
			// @formatter:off
  			.insertInto(T_TRANSLATION, 
  									T_TRANSLATION.FK_LANG, 
  									T_TRANSLATION.ISOCODE, 
  									T_TRANSLATION.VALUE)
  			.values(fkLang, enumIsocode, translation)
  			.onConflict(T_TRANSLATION.FK_LANG, T_TRANSLATION.ISOCODE)
  			.doUpdate()
  			.set(T_TRANSLATION.VALUE, translation);
  		// @formatter:on
			LOGGER.debug("{}", sql.toString());
			return sql.execute();
		} else {
			return 0;
		}
	}

	/**
	 * get all group privileges of privilege
	 * 
	 * @param privilege to be used as filter
	 * @return list of group privileges
	 * 
	 * if anything went wrong on database side
	 */
	public List<GroupPrivilegeBean> getAllGroupPrivileges(Integer privilege) {
		List<GroupPrivilegeBean> list = new ArrayList<>();
		try (SelectConditionStep<Record7<Integer, String, Integer, JSON, EnumAccesslevel, Boolean, Integer>> sql = getJooq()
		// @formatter:off
					.select(V_PRIVILEGES.PK, 
									V_PRIVILEGES.NAME,
									V_PRIVILEGES.FK_GROUP, 
									V_PRIVILEGES.USERS,
									V_PRIVILEGES.SQUARE_ACL, 
									V_PRIVILEGES.ISGROUP,
									V_PRIVILEGES.FK_PRIVILEGE)
					.from(V_PRIVILEGES)
					.where(V_PRIVILEGES.FK_PRIVILEGE.eq(privilege));
				// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				GroupPrivilegeBean bean = new GroupPrivilegeBean(r.get(V_PRIVILEGES.PK), r.get(V_PRIVILEGES.ISGROUP),
						r.get(V_PRIVILEGES.FK_PRIVILEGE));
				bean.setFkGroup(r.get(V_PRIVILEGES.FK_GROUP));
				bean.setName(r.get(V_PRIVILEGES.NAME));
				bean.getAcl().setAcl(r.get(V_PRIVILEGES.SQUARE_ACL));
				JSON users = r.get(V_PRIVILEGES.USERS);
				if (users != null) {
					List<ProfileBean> userBeans = convertJsonToProfileBeans(users);
					bean.getUsers().addAll(userBeans);
					if (!bean.getIsGroup()) {
						// if this is a privilege from T_USERPRIVILEGE, there must be still one usnr;
						// otherwise, that user is no longer a Square² user
						if (!bean.getUsers().isEmpty()) {
							Integer currentUsnr = bean.getUsers().get(0).getUsnr();
							bean.setFkUsnr(currentUsnr);
						} else {
							facesContext.notifyWarning("privilege without users (or empty group) found: {0}", bean.getFkPrivilege());
						}
					}
				}
				list.add(bean);
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return list;
	}

	/**
	 * get all the groups from the database except the ones with the pks
	 * 
	 * @param pks list of excluded group pks
	 * @return list of user group beans
	 * 
	 * if anything went wrong on database side
	 */
	public List<UsergroupBean> getAllGroupsExcept(List<Integer> pks) {
		List<UsergroupBean> list = new ArrayList<>();
		try (SelectConditionStep<Record3<Integer, String, JSON>> sql = getJooq()
		// @formatter:off
				.select(V_GROUP.PK, 
								V_GROUP.NAME, 
								V_GROUP.USERS)
				.from(V_GROUP)
				.where(V_GROUP.PK.notIn(pks));
			// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				Integer pk = r.get(V_GROUP.PK);
				String name = r.get(V_GROUP.NAME);
				List<ProfileBean> users = convertJsonToProfileBeans(r.get(V_GROUP.USERS));
				list.add(new UsergroupBean(pk, name, users));
			}
			list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return list;
	}

	/**
	 * get all groups except the one from the list
	 * 
	 * @param groupPrivileges containing group pks to exclude
	 * @return list of groups
	 */
	public List<UsergroupBean> getAllGroupsExceptOf(List<GroupPrivilegeBean> groupPrivileges) {
		List<Integer> pks = new ArrayList<>();
		for (GroupPrivilegeBean bean : groupPrivileges) {
			if (bean.getIsGroup()) {
				pks.add(bean.getFkGroup());
			}
		}
		return getAllGroupsExcept(pks);
	}

	/**
	 * upsert acl privileges
	 * 
	 * @param bean to be used
	 * @return affected lines in db
	 * 
	 * if anything went wrong on database side
	 */
	public Integer upsertUserGroupPrivilege(GroupPrivilegeBean bean) {
		List<GroupPrivilegeBean> list = new ArrayList<>();
		list.add(bean);
		return upsertUserGroupPrivileges(list);
	}

	/**
	 * upsert acl privileges
	 * 
	 * @param beans to be used
	 * @return affected lines in db
	 * 
	 * if anything went wrong on database side
	 */
	public Integer upsertUserGroupPrivileges(List<GroupPrivilegeBean> beans) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(t -> {
				for (GroupPrivilegeBean bean : beans) {
					if (bean.getIsGroup()) {
						if (bean.getFkGroup() == null) {
							throw new DataAccessException("group must not be null on upserting group privilege");
						}
						InsertOnDuplicateSetMoreStep<TGroupprivilegeRecord> sql = DSL.using(t)
						// @formatter:off
							.insertInto(T_GROUPPRIVILEGE, 
													T_GROUPPRIVILEGE.FK_PRIVILEGE, 
													T_GROUPPRIVILEGE.FK_GROUP,
													T_GROUPPRIVILEGE.SQUARE_ACL)
							.values(bean.getFkPrivilege(), bean.getFkGroup(), bean.getAcl().getAcl())
							.onConflict(T_GROUPPRIVILEGE.FK_GROUP, T_GROUPPRIVILEGE.FK_PRIVILEGE)
							.doUpdate()
							.set(T_GROUPPRIVILEGE.SQUARE_ACL, bean.getAcl().getAcl());
						// @formatter:on
						LOGGER.debug("{}", sql.toString());
						lrw.addToInteger(sql.execute());
					} else {
						if (bean.getFkUsnr() == null) {
							throw new DataAccessException("usnr must not be null on upserting user privilege");
						}
						InsertOnDuplicateSetMoreStep<TUserprivilegeRecord> sql2 = DSL.using(t)
						// @formatter:off
							.insertInto(T_USERPRIVILEGE, 
													T_USERPRIVILEGE.FK_PRIVILEGE,
													T_USERPRIVILEGE.FK_USNR,
													T_USERPRIVILEGE.SQUARE_ACL)
							.values(bean.getFkPrivilege(), bean.getFkUsnr(), bean.getAcl().getAcl())
							.onConflict(T_USERPRIVILEGE.FK_USNR, T_USERPRIVILEGE.FK_PRIVILEGE)
							.doUpdate()
							.set(T_USERPRIVILEGE.SQUARE_ACL, bean.getAcl().getAcl());
						// @formatter:on
						LOGGER.debug("{}", sql2.toString());
						lrw.addToInteger(sql2.execute());
					}
				}
			});
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return lrw.getInteger();
	}

	/**
	 * @param cacheAllUsers the cacheAllUsers to set
	 */
	protected void setCacheAllUsers(Map<Integer, ProfileBean> cacheAllUsers) {
		this.cacheAllUsers = cacheAllUsers;
	}

	/**
	 * add a warning to the faces messages
	 * 
	 * @param message the message; use {X} as placeholder for params where X is the
	 * index of the param
	 * @param params the params
	 */
	public void notifyWarning(String message, Object... params) {
		facesContext.notifyWarning(message, params);
	}
}
