package square2.db.control;

import static de.ship.dbppsquare.square.Tables.V_VERSION;

import java.sql.SQLException;
import java.util.Map;

import org.jooq.Record1;
import org.jooq.SelectJoinStep;
import org.jooq.exception.DataAccessException;

import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;

/**
 * 
 * @author henkej
 * 
 */
public class VersionGateway extends JooqGateway {
	/**
	 * generate new administration gateway
	 * 
	 * @param facesContext context of this constructor call
	 */
	public VersionGateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	@Override
	public Map<Integer, Map<Integer, AclBean>> getRightMap() throws Exception {
		return null;
	}

	/**
	 * @return the database version
	 */
	public String getVersion() {
		try (SelectJoinStep<Record1<Integer>> sql = getJooq().select(V_VERSION.DB_VERSION).from(V_VERSION)) {
			Integer version = sql.fetchOne(V_VERSION.DB_VERSION);
			if (version == null) {
				throw new DataAccessException("db_version is null from v_version; please correct your database");
			}
			return version.toString();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}
}
