package square2.db.control;

import static de.ship.dbppsquare.square.Tables.T_LOGIN;
import static de.ship.dbppsquare.square.Tables.T_MAINTENANCE;
import static de.ship.dbppsquare.square.Tables.T_PERSON;
import static de.ship.dbppsquare.square.Tables.T_PRIVILEGE;
import static de.ship.dbppsquare.square.Tables.V_LOGIN;
import static de.ship.dbppsquare.square.Tables.V_PRIVILEGE;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.ProcessingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.jooq.CloseableDSLContext;
import org.jooq.DSLContext;
import org.jooq.InsertReturningStep;
import org.jooq.JSONB;
import org.jooq.Record;
import org.jooq.Record2;
import org.jooq.Record3;
import org.jooq.Record5;
import org.jooq.SelectConditionStep;
import org.jooq.UpdateConditionStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;

import de.ship.dbppsquare.square.enums.EnumAccesslevel;
import de.ship.dbppsquare.square.tables.records.TLoginRecord;
import de.ship.dbppsquare.square.tables.records.TPersonRecord;
import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.KeyCloakDelegator;
import square2.modules.model.MaintenanceBean;
import square2.modules.model.PrivilegeBean;
import square2.modules.model.ProfileBean;

/**
 * 
 * @author henkej
 * 
 */
public class LoginGateway extends JooqGateway {
	private static final Logger LOGGER = LogManager.getLogger(LoginGateway.class);

	/**
	 * create new login gateway
	 * 
	 * @param facesContext the context of this gateway
	 */
	public LoginGateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	@Override
	public Map<Integer, Map<Integer, AclBean>> getRightMap() throws Exception {
		return super.getPrivilegeMap(getJooq());
	}
	
	/**
	 * @return the map of roles
	 */
	public Map<String, Integer> getRolePrivilegeMap() {
		try (CloseableDSLContext jooq = getJooq()) {
			final Map<String, Integer> map = new HashMap<>();
			jooq.transaction(c -> {
				SelectConditionStep<Record2<Integer, String>> sql = DSL.using(c)
				// @formatter:off
					.select(T_PRIVILEGE.PK,
							    T_PRIVILEGE.NAME)
					.from(T_PRIVILEGE)
					.where(T_PRIVILEGE.NAME.like("role.square.%"));
		    // @formatter:on
				LOGGER.debug("{}", sql.toString());
				for (Record r : sql.fetch()) {
					String name = r.get(T_PRIVILEGE.NAME);
					Integer pk = r.get(T_PRIVILEGE.PK);
					map.put(name, pk);
				}
			});
			return map;
		} catch (DataAccessException | ClassNotFoundException | SQLException | ProcessingException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get profile from db
	 * 
	 * @param username the user name
	 * @param password the password
	 * @return profile of found user
	 */
	public ProfileBean getProfile(String username, String password) {
		SquareFacesContext facesContext = getFacesContext();
		final ProfileBean bean = new ProfileBean();

		String url = facesContext.getParam("KEYCLOAK_URL", null);
		String realm = facesContext.getParam("KEYCLOAK_REALM", "ship");
		String clientId = facesContext.getParam("KEYCLOAK_CLIENTID", "Square2");
//		String secret = facesContext.getParam("KEYCLOAK_SECRET", "");
		if (url != null) {
			try {
				KeyCloakDelegator del = new KeyCloakDelegator(username, password, url, realm, clientId);
				bean.setKeyCloakDelegator(del);
				bean.setUsername(username);
				bean.setForename(del.getForename());
				bean.setSurname(del.getSurname());
				bean.setUsnr(del.getUsnr());
				bean.setRoles(del.getRoles(), getRolePrivilegeMap());
				bean.getConfig().addAll(new ConfigGateway(facesContext).getConfig(bean.getUsnr()));
				registerKeycloakUser(facesContext, bean);
			} catch (ProcessingException e) {
				// invalid keycloak login or misconfigured keycloak options
				LOGGER.error(e.getMessage()); // no stacktrace for wrong logins (too many lines)
				bean.logoutFromKeycloakIfConnected(); // if there is no usnr
				bean.setKeycloakAccessToken(null); // if there is no valid profile any longer
			} catch (NotAuthorizedException e) {
				// login not found in keycloak, so do nothing here
				LOGGER.warn("{} is not a user of keycloak", username);
			} catch (ForbiddenException e) {
				LOGGER.error("keycloak client seems to be not yet configured");
				facesContext.notifyError("error.keycloak.configuration");
				bean.logoutFromKeycloakIfConnected();
				bean.setKeycloakAccessToken(null);
			}
		}
		if (bean.hasKeycloakAccessToken()) {
			bean.setExpireOn(new Date());
		} else {
			try (CloseableDSLContext jooq = getJooq()) {
				jooq.transaction(c -> {
					SelectConditionStep<Record5<String, String, Integer, String, LocalDateTime>> sql = DSL.using(c)
					// @formatter:off
			    	.select(V_LOGIN.FORENAME, 
			    			V_LOGIN.SURNAME, 
			    			V_LOGIN.USNR, 
			    			V_LOGIN.PASSWORD, 
			    			V_LOGIN.EXPIRE_ON)
			    	.from(V_LOGIN)
			    	.where(V_LOGIN.USERNAME.eq(username));
			    // @formatter:on
					LOGGER.debug("{}", sql.toString());
					for (Record r : sql.fetch()) {
						String dbPwd = r.get(V_LOGIN.PASSWORD);
						Integer usnr = r.get(V_LOGIN.USNR);
						Boolean valid = false;
						try {
							valid = new StrongPasswordEncryptor().checkPassword(password, dbPwd);
						} catch (EncryptionOperationNotPossibleException e) {
							LOGGER.info("found an invalid password for the new encryption");
						}
						if (valid) {
							bean.setUsername(username);
							bean.setForename(r.get(V_LOGIN.FORENAME));
							bean.setSurname(r.get(V_LOGIN.SURNAME));
							bean.setUsnr(usnr);
							bean.getPrivileges().addAll(getPrivileges(DSL.using(c), bean.getUsnr()));
							bean.getConfig().addAll(new ConfigGateway(facesContext).getConfig(bean.getUsnr()));
							LocalDateTime ldt = r.get(V_LOGIN.EXPIRE_ON);
							Date expireOn = ldt == null ? null : Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
							bean.setExpireOn(expireOn);
						}
					}
				});
			} catch (DataAccessException | ClassNotFoundException | SQLException | ProcessingException e) {
				throw new DataAccessException(e.getMessage(), e);
			}
		}
		if (bean.getUsername() != null) {
			return bean;
		}
		throw new DataAccessException("login invalid");
	}

	/**
	 * register the keycloak user in T_PERSON to have a usnr for privileges
	 * 
	 * @param facesContext the faces context
	 * @param bean the bean
	 */
	private void registerKeycloakUser(SquareFacesContext facesContext, ProfileBean bean) {
		StringBuilder buf = new StringBuilder();
		buf.append("{\"registration\": \"");
		buf.append(facesContext.getParam("KEYCLOAK_URL", "keyloak unknown instance"));
		buf.append("\"}");
		JSONB contact = JSONB.valueOf(buf.toString());
		try (InsertReturningStep<TPersonRecord> sql = getJooq()
		// @formatter:off
			.insertInto(T_PERSON,
					        T_PERSON.USNR,
					        T_PERSON.USERNAME,
					        T_PERSON.FORENAME,
					        T_PERSON.SURNAME,
					        T_PERSON.CONTACT)
			.values(bean.getUsnr(), bean.getUsername(), bean.getForename(), bean.getSurname(), contact)
			.onConflict(T_PERSON.USERNAME)
			.doNothing();
		// @formatter.on
		) {
			LOGGER.debug("{}", sql.toString());
			sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update password
	 * 
	 * @param usnr the id of the user
	 * @param encryptedPassword the encrypted password
	 * @return number of affected database rows, should be 1
	 * 
	 * if anything went wrong on database side
	 */
	public Integer updatePassword(Integer usnr, String encryptedPassword) {
		try (UpdateConditionStep<TLoginRecord> sql = getJooq()
		// @formatter:off
		  	.update(T_LOGIN)
		  	.set(T_LOGIN.PASSWORD, encryptedPassword)
		  	.where(T_LOGIN.FK_USNR.eq(usnr));
		  // @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all privileges of usnr
	 * 
	 * @param jooq the jooq connection
	 * @param usnr the id of the user
	 * @return a list of privileges for that user
	 * 
	 * if anything went wrong on database side
	 */
	public List<PrivilegeBean> getPrivileges(DSLContext jooq, Integer usnr) {
		try (SelectConditionStep<Record3<Integer, String, EnumAccesslevel>> sql = jooq
		// @formatter:off
		  	.select(T_PRIVILEGE.PK, 
		  					T_PRIVILEGE.NAME, 
		  					V_PRIVILEGE.ACL)
		  	.from(T_PRIVILEGE)
		  	.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_PRIVILEGE.PK))
		  	.where(V_PRIVILEGE.FK_USNR.eq(usnr))
		  	.and(V_PRIVILEGE.ACL.isNotNull()); // by left join, this can be null
		  // @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<PrivilegeBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				Integer pk = r.get(T_PRIVILEGE.PK);
				String name = r.get(T_PRIVILEGE.NAME);
				EnumAccesslevel acl = r.get(V_PRIVILEGE.ACL);
				list.add(new PrivilegeBean(pk, name, acl));
			}
			return list;
		}
	}

	/**
	 * get maintenances from db; if none is found, return empty list
	 * 
	 * @return list of maintenances
	 * 
	 * if anything went wrong on database side
	 */
	public List<MaintenanceBean> getMaintenances() {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record3<String, LocalDateTime, LocalDateTime>> sql = jooq
			// @formatter:off
		  	.select(T_MAINTENANCE.MESSAGE, 
		  					T_MAINTENANCE.DOWNTIME_START, 
		  					T_MAINTENANCE.DOWNTIME_END)
		  	.from(T_MAINTENANCE)
		  	.where(T_MAINTENANCE.AFFECTED_SQUARE.eq(true))
		  	.and(T_MAINTENANCE.DOWNTIME_END.ge(LocalDateTime.now()));
		  // @formatter:on
			LOGGER.debug("{}", sql.toString());
			List<MaintenanceBean> list = new ArrayList<>();
			for (Record3<String, LocalDateTime, LocalDateTime> r : sql.fetch()) {
				LocalDateTime ldtStart = r.get(T_MAINTENANCE.DOWNTIME_START);
				LocalDateTime ldtEnd = r.get(T_MAINTENANCE.DOWNTIME_END);
				Timestamp start = ldtStart == null ? null : Timestamp.valueOf(ldtStart);
				Timestamp end = ldtEnd == null ? null : Timestamp.valueOf(ldtEnd);
				list.add(new MaintenanceBean(r.get(T_MAINTENANCE.MESSAGE), start, end));
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}
}
