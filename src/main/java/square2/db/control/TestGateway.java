package square2.db.control;

import static de.ship.dbppsquare.square.Tables.T_FUNCTION;
import static de.ship.dbppsquare.square.Tables.T_FUNCTIONINPUT;
import static de.ship.dbppsquare.square.Tables.T_FUNCTIONOUTPUT;
import static de.ship.dbppsquare.square.Tables.V_FUNCTIONREFERENCE;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.CloseableDSLContext;
import org.jooq.JSONB;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Record3;
import org.jooq.SelectConditionStep;
import org.jooq.SelectSeekStep1;
import org.jooq.exception.DataAccessException;

import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.statistic.FunctionOutputBean;
import square2.modules.model.statistic.FunctionReferenceBean;

/**
 * 
 * @author henkej
 *
 */
public class TestGateway extends JooqGateway {
	private static final Logger LOGGER = LogManager.getLogger(TestGateway.class);

	/**
	 * create new test gateway
	 * 
	 * @param facesContext
	 *          the context of this gateway
	 */
	public TestGateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	@Override
	public Map<Integer, Map<Integer, AclBean>> getRightMap() throws Exception {
		try (CloseableDSLContext jooq = getJooq()) {
			return super.getPrivilegeMap(jooq);
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get lines of function; this is the function body
	 * 
	 * @param functionId
	 *          the id of the function
	 * @param functionName
	 *          the name of the function
	 * @param squareConfigFile
	 *          the square config file
	 * @param varname
	 *          the varname
	 * @param varlist
	 *          the varlist
	 * @param dataframe
	 *          the dataframe
	 * @param vardef
	 *          the vardef
	 * @return a list of function lines
	 * 
	 *         if anything went wrong on database side
	 */
	public List<String> getFunctionLines(Integer functionId, String functionName, String squareConfigFile, String varname,
			String varlist, String dataframe, String vardef) {
		List<String> result = new ArrayList<>();
		try (CloseableDSLContext jooq = getJooq()) {

			// TODO: how to define parents in withRecursive?
			// ctx.withRecursive(V_FUNCTIONREFERENCE.FUNCTION_NAME.getName(),
			// V_FUNCTIONREFERENCE.REFERENCE_NAME.getName(),
			// V_FUNCTIONREFERENCE.FUNCTION_PK.getName(),
			// V_FUNCTIONREFERENCE.REFERENCE_PK.getName())
			// .as(ctx.select(V_FUNCTIONREFERENCE.FUNCTION_NAME,
			// V_FUNCTIONREFERENCE.REFERENCE_NAME, V_FUNCTIONREFERENCE.FUNCTION_PK,
			// V_FUNCTIONREFERENCE.REFERENCE_PK)
			// .from(V_FUNCTIONREFERENCE)
			// .where(V_FUNCTIONREFERENCE.FUNCTION_PK.equal(functionId))
			// .unionAll(
			// ctx.select(V_FUNCTIONREFERENCE.FUNCTION_NAME,
			// V_FUNCTIONREFERENCE.REFERENCE_NAME,
			// V_FUNCTIONREFERENCE.FUNCTION_PK, V_FUNCTIONREFERENCE.REFERENCE_PK)
			// .from(V_FUNCTIONREFERENCE)
			// .join(arg0, arg1)
			// )
			// )

			// possible solution in this code:

			// Tree h = Tree.TREE.as("h");
			// Tree h2 = Tree.TREE.as("h2");
			// CommonTableExpression<Record3<String, Integer, Integer>> myTree =
			// name("t")
			// .fields(Tree.TREE.NAME.getName(), Tree.TREE.KEY.getName(),
			// Tree.TREE.PARENT.getName())
			// .as(create
			// .select(h.NAME, h.KEY, h.PARENT)
			// .from(h)
			// .where(h.PARENT.isNull())
			// .unionAll(create
			// .select(h2.NAME, h2.KEY, h2.PARENT)
			// .from(h2)
			// .innerJoin(table(name("t")).as("t1"))
			// .on(h2.PARENT.equal(field(name("t1", "key"), Integer.class)))
			// ));
			//
			// Result<Record1<Object>> result =
			// create.withRecursive(myTree).select(field(name("t",
			// "name"))).from("t").fetch();
			// System.out.println(create.withRecursive(myTree).select(field("t",
			// "name")).from("t").toString());

			// @formatter:off
			String sql = "with recursive parents(" + "function_name, " + "reference_name, " + "function_pk, "
					+ "reference_pk" + ") as (" + "select " + "function_name, " + "reference_name, " + "function_pk, "
					+ "reference_pk " + "from " + V_FUNCTIONREFERENCE.getSchema().getQualifiedName() + "."
					+ V_FUNCTIONREFERENCE.getQualifiedName() + " where function_pk =  " + functionId + "union all select "
					+ "V_FUNCTIONREFERENCE.function_name, " + "V_FUNCTIONREFERENCE.reference_name, "
					+ "V_FUNCTIONREFERENCE.function_pk, " + "V_FUNCTIONREFERENCE.reference_pk " + "from "
					+ V_FUNCTIONREFERENCE.getSchema().getQualifiedName() + "." + V_FUNCTIONREFERENCE.getQualifiedName()
					+ " join parents on parents.reference_pk = V_FUNCTIONREFERENCE.function_pk) "
					+ "select parents.function_name, " + "parents.reference_name, parents.function_pk, "
					+ "parents.reference_pk from parents ";
			// @formatter:on
			List<FunctionReferenceBean> functions = new ArrayList<>();
			LOGGER.debug("{}", sql.toString());
			for (Record r : jooq.fetch(sql)) {
				FunctionReferenceBean bean = new FunctionReferenceBean();
				bean.setReferenceId(r.get(V_FUNCTIONREFERENCE.REFERENCE_PK));
				bean.setReferenceName(r.get(V_FUNCTIONREFERENCE.REFERENCE_NAME));
				functions.add(bean);
			}
			FunctionReferenceBean thisBean = new FunctionReferenceBean();
			thisBean.setReferenceId(functionId);
			thisBean.setReferenceName(functionName);
			functions.add(thisBean); // make this function written as last one
			for (FunctionReferenceBean bean : functions) {
				StringBuilder functionHead = new StringBuilder(bean.getReferenceName());
				functionHead.append(" <- function(");
				// by default, add varname, dataframe and vardef
				functionHead.append(varname);
				functionHead.append(", ").append(dataframe);
				functionHead.append(", ").append(vardef);

				SelectSeekStep1<Record3<String, JSONB, Integer>, Integer> sql2 = jooq
				// @formatter:off
					.select(T_FUNCTIONINPUT.INPUT_NAME, 
									T_FUNCTIONINPUT.DEFAULT_VALUE, 
									T_FUNCTIONINPUT.ORDERING)
					.from(T_FUNCTIONINPUT)
					.where(T_FUNCTIONINPUT.FK_FUNCTION.eq(bean.getReferenceId()))
					.orderBy(T_FUNCTIONINPUT.ORDERING);
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				for (Record r : sql2.fetch()) {
					functionHead.append(", ");
					functionHead.append(r.get(T_FUNCTIONINPUT.INPUT_NAME));
					JSONB defaultValueJsonb = r.get(T_FUNCTIONINPUT.DEFAULT_VALUE);
					String defaultValue = defaultValueJsonb == null ? null : defaultValueJsonb.data();
					if (defaultValue != null && !defaultValue.trim().isEmpty()) {
						functionHead.append(" = ").append(defaultValue);
					}
				}
				functionHead.append("){");
				result.add(functionHead.toString());

				List<String> testLines = new ArrayList<>();
				testLines.add("test <- function(){");

				SelectConditionStep<Record2<String, String>> sql3 = jooq
				// @formatter:off
					.select(T_FUNCTION.BODY, 
									T_FUNCTION.TEST)
					.from(T_FUNCTION)
					.where(T_FUNCTION.PK.eq(bean.getReferenceId()));
				// @formatter:on
				LOGGER.debug("{}", sql3.toString());

				for (Record r : sql3.fetch()) {
					String s = r.get(T_FUNCTION.BODY);
					result.add(s == null ? "" : s);
					s = r.get(T_FUNCTION.TEST);
					testLines.add(s == null ? "" : s);
				}
				result.add("}");
				result.add("");
				testLines.add("}");
				result.addAll(testLines);
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return result;
	}

	/**
	 * get output params from db
	 * 
	 * @param functionId
	 *          id of the function
	 * @return list of function output beans
	 * 
	 *         if anything went wrong on database side
	 */
	public List<FunctionOutputBean> getFunctionOutput(Integer functionId) {
		List<FunctionOutputBean> list = new ArrayList<>();
		try (SelectConditionStep<Record3<Integer, String, String>> sql = getJooq()
		// @formatter:off
			.select(T_FUNCTIONOUTPUT.PK, 
							T_FUNCTIONOUTPUT.NAME, 
							T_FUNCTIONOUTPUT.DESCRIPTION)
			.from(T_FUNCTIONOUTPUT)
			.where(T_FUNCTIONOUTPUT.FK_FUNCTION.eq(functionId));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				FunctionOutputBean bean = new FunctionOutputBean(r.get(T_FUNCTIONOUTPUT.PK));
				bean.setName(r.get(T_FUNCTIONOUTPUT.NAME));
				bean.setDescription(r.get(T_FUNCTIONOUTPUT.DESCRIPTION));
				list.add(bean);
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return list;
	}

	/**
	 * get name of function
	 * 
	 * @param functionId
	 *          the id of the function
	 * @return function name
	 * 
	 *         if anything went wrong on database side
	 */
	public String getFunctionName(Integer functionId) {
		try (SelectConditionStep<Record1<String>> sql = getJooq()
		// @formatter:off
			.select(T_FUNCTION.NAME)
			.from(T_FUNCTION)
			.where(T_FUNCTION.PK.eq(functionId));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.fetchOne(T_FUNCTION.NAME);
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}
}
