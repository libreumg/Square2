package square2.latex.control;

import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author henkej
 * @param <E>
 *          type of the list
 *
 */
public class AfterIterator<E> implements Iterator<E> {
	private List<E> list;
	private int indexSelected = -1;

	/**
	 * generate new after iterator
	 * 
	 * @param list
	 *          list of elements
	 */
	public AfterIterator(List<E> list) {
		this.list = list;
	}

	@Override
	public boolean hasNext() {
		return indexSelected < list.size() - 1;
	}

	@Override
	public E next() {
		indexSelected++;
		return list.get(indexSelected);
	}

	@Override
	public void remove() {
		list.remove(indexSelected);
	}

	/**
	 * @return the value after the next one
	 */
	public E afterNext() {
		return hasNext() ? list.get(indexSelected + 1) : null;
	}
}
