package square2.servlet;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import square2.help.ContextKey;
import square2.modules.model.report.ReportBean;
import square2.r.control.SquareControl2Gateway;
import square2.r.control.config.ConfigurationInterface;
import square2.r.control.config.SquareControl2Configuration;

/**
 * 
 * @author henkej
 *
 */
@WebServlet("/export/*")
public class ExportService extends SquareHttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LogManager.getLogger(AjaxJsonmatrixServlet.class);

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String filename = request.getPathInfo(); // ! contains leading /

		LOGGER.debug("requested {}", filename);

		String squareRunEdition = "SquareControlImpl"; // TODO: replace by a more flexible selection
		response.reset();
		try {
			ReportBean bean = getReportModel(request).getShowReportBean();
			String conffile = getServletContextInitParameter("squaredbconnectorfile", "/etc/squaredbconnector.properties");
			String section = getServletContextInitParameter("squaredbconnectorsection", "prod");
			ServletContext ctx = getServletContext();
			String outputPath = (String) ctx.getAttribute(ContextKey.SC_OUTPUT_PATH.get());

			ConfigurationInterface conf = new SquareControl2Configuration(conffile, section, bean.getPk(), outputPath);
			byte[] content = SquareControl2Gateway.callReport(ctx, squareRunEdition, conf, filename);
			
			response.setStatus(200);
			response.setContentType("*/*");

			response.getOutputStream().write(content);
			response.flushBuffer();
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			response.setContentType("text");
			response.setStatus(400);
			response.getOutputStream().print(e.getMessage());
		}
	}
}
