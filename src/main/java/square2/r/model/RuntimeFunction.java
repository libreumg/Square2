package square2.r.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class RuntimeFunction {
	private String callStatement;
	private String fileName;
	private List<String> lines;

	/**
	 * create a new runtime function
	 * 
	 * @param callStatement
	 *          the statement to be called
	 * @param fileName
	 *          the filename
	 */
	public RuntimeFunction(String callStatement, String fileName) {
		this.callStatement = callStatement;
		this.fileName = fileName;
		this.lines = new ArrayList<>();
	}

	@Override
	public final String toString() {
		StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
		buf.append("@{callStatement=").append(callStatement);
		buf.append(",fileName=").append(fileName);
		buf.append(",lines=").append(lines);
		buf.append("}");
		return buf.toString();
	}

	/**
	 * @return the call statement
	 */
	public String getCallStatement() {
		return callStatement;
	}

	/**
	 * @return the file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @return the lines
	 */
	public List<String> getLines() {
		return lines;
	}
}
