package square2.r.control;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import square2.help.SquareFacesContext;
import square2.modules.model.study.io.ShipBean;

/**
 * 
 * @author henkej
 *
 */
public class ShipGateway extends RGatewayInterface {
	private static final Logger LOGGER = LogManager.getLogger(ShipGateway.class);
	/**
	 * the R library name
	 */
	public static final String LIBNAME = "shipdbRepository";

	/**
	 * @param facesContext the context of the call
	 */
	public ShipGateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	@Override
	public RConnection loadLibrary() throws RserveException, IOException {
		RConnection connection;
		try {
			connection = getConnection();
		} catch (RserveException e) {
			LOGGER.error(e.getMessage(), e);
			getFacesContext().notifyException(e);
			return null;
		}
		REXP rexp;
		try {
			rexp = connection.parseAndEval("try(library(" + LIBNAME + "))");
			if (!rexp.isString() || !LIBNAME.equals(rexp.asString())) {
				throw new IOException("loaded library " + LIBNAME + " and got " + rexp.asString());
			} else {
				LOGGER.debug("successfully loaded library {}", LIBNAME);
			}
		} catch (REngineException | REXPMismatchException e) {
			LOGGER.error(e.getMessage(), e);
			getFacesContext().notifyException(e);
			return connection;
		}
		return connection;
	}

	/**
	 * import from ship as definied in the ship bean
	 * 
	 * @param shipConfigSection the ship configuration section
	 * @param shipBean the import information
	 * @throws REXPMismatchException on expression errors
	 * @throws REngineException on R engine errors
	 * @throws IOException on io errors
	 */
	public void importFrom(Object shipConfigSection, ShipBean shipBean) throws IOException, REngineException, REXPMismatchException {
		if (shipConfigSection == null) {
			shipConfigSection = "db"; // default
		}
		String sq2psqlConffile = getFacesContext().getParam("squaredbconnectorfile", "/etc/squaredbconnector.properties");
		String sq2psqlDestsect = getFacesContext().getParam("squaredbconnectorsection", "prod");
		String username = getFacesContext().getUsername();
		StringBuilder buf = new StringBuilder();
		buf.append("try({");
		buf.append("repository <- shipdbRepository(");
		// deactivated, because this is the default: buf.append("config_filename='/etc/square_foreign/shipdbRepository.properties', ");
		buf.append("config_section='").append(shipConfigSection).append("', ");
		buf.append("parent_unique_name='").append(shipBean.getSrcUniqueName()).append("')\n");
		buf.append("library(squarerepositoryspec)\n");
		buf.append("dd <- getDD(repository = repository)\n"); // for prod, see SquareControl2Gateway
		buf.append("library(sq2psqlRepository)\n");
		buf.append("repository <- sq2psqlRepository(");
		buf.append("config_filename = '").append(sq2psqlConffile).append("', ");
		buf.append("config_section = '").append(sq2psqlDestsect).append("', report_id = NA, ignore_report = TRUE)\n");
		buf.append("writeDD(repository = repository, dd = dd, owner = '").append(username);
		buf.append("', parent_unique_name='").append(shipBean.getDestUniqueName());
		buf.append("', default_locale = '").append(shipBean.getLocale()).append("')\n");
		buf.append("})");
		LOGGER.debug("calling R with {}", buf.toString());
		RConnection connection = loadLibrary();
		REXP resultString = connection.parseAndEval(buf.toString());
		String result = resultString.asString();
		getFacesContext().notifyInformation(result);
	}

	@Override
	public void close() throws Exception {
		getConnection().close();
	}
}
