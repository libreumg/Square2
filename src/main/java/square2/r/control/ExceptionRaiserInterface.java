package square2.r.control;

/**
 * 
 * @author henkej
 *
 */
public interface ExceptionRaiserInterface {
	/**
	 * handle exception case; do some logs or inform user about it or whatever
	 * 
	 * @param e
	 *          exception that has been thrown
	 * @param attachment
	 *          optional attachments to exception
	 */
	public void raise(Exception e, Object... attachment);
}
