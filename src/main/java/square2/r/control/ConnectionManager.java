package square2.r.control;

import java.io.IOException;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import square2.help.ContextKey;

/**
 * 
 * @author henkej
 *
 */
public class ConnectionManager {

	/**
	 * we don't need this constructor to be public
	 */
	private ConnectionManager() {
	}

	/**
	 * get a new R connection
	 * 
	 * @param facesContext the faces context
	 * @return the connection
	 * @throws IOException on io errors
	 * @throws RserveException on rserve exceptions
	 */
	public static final RConnection getConnection(FacesContext facesContext) throws RserveException, IOException {
		return getConnection(facesContext.getExternalContext());
	}

	/**
	 * @param externalContext the external context
	 * @return the connection
	 * @throws RserveException on rserve exceptions
	 * @throws IOException on io errors
	 */
	public static final RConnection getConnection(ExternalContext externalContext) throws RserveException, IOException {
		Map<String, Object> applicationMap = externalContext.getApplicationMap();
		String host = (String) applicationMap.get(ContextKey.R_HOST.get());
		String portString = (String) applicationMap.get(ContextKey.R_PORT.get());
		String ssl = (String) applicationMap.get(ContextKey.R_SSL.get());
		return getConnection(host, portString, ssl);
	}
	
	/**
	 * @param ctx the servlet context
	 * @return the connection
	 * @throws RserveException on rserve exceptions
	 * @throws IOException on io errors
	 */
	public static final RConnection getConnection(ServletContext ctx) throws RserveException, IOException {
		String host = ctx.getInitParameter(ContextKey.R_HOST.get());
		String portString = ctx.getInitParameter(ContextKey.R_PORT.get());
		String ssl = ctx.getInitParameter(ContextKey.R_SSL.get());
		return getConnection(host, portString, ssl);
	}	

	/**
	 * get a new R connection
	 * 
	 * @param host the host
	 * @param portString the port as string
	 * @param ssl the ssl
	 * @return the connection
	 * @throws IOException on io errors
	 * @throws RserveException on rserve exceptions
	 */
	public static final RConnection getConnection(String host, String portString, String ssl)
			throws RserveException, IOException {
		Boolean sslMode = "true".equalsIgnoreCase(ssl);
		host = host == null ? "localhost" : host;
		Integer port = portString == null ? 6311 : Integer.valueOf(portString);
		return sslMode ? new SslRConnection(host, port) : new RConnection(host, port);
	}
}
