package square2.r.control;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * @author henkej
 *
 */
public class ExceptionRaiserForLog implements ExceptionRaiserInterface {
	private static final Logger LOGGER = LogManager.getLogger("Square2");

	@Override
	public void raise(Exception e, Object... attachment) {
		StringBuilder buf = new StringBuilder();
		buf.append(e.getMessage());
		for (Object o : attachment) {
			buf.append(o);
		}
		LOGGER.error(buf.toString(), e);
	}
}
