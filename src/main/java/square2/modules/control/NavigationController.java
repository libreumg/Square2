package square2.modules.control;

import javax.faces.context.FacesContext;

import square2.help.SquareFacesContext;
import square2.modules.control.help.Pages;
import square2.modules.control.help.Session;

/**
 * 
 * @author henkej
 *
 */
public abstract class NavigationController {
	private Pages[] allowed;

	/**
	 * create new navigation controller
	 * 
	 * @param allowed
	 *          the allowed pages
	 */
	public NavigationController(Pages... allowed) {
		this.allowed = allowed;
	}

	/**
	 * @return the current faces context
	 */
	public SquareFacesContext getFacesContext() {
		return (SquareFacesContext) FacesContext.getCurrentInstance();
	}

	/**
	 * navigate to a page that does not have to be registered
	 * 
	 * @param page
	 *          destination object
	 * @return page destination string
	 */
	public String navigateToPageless(Pages page) {
		return page.get();
	}

	/**
	 * navigate to destination referenced by page
	 * 
	 * @param facesContext
	 *          of this current function call
	 * @param page
	 *          destination object
	 * @return page destination string
	 */
	public String navigateTo(FacesContext facesContext, Pages page) {
		facesContext.getExternalContext().getSessionMap().put(Session.PAGE.get(), page);
		facesContext.getExternalContext().getSessionMap().remove(Session.SUBPAGE.get());
		return page.get();
	}

	/**
	 * navigate to subpage, but keep page as current one in session for main menu
	 * 
	 * @param facesContext
	 *          to store pages into session
	 * @param page
	 *          main menu session entry
	 * @param subPage
	 *          destination object
	 * @return page of destination string
	 */
	public String navigateTo(FacesContext facesContext, Pages page, Pages subPage) {
		facesContext.getExternalContext().getSessionMap().put(Session.PAGE.get(), page);
		facesContext.getExternalContext().getSessionMap().put(Session.SUBPAGE.get(), subPage);
		return subPage.get();
	}

	/**
	 * call getIsCurrentController with current facesContext
	 * 
	 * @return true or false
	 */
	public abstract boolean getIsCurrentController();

	/**
	 * returns true if current page is rendered by current module
	 * 
	 * @param facesContext
	 *          to lookup page
	 * 
	 * @return true or false
	 */
	public Boolean getIsCurrentController(FacesContext facesContext) {
		Pages page = (Pages) facesContext.getExternalContext().getSessionMap().get(Session.PAGE.get());
		for (Pages p : allowed) {
			if (p.equals(page)) {
				return true;
			}
		}
		return false;
	}
}
