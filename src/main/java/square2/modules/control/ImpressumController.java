package square2.modules.control;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import square2.help.SquareFacesContext;
import square2.modules.control.help.Pages;

/**
 * 
 * @author henkej
 *
 */
@Named
@RequestScoped
public class ImpressumController extends NavigationController {
	/**
	 * go to impressum page
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * 
	 * @return navigation destination
	 */
	public String toImpressum(SquareFacesContext facesContext) {
		return navigateTo(facesContext, Pages.IMPRESSUM);
	}

	@Override
	public boolean getIsCurrentController() {
		return false;
	}
}
