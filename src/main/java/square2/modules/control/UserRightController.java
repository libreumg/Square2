package square2.modules.control;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import square2.help.SquareFacesContext;
import square2.modules.control.help.Pages;
import square2.modules.model.AclBean;

/**
 * 
 * @author henkej
 *
 */
public abstract class UserRightController extends NavigationController {
	private static final Logger LOGGER = LogManager.getLogger(UserRightController.class);

	private Map<Integer, Map<Integer, AclBean>> map;
	private Boolean forceRefresh;

	/**
	 * create a new user right controller
	 * 
	 * @param pages
	 *          the valid pages
	 */
	public UserRightController(Pages... pages) {
		super(pages);
		forceRefresh = false;
	}

	/**
	 * load map of rights from db
	 * 
	 * @return the map of rights
	 */
	public abstract Map<Integer, Map<Integer, AclBean>> refreshMap();

	/**
	 * force map to be refreshed
	 * 
	 * @return true
	 */
	public boolean forceRefresh() {
		this.forceRefresh = true;
		return true;
	}

	/**
	 * check if this privilege can read
	 * 
	 * @param rightId
	 *          id of the privilege
	 * @param facesContext
	 *          the context of this function call
	 * @return navigation destination
	 */
	public Boolean canRead(Integer rightId, SquareFacesContext facesContext) {
		return getAclBean(rightId, facesContext).getRead();
	}

	/**
	 * check if this privilege can write
	 * 
	 * @param rightId
	 *          id of the privilege
	 * @param facesContext
	 *          the context of this function call
	 * @return navigation destination
	 */
	public Boolean canWrite(Integer rightId, SquareFacesContext facesContext) {
		return getAclBean(rightId, facesContext).getWrite();
	}

	/**
	 * check if this privilege can execute
	 * 
	 * @param rightId
	 *          id of the privilege
	 * @param facesContext
	 *          the context of this function call
	 * @return navigation destination
	 */
	public Boolean canExecute(Integer rightId, SquareFacesContext facesContext) {
		return getAclBean(rightId, facesContext).getExecute();
	}

	private AclBean getAclBean(Integer rightId, SquareFacesContext facesContext) {
		if (map == null || forceRefresh) {
			map = refreshMap();
			forceRefresh = false;
		}
		if (map == null) {
			LOGGER.warn("refreshMap returned null");
			return new AclBean();
		}
		Map<Integer, AclBean> innerMap = map.get(facesContext.getUsnr());
		if (innerMap != null) {
			AclBean bean = innerMap.get(rightId);
			if (bean == null) {
				return new AclBean();
			} else {
				return bean;
			}
		} else {
			return new AclBean();
		}
	}
}
