package square2.modules.control;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import square2.help.ThemeBean;
import square2.modules.control.help.Pages;
import square2.modules.model.ApplicationBean;
import square2.modules.model.LoginModel;

/**
 * 
 * @author henkej
 * 
 */
@Named
@RequestScoped
public class LoginController extends NavigationController {
	@Inject
	@Named(value = "loginModel")
	private LoginModel model;

	@Inject
	@Named(value = "applicationBean")
	private ApplicationBean applicationBean;

	@Inject
	@Named(value = "themeBean")
	private ThemeBean themeBean;

	/**
	 * create new login controller
	 */
	public LoginController() {
		super(Pages.PROFILE, Pages.LOGIN, Pages.MAIN);
	}

	/**
	 * load maintenance information
	 * 
	 * @return navigation destination
	 */
	public String toLogin() {
		return navigateTo(getFacesContext(), Pages.LOGIN);
	}

	/**
	 * check username and password and navigate to internal page if correct
	 * 
	 * @return navigation destination
	 */
	public String doLogin() {
		boolean result = model.doLogin(getFacesContext(), applicationBean);
		return result ? navigateTo(getFacesContext(), Pages.MAIN) : toLogin();
	}

	/**
	 * clear session and go to login page
	 * 
	 * @return navigation destination
	 */
	public String doLogout() {
		model.doLogout(getFacesContext(), applicationBean);
		return toLogin();
	}

	/**
	 * nagivate to change password
	 * 
	 * @return navigation destination
	 */
	public String toPassword() {
		model.toChangePassword(getFacesContext());
		return navigateTo(getFacesContext(), Pages.PASSWORD);
	}

	/**
	 * nagivate to change password
	 * 
	 * @return navigation destination
	 */
	public String toProfile() {
		model.toChangePassword(getFacesContext());
		return navigateTo(getFacesContext(), Pages.PROFILE);
	}

	/**
	 * change password
	 * 
	 * @return navigation destination
	 */
	public String doChangePassword() {
		model.changePassword(getFacesContext());
		return navigateTo(getFacesContext(), Pages.PROFILE);
	}

	/**
	 * set configuration param
	 * 
	 * @param key
	 *          the key
	 * @return navigation destination
	 */
	public String doSetConfigParam(String key) {
		model.setConfigParam(getFacesContext(), key, getFacesContext().getUsnr(), getFacesContext().getProfile().getConfig(key), themeBean);
		model.setConfigValue(null); // for next changes, free variable
		return toProfile();
	}

	/**
	 * unset configuration param
	 * 
	 * @param key
	 *          the key
	 * @return navigation destination
	 */
	public String doUnsetConfigParam(String key) {
		model.setConfigParam(getFacesContext(), key, getFacesContext().getUsnr(), null, themeBean);
		return toProfile();
	}

	@Override
	public boolean getIsCurrentController() {
		return getIsCurrentController(getFacesContext());
	}
}
