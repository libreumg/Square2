package square2.modules.control.r;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;

import square2.help.ContextKey;
import square2.help.SquareFacesContext;
import square2.r.control.RExecutor;

/**
 * 
 * @author henkej
 *
 */
public class RServerCall {
	private static final Logger LOGGER = LogManager.getLogger(RServerCall.class);

	private SquareFacesContext facesContext;

	/**
	 * create a new r server call
	 * 
	 * @param facesContext
	 *          the context of this call
	 */
	public RServerCall(SquareFacesContext facesContext) {
		this.facesContext = facesContext;
	}

	/**
	 * @return the html content of the performance or an error message
	 */
	public String callPerformance() { // TODO: move to SquareControlGateway
		String configFile = (String) facesContext.getExternalContext().getApplicationMap()
				.get(ContextKey.RSERVERACCESS.get());
		StringBuilder buf = new StringBuilder();
		buf.append("square.generate.performance.statistics(dbconfigfile = \"");
		buf.append(configFile);
		buf.append("\", output_format = \"html_document\")"); // flexdashboard::flex_dashboard\")");
		String result = "before try catch block";
		try {
			RExecutor executor = new RExecutor(facesContext);
			executor.runTryEval("library(squareControl)");
			REXP rexp = executor.runTryEval(buf.toString());
			result = rexp.asString();
		} catch (IOException | NumberFormatException | DataAccessException | REXPMismatchException | REngineException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return e.getMessage();
		}
		return result;
	}
}
