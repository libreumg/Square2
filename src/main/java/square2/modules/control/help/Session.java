package square2.modules.control.help;

/**
 * 
 * @author henkej
 *
 */
public enum Session {
	/**
	 * profile
	 */
	PROFILE("profile"),
	/**
	 * page
	 */
	PAGE("page"),
	/**
	 * subpage
	 */
	SUBPAGE("subpage"),
	/**
	 * active_panel
	 */
	ACTIVE_PANEL("active_panel"), 
	/**
	 * jooq
	 */
	JOOQ("jooq");

	private final String key;

	private Session(String key) {
		this.key = key;
	}

	/**
	 * @return the value of the enum
	 */
	public String get() {
		return key;
	}
}
