package square2.modules.control.help;

/**
 * 
 * @author henkej
 *
 */
public class ValidationException extends Exception {
	private static final long serialVersionUID = 1L;

	/**
	 * create a new validation exception
	 * 
	 * @param message
	 *          the error message
	 */
	public ValidationException(String message) {
		super(message);
	}
}
