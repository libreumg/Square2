package square2.modules.control;

import java.util.Date;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;

import de.ship.dbppsquare.square.enums.EnumAccesslevel;
import de.ship.dbppsquare.square.tables.records.TMetadatatypeRecord;
import square2.help.SquareFacesContext;
import square2.modules.control.help.Pages;
import square2.modules.control.help.Session;
import square2.modules.model.AclBean;
import square2.modules.model.ApplicationBean;
import square2.modules.model.ProfileBean;
import square2.modules.model.admin.AdminModel;
import square2.modules.model.admin.FunctioncategoryBean;
import square2.modules.model.admin.ImpressumBean;
import square2.modules.model.admin.MaintenanceBean;
import square2.modules.model.admin.UsergroupBean;
import square2.modules.model.statistic.FunctioninputtypeBean;
import square2.modules.model.study.MetadatatypeBean;

/**
 * 
 * @author henkej
 * 
 */
@Named
@RequestScoped
public class AdminController extends SubmenuController {
	private static final Logger LOGGER = LogManager.getLogger(AdminController.class);

	@Inject
	@Named(value = "adminModel")
	private AdminModel model;

	@Inject
	@Named(value = "applicationBean")
	private ApplicationBean applicationBean;

	/**
	 * generate new admin controller
	 */
	public AdminController() {
		super(Pages.ADMIN_WELCOME, Pages.ADMIN_USERS, Pages.ADMIN_SESSION, Pages.ADMIN_LOGINS,
				Pages.ADMIN_FUNCTIONOUTPUTTYPE, Pages.ADMIN_APPLROLES, Pages.ADMIN_APPLRIGHTS);
	}

	/**
	 * navigate to the page
	 * 
	 * @param page
	 *          the page
	 * @return navigation destinatino
	 */
	public String navigateTo(Pages page) {
		setActivePanel(page.get(), getFacesContext());
		return super.navigateTo(getFacesContext(), Pages.ADMIN_WELCOME, page);
	}

	/**
	 * @return true if on the users submenu
	 */
	public Boolean getOnUsers() {
	  return (getOnPageUsers() || getOnPageShowSession() || getOnPageShowLogins() || getOnPageApplroles()
        || getOnPageUsergroup());
	}
	
	/**
	 * @return true if on the config submenu
	 */
	public Boolean getOnConfig() {
	  return (getOnPageFunctioninputtype() || getOnPageFunctionoutputtype() || getOnPageSnippletadmin() || getOnPageFunctioncategory() || getOnPageMetadatatype());
	}
	
	/**
	 * @return style submenu users
	 * @deprecated use getOnUsers instead
	 */
	@Deprecated
	public String getStyleSubmenuUsers() {
		return (getOnPageUsers() || getOnPageShowSession() || getOnPageShowLogins() || getOnPageApplroles()
				|| getOnPageUsergroup()) ? "menuactivated" : "";
	}

	/**
	 * @return style submenu config
	 * @deprecated use getOnConfig instead
	 */
	@Deprecated
	public String getStyleSubmenuConfig() {
		return (getOnPageFunctioninputtype() || getOnPageFunctionoutputtype() || getOnPageSnippletadmin()) ? "menuactivated" : "";
	}

	/**
	 * @return style submenu test
	 * @deprecated use getOnPageTest instead
	 */
	@Deprecated
	public String getStyleSubmenuTest() {
		return getOnPageTest() ? "menuactivated" : "";
	}

	/**
	 * @return true or false
	 */
	public boolean getOnPageTest() {
		return Pages.ADMIN_FILEUPLOAD.equals(getFacesContext().getSessionMapValue(Session.SUBPAGE.get()));
	}

	/**
	 * @return true or false
	 */
	public boolean getOnPageUsergroup() {
		return Pages.ADMIN_USERGROUP.equals(getFacesContext().getSessionMapValue(Session.SUBPAGE.get()));
	}

	/**
	 * @return true or false
	 */
	public boolean getOnPageUsers() {
		return Pages.ADMIN_USERS.equals(getFacesContext().getSessionMapValue(Session.SUBPAGE.get()));
	}

	/**
	 * @return true or false
	 */
	public boolean getOnPageShowSession() {
		return Pages.ADMIN_SESSION.equals(getFacesContext().getSessionMapValue(Session.SUBPAGE.get()));
	}

	/**
	 * @return true or false
	 */
	public boolean getOnPageShowLogins() {
		return Pages.ADMIN_LOGINS.equals(getFacesContext().getSessionMapValue(Session.SUBPAGE.get()));
	}

	/**
	 * @return true or false
	 */
	public boolean getOnPageFunctioninputtype() {
		return Pages.ADMIN_FUNCTIONINPUTTYPE.equals(getFacesContext().getSessionMapValue(Session.SUBPAGE.get()));
	}

	/**
	 * @return true or false
	 */
	public boolean getOnPageFunctionoutputtype() {
		return Pages.ADMIN_FUNCTIONOUTPUTTYPE.equals(getFacesContext().getSessionMapValue(Session.SUBPAGE.get()));
	}

	/**
	 * @return true or false
	 */
	public boolean getOnPageSnippletadmin() {
		return Pages.ADMIN_SNIPPLET.equals(getFacesContext().getSessionMapValue(Session.SUBPAGE.get()));
	}

	/**
	 * @return true or false
	 */
	public boolean getOnPageApplroles() {
		return Pages.ADMIN_APPLROLES.equals(getFacesContext().getSessionMapValue(Session.SUBPAGE.get()));
	}

	/**
	 * @return true or false
	 */
	public boolean getOnPageMaintenanceList() {
		return Pages.ADMIN_MAINTENANCE_LIST.equals(getFacesContext().getSessionMapValue(Session.SUBPAGE.get()));
	}

	/**
	 * @return true or false
	 */
	public boolean getOnPageMaintenanceAdd() {
		return Pages.ADMIN_MAINTENANCE_EDIT.equals(getFacesContext().getSessionMapValue(Session.SUBPAGE.get()));
	}

	/**
	 * @return true or false
	 */
	public boolean getOnPageImpressum() {
		return Pages.ADMIN_IMPRESSUM.equals(getFacesContext().getSessionMapValue(Session.SUBPAGE.get()));
	}

	/**
	 * @return true or false
	 */
	public boolean getOnPageFunctioncategory() {
		return Pages.ADMIN_FUNCTIONCATEGORY_LIST.equals(getFacesContext().getSessionMapValue(Session.SUBPAGE.get()));
	}

	/**
	 * @return true or false
	 */
	public boolean getOnPageMetadatatype() {
		return Pages.ADMIN_METADATATYPE_LIST.equals(getFacesContext().getSessionMapValue(Session.SUBPAGE.get()));
	}

	/**
	 * navigate to main admin page
	 * 
	 * @return navigation destination
	 */
	public String toWelcome() {
		return super.navigateTo(getFacesContext(), Pages.ADMIN_WELCOME);
	}

	/**
	 * the maintenance view
	 * 
	 * @return navigation destination
	 */
	public String toListMaintenance() {
		model.loadMaintenances(getFacesContext());
		return this.navigateTo(Pages.ADMIN_MAINTENANCE_LIST);
	}

	/**
	 * @param bean
	 *          of the maintenance
	 * @return result of {@link #toListMaintenance()}
	 */
	public String toEditMaintenance(MaintenanceBean bean) {
		return toAddMaintenance(bean);
	}

	/**
	 * the maintenance view for maintenance
	 * 
	 * @param bean
	 *          of the maintenance
	 * @return result of {@link #toListMaintenance()}
	 */
	public String toAddMaintenance(MaintenanceBean bean) {
		model.setMaintenance(bean);
		return toAddMaintenance();
	}

	/**
	 * navigate to impressum administration page
	 * 
	 * @return navigation destination
	 */
	public String toImpressum() {
		model.loadImpressum(getFacesContext());
		return this.navigateTo(Pages.ADMIN_IMPRESSUM);
	}

	/**
	 * navigate to function category page
	 * 
	 * @return navigation destination
	 */
	public String toFunctioncategoryList() {
		model.toFunctioncategoryList(getFacesContext());
		return this.navigateTo(Pages.ADMIN_FUNCTIONCATEGORY_LIST);
	}

	/**
	 * navigate to edit function category page
	 * 
	 * @param bean
	 *          function category bean
	 * @return navigation destination
	 */
	public String toEditFunctioncategory(FunctioncategoryBean bean) {
		model.toEditFunctioncategory(getFacesContext(), bean);
		return this.navigateTo(Pages.ADMIN_FUNCTIONCATEGORY_EDIT);
	}

	/**
	 * navigate to edit function category page (adding is editing with an empty id)
	 * 
	 * @return navigation destination
	 */
	public String toAddFunctioncategory() {
		model.toEditFunctioncategory(getFacesContext(), new FunctioncategoryBean(null));
		return this.navigateTo(Pages.ADMIN_FUNCTIONCATEGORY_EDIT);
	}

	/**
	 * navigate to metadatatyle list page
	 * 
	 * @return navigation destination
	 */
	public String toMetadatatypeList() {
		model.toMetadatatypeList(getFacesContext());
		return this.navigateTo(Pages.ADMIN_METADATATYPE_LIST);
	}

	/**
	 * navigate to edit metadatatype page
	 * 
	 * @param bean
	 *          metadatatype bean
	 * @return navigation destination
	 */
	public String toEditMetadatatype(MetadatatypeBean bean) {
		model.toEditMetadatatype(getFacesContext(), bean);
		return this.navigateTo(Pages.ADMIN_METADATATYPE_EDIT);
	}

	/**
	 * navigate to edit function category page (adding is editing with an empty id)
	 * 
	 * @return navigation destination
	 */
	public String toAddMetadatatype() {
		model.toEditMetadatatype(getFacesContext(), new MetadatatypeBean(null));
		return this.navigateTo(Pages.ADMIN_METADATATYPE_EDIT);
	}

	/**
	 * upsert function category
	 * 
	 * @return navigation destination
	 */
	public String doMergeFunctioncategory() {
		boolean result = model.doUpsertFunctioncategory(getFacesContext());
		return result ? toFunctioncategoryList() : toEditFunctioncategory(model.getFunctioncategory());
	}

	/**
	 * upsert metadatatype
	 * 
	 * @return navigation destination
	 */
	public String doMergeMetadatatype() {
		boolean result = model.doUpsertMetadatatype(getFacesContext());
		return result ? toMetadatatypeList() : toEditMetadatatype(model.getMetadatatypeBean());
	}

	/**
	 * remove function category from database
	 * 
	 * @param id
	 *          the id of the function category
	 * 
	 * @return navigation destination
	 */
	public String doRemoveFunctioncategory(Integer id) {
		model.doDeleteFunctioncategory(getFacesContext(), id);
		return toFunctioncategoryList();
	}

	/**
	 * remove function category from database
	 * 
	 * @param bean
	 *          the metadatatype bean
	 * 
	 * @return navigation destination
	 */
	public String doRemoveMetadatatype(TMetadatatypeRecord bean) {
		model.doDeleteMetadatatype(getFacesContext(), bean);
		return toMetadatatypeList();
	}

	/**
	 * update impressum and navigate to view mode
	 * 
	 * @param bean
	 *          the impressum bean
	 * 
	 * @return navigation destination
	 */
	public String doUpdateImpressum(ImpressumBean bean) {
		Boolean result = model.upsertImpressum(getFacesContext(), bean);
		return result ? this.navigateTo(Pages.IMPRESSUM) : this.navigateTo(Pages.ADMIN_IMPRESSUM);
	}

	/**
	 * the fileupload view
	 * 
	 * @return navigation destination
	 */
	public String toFileUpload() {
		return this.navigateTo(Pages.ADMIN_FILEUPLOAD);
	}

	/**
	 * @return navigation destination
	 */
	public String doFileUpload() {
		model.doFileUpload(getFacesContext());
		return toFileUpload();
	}

	/**
	 * add new empty maintenance bean
	 * 
	 * @return navigation destination
	 */
	public String doAddMaintenance() {
		return toAddMaintenance(new MaintenanceBean(null, "", new Date(), new Date()));
	}

	/**
	 * remove maintenance from db
	 * 
	 * @param pk
	 *          the maintenance id
	 * @return navigation destination
	 */
	public String doRemoveMaintenance(Integer pk) {
		model.removeMaintenance(getFacesContext(), pk);
		return toListMaintenance();
	}

	/**
	 * update current maintenance
	 * 
	 * @return navigation destination
	 */
	public String doMergeMaintenance() {
		if (model.mergeMaintenance(getFacesContext())) {
			model.setMaintenance(null);
		}
		return toListMaintenance();
	}

	/**
	 * show users with login to square2
	 * 
	 * @return navigation destination
	 */
	public String toUsers() {
		model.showUsers(getFacesContext());
		return this.navigateTo(Pages.ADMIN_USERS);
	}

	/**
	 * show all user groups
	 * 
	 * @return navigation destination
	 */
	public String toUsergroup() {
		model.loadUsergroups(getFacesContext());
		return this.navigateTo(Pages.ADMIN_USERGROUP);
	}

	/**
	 * delete user group combination
	 * 
	 * @param group
	 *          of combination to be used
	 * @param user
	 *          of combination to be used
	 * @return destination of page in any case
	 */
	public String doRemoveUserFromGroup(UsergroupBean group, ProfileBean user) {
		model.removeUserFromGroup(getFacesContext(), group, user);
		return toUsergroup();
	}

	/**
	 * add user to group
	 * 
	 * @return navigation destination
	 */
	public String doAddUserToGroup() {
		model.addUserToGroup(getFacesContext());
		return toUsergroup();
	}

	/**
	 * add user
	 * 
	 * @return navigation destination
	 */
	public String doAddUser() {
	  boolean result = model.addUser(getFacesContext());
	  return result ? toUsers() : "";
	}
	
	/**
	 * add group
	 * 
	 * @return navigation destination
	 */
	public String doAddGroup() {
		boolean result = model.addGroup(getFacesContext());
		return result ? toUsergroup() : "";
	}

	/**
	 * delete group
	 * 
	 * @param bean
	 *          contains the group to be deleted
	 * @return result of {@link #toUsergroup()}
	 */
	public String doDeleteGroup(UsergroupBean bean) {
		model.deleteGroup(getFacesContext(), bean);
		return toUsergroup();
	}

	/**
	 * manipulate shipplet styles
	 * 
	 * @return navigation destination
	 */
	public String toSnippletadmin() {
		return model.resetSnippletadmin(getFacesContext()) ? this.navigateTo(Pages.ADMIN_SNIPPLET) : toWelcome();
	}

	/**
	 * run an R test
	 * 
	 * @param facesContext
	 *          the faces context
	 * @return navigation destination
	 */
	public String doRunRTest(SquareFacesContext facesContext) {
		model.testR(facesContext);
		setActivePanel("rtest", facesContext);
		return toWelcome();
	}

	/**
	 * @param id
	 *          the snipplet type id
	 * @return navigation destination
	 */
	public String doUpdateSnipplettype(Integer id) {
		model.updateSnipplettype(id, getFacesContext());
		return toSnippletadmin();
	}

	/**
	 * remove user from t_qs_login
	 * 
	 * @param usnr
	 *          id of the user
	 * @return navigation destination
	 */
	public String doRemoveUser(Integer usnr) {
		model.removeUser(usnr, getFacesContext());
		return toUsers();
	}

  /**
   * remove user from t_person
   * 
   * @return navigation destination
   */
  public String doDeleteUser() {
    model.deleteUser(getFacesContext());
    return toUsers();
  }

	/**
	 * reload translations
	 * 
	 * @return navigation destination
	 */
	public String doReloadTranslations() {
		model.reloadTranslations(getFacesContext());
		return toSession();
	}

	/**
	 * show session
	 * 
	 * @return navigation destination
	 */
	public String toSession() {
		model.showSession(applicationBean, getFacesContext());
		return navigateTo(Pages.ADMIN_SESSION);
	}

	/**
	 * show all user logins
	 * 
	 * @return navigation destination
	 */
	public String showLogins() {
		model.showLogins(applicationBean);
		return navigateTo(Pages.ADMIN_LOGINS);
	}

	/**
	 * show all function input types
	 * 
	 * @param bean
	 *          the function input type bean
	 * 
	 * @return navigation destination
	 */
	public String toFunctioninputtype(FunctioninputtypeBean bean) {
		model.toFunctioninputtype(getFacesContext(), bean);
		return navigateTo(Pages.ADMIN_FUNCTIONINPUTTYPE);
	}

	/**
	 * update function input type
	 * 
	 * @return navigation destination
	 */
	public String doUpdateFunctioninputtype() {
		boolean result = model.updateFunctioninputtype(getFacesContext());
		return result ? toFunctioninputtype(null) : toFunctioninputtype(model.getFunctioninputtype());
	}

	/**
	 * show all function output types
	 * 
	 * @return navigation destination
	 */
	public String toFunctionoutputtype() {
		model.toFunctionoutputtype(getFacesContext());
		return navigateTo(Pages.ADMIN_FUNCTIONOUTPUTTYPE);
	}

	/**
	 * set the log level
	 * 
	 * @param e
	 *          the value change event
	 */
	public void doSetLoglevel(ValueChangeEvent e) {
		String newLevel = (String) e.getNewValue();
		setLogLevelDirect(newLevel);
	}

	/**
	 * add function output type
	 * 
	 * @return navigation destination
	 */
	public String doAddFunctionoutputtype() {
		model.addFunctionoutputtype(getFacesContext());
		return toFunctionoutputtype();
	}

	/**
	 * delete function output type
	 * 
	 * @param pk
	 *          the id of the function output type
	 * @return navigation destination
	 */
	public String doDeleteFunctionoutputtype(Integer pk) {
		model.deleteFunctionoutputtype(pk, getFacesContext());
		return toFunctionoutputtype();
	}

	/**
	 * create login from existing user
	 * 
	 * @return navigation destination
	 */
	public String doCreateLogin() {
		model.createLogin(getFacesContext());
		return toUsers();
	}

	/**
	 * add days to expire date
	 * 
	 * @param usnr
	 *          if of user to extend login time
	 * @param days
	 *          the number of days that this account should be extended to
	 * @return result of {@link #toUsers()}
	 */
	public String doAddExpire(Integer usnr, Integer days) {
		model.addExpire(getFacesContext(), usnr, days);
		return toUsers();
	}

	/**
	 * set the expire date
	 * 
	 * @param usnr
	 *          id of the user to set the login time
	 * @param daysFromNow
	 *          the number of days from now, might be negative
	 * @return result of {@link #toUsers()}
	 */
	public String doSetExpire(Integer usnr, Integer daysFromNow) {
		model.setExpire(getFacesContext(), usnr, daysFromNow);
		return toUsers();
	}

	/**
	 * @return navigation destination
	 */
	public String toApplroles() {
		model.showApplroles(getFacesContext());
		model.getAcl().setAcl(EnumAccesslevel.rwx);
		return navigateTo(Pages.ADMIN_APPLROLES);
	}

	/**
	 * @return navigation destination
	 */
	public String toAddMaintenance() {
		return navigateTo(Pages.ADMIN_MAINTENANCE_EDIT);
	}

	/**
	 * remove role of user and reload list
	 * 
	 * @param usnr
	 *          the id of the user
	 * @param group
	 *          the id of the group
	 * @param rightId
	 *          the id of the privilege
	 * @return navigation destination
	 */
	public String doRemoveApplrole(Integer usnr, Integer group, Integer rightId) {
		model.removeApplrole(usnr, group, rightId, getFacesContext());
		return toApplroles();
	}

	/**
	 * add role for user
	 * 
	 * @return navigation destination
	 */
	public String doAddApplrole() {
		model.addApplrole(getFacesContext());
		return toApplroles();
	}

	/**
	 * @return the application bean
	 */
	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	/**
	 * @param applicationBean
	 *          the application bean
	 */
	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	/**
	 * @return the model
	 */
	public AdminModel getModel() {
		return model;
	}

	@Override
	public Map<Integer, Map<Integer, AclBean>> refreshMap() {
		return null;
	}

	@Override
	public boolean getIsCurrentSubController() {
		return getIsCurrentSubController(getFacesContext());
	}

	/**
	 * @return the log level
	 */
	public String getLogLevel() {
		LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
		Configuration config = ctx.getConfiguration();
		LoggerConfig loggerConfig = config.getLoggerConfig(LogManager.ROOT_LOGGER_NAME);
		return loggerConfig.getLevel().name();
	}

	/**
	 * check, if level is current loglevel
	 * 
	 * @param level
	 *          the level
	 * @param successCssClass
	 *          the css class on success
	 * @param errorCssClass
	 *          the css class on error
	 * @return the successCssClass or the errorCssClass
	 */
	public String isLogLevel(String level, String successCssClass, String errorCssClass) {
		String currentLevel = getLogLevel();
		LOGGER.debug("check, if {} is {} (case insensitive)", level, currentLevel);
		return currentLevel.equalsIgnoreCase(level) ? successCssClass : errorCssClass;
	}

	/**
	 * @param level
	 *          the log level
	 * @return an empty String to stay on the current location
	 */
	public String setLogLevelDirect(String level) {
		LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
		Configuration config = ctx.getConfiguration();
		LoggerConfig loggerConfig = config.getLoggerConfig(LogManager.ROOT_LOGGER_NAME);
		loggerConfig.setLevel(Level.getLevel(level));
		ctx.updateLoggers();
		getFacesContext().notifyInformation("loglevel set to {0}", level);
		return "";
	}

	/**
	 * @param level
	 *          the log level
	 */
	public void setLogLevel(String level) {
		// not needed because of ajax
	}

	@Override
	public boolean getIsCurrentController() {
		return getIsCurrentController(getFacesContext());
	}
}
