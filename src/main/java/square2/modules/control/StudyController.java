package square2.modules.control;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;

import square2.db.control.converter.EnumConverter;
import square2.help.SquareFacesContext;
import square2.modules.control.help.Pages;
import square2.modules.model.AclBean;
import square2.modules.model.LocaleBean;
import square2.modules.model.ModelException;
import square2.modules.model.PrivilegeBean;
import square2.modules.model.study.DeptmetaBean;
import square2.modules.model.study.ElementBean;
import square2.modules.model.study.MetarefBean;
import square2.modules.model.study.MissingBean;
import square2.modules.model.study.MissinglistBean;
import square2.modules.model.study.StudyModel;
import square2.modules.model.study.VariableAttributeBean;
import square2.modules.model.study.VariableBean;

/**
 * 
 * @author henkej
 *
 */
@Named
@RequestScoped
public class StudyController extends SubmenuController {
  private static final Logger LOGGER = LogManager.getLogger(StudyController.class);

  @Inject
  @Named(value = "studyModel")
  private StudyModel model;

  @Inject
  @Named(value = "lang")
  private LocaleBean locale;

  /**
   * generate new study controller
   */
  public StudyController() {
    super(Pages.STUDY_WELCOME, Pages.STUDY_EDIT, Pages.STUDY_SHOW, Pages.STUDY_USER, Pages.STUDY_METADATA,
        Pages.STUDY_EDIT_VARIABLE, Pages.STUDY_EDIT_DEPARTMENT, Pages.STUDY_DEPTMETA, Pages.STUDY_EDITDEPTMETA,
        Pages.STUDY_ADD_DEPARTMENT, Pages.STUDY_ADD_VARIABLE, Pages.STUDY_MISSINGLIST, Pages.STUDY_MISSINGLIST_EDIT);
  }

  /**
   * @return navigation destination
   */
  public String toWelcome() {
    model.resetWelcome(getFacesContext());
    return navigateTo(getFacesContext(), Pages.STUDY_WELCOME);
  }

  /**
   * @param elementId id of the current element
   * @return navigation destination
   */
  public String toWelcome(Integer elementId) {
    model.resetUploadBean(getFacesContext());
    model.resetItem(elementId, getFacesContext().getUsnr(), getFacesContext());
    return navigateTo(getFacesContext(), Pages.STUDY_WELCOME);
  }

  /**
   * navigate to edit page
   * 
   * @param bean the element bean
   * @return navigation destination
   */
  public String toEdit(ElementBean bean) {
    if (bean.getIsVariable()) {
      model.toEdit(getFacesContext(), bean);
    } else {
      try {
        model.reloadDepartment(bean.getId(), getFacesContext());
      } catch (DataAccessException | ModelException e) {
        getFacesContext().notifyException(e);
        LOGGER.error(e.getMessage(), e);
      }
    }
    model.setInfo(getFacesContext());
    return navigateTo(getFacesContext(), Pages.STUDY_EDIT);
  }

  /**
   * navigate to edit page
   * 
   * @param bean the variable bean
   * @return navigation destination
   */
  public String toEdit(VariableBean bean) {
  	String label = getFacesContext().selectTranslation(bean.getTranslation());
    ElementBean eBean = new ElementBean(bean.getUniqueName(), bean.getKeyElement(), bean.getRightId(),
        bean.getKeyParentElement(), bean.getTranslation(), label, bean.getVarOrder(), bean.getShipOrder(),
        bean.getStudy(), bean.getStudygroup(), bean.getFkMissinglist(), bean.getLocales());
    eBean.setIsVariable(true);
    return toEdit(eBean);
  }

  /**
   * navigate to show
   * 
   * @param bean the bean
   * @return navigation destination
   */
  public String toShow(VariableBean bean) {
  	String label = getFacesContext().selectTranslation(bean.getTranslation());
    ElementBean eBean = new ElementBean(bean.getUniqueName(), bean.getKeyElement(), bean.getRightId(),
        bean.getKeyParentElement(), bean.getTranslation(), label, bean.getVarOrder(), bean.getShipOrder(),
        bean.getStudy(), bean.getStudygroup(), bean.getFkMissinglist(), bean.getLocales());
    eBean.setIsVariable(true);
    return toShow(eBean);
  }

  /**
   * navigate to show
   * 
   * @param bean the bean
   * @return navigation destination
   */
  public String toShow(ElementBean bean) {
    if (bean.getIsVariable()) {
      model.toItem(bean.getId(), getFacesContext());
    } else {
      try {
        model.reloadDepartment(bean.getId(), getFacesContext());
      } catch (DataAccessException | ModelException e) {
        getFacesContext().notifyException(e);
        LOGGER.error(e.getMessage(), e);
      }
    }
    model.setInfo(getFacesContext());
    return navigateTo(getFacesContext(), Pages.STUDY_SHOW);
  }

  /**
   * navigate to user
   * 
   * @param bean the bean
   * @return navigation destination
   */
  public String toUser(VariableBean bean) {
  	String label = getFacesContext().selectTranslation(bean.getTranslation());
    ElementBean eBean = new ElementBean(bean.getUniqueName(), bean.getKeyElement(), bean.getRightId(),
        bean.getKeyParentElement(), bean.getTranslation(), label, bean.getVarOrder(), bean.getShipOrder(),
        bean.getStudy(), bean.getStudygroup(), bean.getFkMissinglist(), bean.getLocales());
    eBean.setIsVariable(true);
    return toUser(eBean);
  }

  /**
   * navigate to user
   * 
   * @param bean the bean
   * @return navigation destination
   */
  public String toUser(ElementBean bean) {
    if (bean.getIsVariable()) {
      model.toItem(bean.getId(), getFacesContext());
    } else {
      try {
        model.reloadDepartment(bean.getId(), getFacesContext());
      } catch (DataAccessException | ModelException e) {
        getFacesContext().notifyException(e);
        LOGGER.error(e.getMessage(), e);
      }
    }
    model.setInfo(getFacesContext());
    return navigateTo(getFacesContext(), Pages.STUDY_USER);
  }

  /**
   * navigate to metadata page
   * 
   * @return navigation destination
   */
  public String toMetadata() {
    model.loadReferenceVariables(getFacesContext());
    return navigateTo(getFacesContext(), Pages.STUDY_METADATA);
  }

  /**
   * navigate to metadata page
   * 
   * @param parentId id of the parent element
   * @return navigation destination
   */
  public String toMetadata(Integer parentId) {
    model.loadVariablesUnder(parentId, getFacesContext());
    return navigateTo(getFacesContext(), Pages.STUDY_METADATA);
  }

  /**
   * navigate to metadata directly
   * 
   * @param bean the element bean
   * @return navigation destination
   */
  public String toMetadata(ElementBean bean) {
    toShow(bean);
    return toMetadata();
  }
  
  /**
   * navigate to deptmeta page
   * 
   * @param parent the parent element bean
   * @return navigation destination
   */
  public String toDeptmeta(ElementBean parent) {
    model.summarizeMetadata(parent, null, getFacesContext());
    return navigateTo(getFacesContext(), Pages.STUDY_DEPTMETA);
  }

  /**
   * navigate to deptmeta page
   * 
   * @param parent the parent element bean
   * @param variablelistParentId the id of the variable list's parent
   * @return navigation destination
   */
  public String toDeptmeta(ElementBean parent, Integer variablelistParentId) {
    model.summarizeMetadata(parent, variablelistParentId, getFacesContext());
    return navigateTo(getFacesContext(), Pages.STUDY_DEPTMETA);
  }

  /**
   * navigate to edit page of deptmeta
   * 
   * @param parent the parent element bean
   * @param deptmetaBean the deptmeta bean
   * @return navigation destination
   */
  public String toEditDeptmeta(ElementBean parent, DeptmetaBean deptmetaBean) {
    model.loadCompleteDeptmeta(parent, deptmetaBean, getFacesContext());
    return navigateTo(getFacesContext(), Pages.STUDY_EDITDEPTMETA);
  }

  /**
   * load missing lists
   * 
   * @return the missinglist view
   */
  public String toMissinglist() {
    model.toMissinglist(getFacesContext());
    return navigateTo(getFacesContext(), Pages.STUDY_MISSINGLIST);
  }

  /**
   * navigate to the study variable definitions page
   * 
	 * @param studyId the id of the study wave
	 * @param studyName the name of the study
	 * @return navigation destination
   */
  public String toStudyvars(Integer studyId, String studyName) {
  	model.prepareStudyVars(studyId, studyName, getFacesContext());
  	return navigateTo(getFacesContext(), Pages.STUDY_STUDYVARS);
  }

  /**
   * update the time variable
   * 
   * @return navigation destination
   */
  public String doUpdateTimeVar() {
  	Boolean result = model.doUpdateTimeVar(getFacesContext());
  	return result ? toWelcome() : "";
  }
  
  /**
   * remove the missing in the bean from the missing list
   * 
   * @param bean the missing bean
   * @return navigation destination
   */
  public String doRemoveMissing(MissingBean bean) {
    model.removeMissing(bean);
    return navigateTo(getFacesContext(), Pages.STUDY_MISSINGLIST_EDIT);
  }

  /**
   * add the missing bean to the list of missings
   * 
   * @return navigation destination
   */
  public String doAddMissing() {
    model.addMissing(getFacesContext());
    return navigateTo(getFacesContext(), Pages.STUDY_MISSINGLIST_EDIT);
  }

  /**
   * @param bean the missinglist bean; may be null
   * @return navigation destination
   */
  public String toEditMissinglist(MissinglistBean bean) {
    model.prepareEditMissinglist(getFacesContext(), bean);
    return navigateTo(getFacesContext(), Pages.STUDY_MISSINGLIST_EDIT);
  }

  /**
   * remove a missinglist completely from the database
   * 
   * @param bean the missinglist bean
   * @return navigation destination
   */
  public String doRemoveMissinglist(MissinglistBean bean) {
    model.removeMissinglist(getFacesContext(), bean);
    return toMissinglist();
  }

  /**
   * make the missing list persistent in the database
   * 
   * @return navigation destination
   */
  public String doApproveMissings() {
    boolean result = model.approveMissings(getFacesContext());
    return result ? toMissinglist() : "";
  }

  /**
   * download variable attributes
   * 
   * @param parentId the id of the parent
   * @return navigation destination
   */
  public String doDownloadVariableAttributes(Integer parentId) {
    model.downloadVariableAttributes(getFacesContext(), parentId);
    return toWelcome(parentId); // only for errors, otherwise, download is returned
  }

  /**
   * download context variables
   * 
   * @param parentId the id of the parent
   * @return navigation destination
   */
  public String doDownloadConfounder(Integer parentId) {
    model.downloadMetaref(getFacesContext(), parentId);
    return toWelcome(parentId);
  }

  /**
   * upload context variables
   * 
   * @return navigation destination
   */
  public String doUploadMetaref() {
    boolean result = model.smiRun(getFacesContext(), "auxilliaryvariable", model.getUploadMetaref());
    return result ? toWelcome(model.getElement().getId()) : "";
  }

  /**
   * add metadata
   * 
   * @param element the element bean
   * @return navigation destination
   */
  public String doAddDeptmeta(ElementBean element) {
    model.addDeptmeta(element, getFacesContext());
    return navigateTo(getFacesContext(), Pages.STUDY_DEPTMETA);
  }

  /**
   * approve all changes on the metadata
   * 
   * @param element the element bean
   * @return navigation destination
   */
  public String doApproveDeptmeta(ElementBean element) {
    boolean result = model.appoveDeptmeta(getFacesContext(), element);
    Integer id = model.getNullForStudyGroupParent(getFacesContext(), element);
    return result ? (id == null ? toWelcome() : toWelcome(id)) : toDeptmeta(model.getElement());
  }
  
  /**
   * rever all changes on the metadata
   * 
   * @param element the element bean
   * @return navigation destination
   */
  public String doRevertDeptmeta(ElementBean element) {
    Integer id = model.getNullForStudyGroupParent(getFacesContext(), element);
    return id == null ? toWelcome() : toWelcome(id);
  }

  /**
   * navigate to add department page
   * 
   * @return navigation destination
   */
  public String toAddDepartment() {
    model.toDepartment(getFacesContext());
    return navigateTo(getFacesContext(), Pages.STUDY_ADD_DEPARTMENT);
  }

  /**
   * navigate to add variable page
   * 
   * @return navigation destination
   */
  public String toAddVariable() {
    return toAddVariable(true);
  }

  private String toAddVariable(boolean reset) {
    if (reset) {
      model.toAddVariable(getFacesContext());
    }
    return navigateTo(getFacesContext(), Pages.STUDY_ADD_VARIABLE);
  }

  /**
   * add metadata
   * 
   * @return navigation destination
   */
  public String doAddMetadata() {
    model.addMetadata();
    return toMetadata();
  }
 
  /**
   * remove metadata
   * 
   * @param referenceVariable the id of the reference variable
   * @return navigation destination
   */
  public String doRemoveMetadata(Integer referenceVariable) {
    model.removeMetadata(referenceVariable, getFacesContext());
    return toMetadata();
  }

  /**
   * remove metadata
   * 
   * @param metaId the id of the meta data
   * @param type the type
   * @param parent the parent element
   * @return navigation destination
   */
  public String doRemoveMetadata(Integer metaId, String type, ElementBean parent) {
    boolean result = model.removeMetadata(metaId, type, parent.getId(), getFacesContext());
    return result ? toDeptmeta(parent) : "";
  }

  /**
   * add a meta reference
   * 
   * @param metaref the meta reference
   * @return navigation destination
   */
  @Deprecated
  public String doAddMetaref(MetarefBean metaref) {
    metaref.getMap().put(metaref.getNewKey(), metaref.getNewValue());
    metaref.resetNew();
    return toMetadata();
  }

  /**
   * remove a meta reference
   * 
   * @param metaref the meta reference
   * @param key the key
   * @return navigation destination
   */
  @Deprecated
  public String doRemoveMetaref(MetarefBean metaref, String key) {
    metaref.remove(new EnumConverter().getEnumMetareftype(key));
    return toMetadata();
  }

  /**
   * save the metaref
   * 
   * @return navigation destination
   */
  public String doSaveMetaref() {
    VariableBean variable = model.getVariable();
    model.saveMetadata(variable, getFacesContext());
    return toEdit(variable);
  }

  /**
   * upload variable attributes
   * 
   * @return navigation destination
   */
  public String doFileUploadVariableAttributes() {
    Boolean result = model.smiRun(getFacesContext(), "variableattribute", model.getUploadBean());
    return result ? toWelcome(model.getElement().getId()) : "";
  }

  /**
   * add a variable
   * 
   * @return navigation destination
   */
  public String doAddVariable() {
    boolean result = model.addVariable(getFacesContext());
    return result ? toEdit(model.getVariable()) : navigateTo(getFacesContext(), Pages.STUDY_ADD_VARIABLE);
  }

  /**
   * add an element
   * 
   * @return navigation destination
   */
  public String doAddElement() {
    Integer keyElement = model.addElement(getFacesContext());
    return keyElement != null ? toWelcome(keyElement) : "";
  }

  /**
   * update a variable
   * 
   * @return navigation destination
   */
  public String doUpdateVariable() {
    model.updateVariable(getFacesContext());
    getFacesContext().reloadTranslations();
    return toShow(model.getVariable());
  }

  /**
   * update a department
   * 
   * @return navigation destination
   */
  public String doUpdateDepartment() {
    model.updateElement(getFacesContext());
    getFacesContext().reloadTranslations();
    return toShow(model.getElement());
  }

  /**
   * delete an element in the hierarchy
   * 
   * @param element the element
   * @return navigation destination
   */
  public String doDeleteElementOrVariable(ElementBean element) {
    boolean isVariable = element.getIsVariable();
    Integer id = element.getId();
    Integer parent = element.getParent();
    return isVariable ? doDeleteVariable(id, parent) : doDeleteElement(id, parent);
  }

  private String doDeleteElement(Integer id, Integer parent) {
    model.deleteElement(id, getFacesContext());
    return toWelcome(parent);
  }

  private String doDeleteVariable(Integer id, Integer parent) {
    model.deleteVariable(id, getFacesContext());
    return toWelcome(parent);
  }

  /**
   * add privilege
   * 
   * @return navigation destination
   */
  public String doAddPrivilege() {
    model.addPrivilege(getFacesContext());
    return toUser(model.getElement());
  }

  /**
   * edit privilege
   * 
   * @return navigation destination
   */
  public String doEditPrivilege() {
    model.updatePrivilege(getFacesContext());
    return toUser(model.getElement());
  }

  /**
   * delete privilege
   * 
   * @return navigation destination
   */
  public String doDeletePrivilege() {
    model.removePrivilege(getFacesContext());
    return toUser(model.getElement());
  }

  /**
   * remove attribute from list of variable attributes
   * 
   * @param variableBean the variable bean
   * @param attributeBean the variable attribute bean
   * 
   * @return navigation destination
   */
  public String doRemoveAttribute(VariableBean variableBean, VariableAttributeBean attributeBean) {
    model.removeAttributeFromVariable(variableBean, attributeBean);
    return navigateTo(getFacesContext(), Pages.STUDY_EDIT); // do not reload because of not yet done database updates
  }

  /**
   * add an attribute to the variable bean
   * 
   * @param variableBean the variable bean
   * @return navigation destination
   */
  public String doAddAttribute(VariableBean variableBean) {
    model.addAttribute(variableBean);
    return navigateTo(getFacesContext(), Pages.STUDY_EDIT); // do not reload because of not yet done database updates
  }
  
  /**
   * try to import the OPAL content underneath the element referenced by uniqueName
   * 
   * @return navigation destination
   */
  public String doImportOpal() {
  	Integer parent = model.doImportOpal(getFacesContext());
  	return toWelcome(parent == null ? model.getElement().getId() : parent);
  }
  
  /**
   * try to import the SHIP content underneath the element referenced by uniqueName
   * 
   * @return navigation destination
   */
  public String doImportShip() {
	Integer parent = model.doImportShip(getFacesContext());
	return toWelcome(parent == null ? model.getElement().getId() : parent);
  }

  /**
   * ajax listener to reset opal projects
   */
  public void doResetOpalProjects() {
  	model.resetOpalProjects(getFacesContext());
  }

  /**
   * ajax listener to reset opal tables
   */
  public void doResetOpalTables() {
  	model.resetOpalTables(getFacesContext());
  }
  
  /**
   * @return a list of studies that the current user has access to
   */
  public List<?> getStudyGroupsOfUser() {
    return model.getStudyGroupsOfUser(getFacesContext());
  }

  private AclBean mightAccessStudy() {
    return model.getStudyAcl();
  }

  /**
   * @return flag to determine if the current user might use this study
   */
  public Boolean mightUseStudy() {
    return mightAccessStudy().getExecute();
  }

  /**
   * @return flag to determine if the current user might not use this study
   */
  public Boolean mightNotUseStudy() {
    Boolean mightUseStudy = mightUseStudy();
    return mightUseStudy == null || !mightUseStudy;
  }

  /**
   * check if current user can write for this privilege id
   * 
   * @param privilegeId the id of the privilege
   * @param facesContext the faces context
   * @return true or false
   */
  public Boolean mightWrite(Integer privilegeId, SquareFacesContext facesContext) {
    for (PrivilegeBean bean : facesContext.getProfile().getPrivileges()) {
      if (bean.getPk().equals(privilegeId)) {
        return bean.getAcl().contains("w");
      }
    }
    return false;
  }

  /**
   * check if current user cannot write for this privilege id
   * 
   * @param privilegeId the id of the privilege
   * @param facesContext the faces context
   * @return true or false
   */
  public Boolean mustNotWrite(Integer privilegeId, SquareFacesContext facesContext) {
    return !mightWrite(privilegeId, facesContext);
  }

  /**
   * @return true if the cancel button was hit
   */
  public boolean getCancelling() {
    return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
        .containsKey("form:btn_cancel");
  }

  /**
   * get the variable attribute names that dataquieR knows
   * 
   * @return a list of strings
   */
  public List<String> getVariableattributeNames() {
    // TODO: fill this list by known attribute names from dataquieR
    List<String> list = new ArrayList<>();
    list.add("limits");
    list.add("recode");
    list.add("eventcat");
    list.add("refcat");
    list.add("varshortlabel");
    list.add("varlabel");
    return list;
  }

  /**
   * @return the model
   */
  public StudyModel getModel() {
    return model;
  }

  @Override
  public Map<Integer, Map<Integer, AclBean>> refreshMap() {
    return null;
  }

  @Override
  public boolean getIsCurrentSubController() {
    return getIsCurrentSubController(getFacesContext());
  }

  @Override
  public boolean getIsCurrentController() {
    return getIsCurrentController(getFacesContext());
  }
}
