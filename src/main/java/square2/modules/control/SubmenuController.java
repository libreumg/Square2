package square2.modules.control;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.context.FacesContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import square2.modules.control.help.Pages;
import square2.modules.control.help.Session;

/**
 * 
 * @author henkej
 *
 */
public abstract class SubmenuController extends UserRightController {
	private static final Logger LOGGER = LogManager.getLogger(SubmenuController.class);

	private Pages[] allowed;

	/**
	 * create new submenu controller
	 * 
	 * @param mainPage
	 *          the main page
	 * @param pages
	 *          the valid pages
	 */
	public SubmenuController(Pages mainPage, Pages... pages) {
		super(constructorHelper(mainPage, pages));
		this.allowed = pages;
	}

	private static final Pages[] constructorHelper(Pages mainPage, Pages... pages) {
		List<Pages> p = new ArrayList<>(Arrays.asList(pages));
		p.add(mainPage);
		Pages all[] = new Pages[p.size()];
		all = p.toArray(all);
		return all;
	}

	/**
	 * call getIsCurrentSubController with current facesContext
	 * 
	 * @return true or false
	 */
	public abstract boolean getIsCurrentSubController();

	/**
	 * check if this is the current sub controller
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return true or false
	 */
	public boolean getIsCurrentSubController(FacesContext facesContext) {
		Pages page = (Pages) facesContext.getExternalContext().getSessionMap().get(Session.SUBPAGE.get());
		for (Pages p : allowed) {
			if (p.equals(page)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * get the active panel
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return the active panel
	 */
	public String getActivePanel(FacesContext facesContext) {
		return (String) facesContext.getExternalContext().getSessionMap().get(Session.ACTIVE_PANEL.get());
	}

	/**
	 * set the active panel
	 * 
	 * @param activePanel
	 *          the active panel
	 * @param facesContext
	 *          the context of this function call
	 */
	public void setActivePanel(String activePanel, FacesContext facesContext) {
		facesContext.getExternalContext().getSessionMap().put(Session.ACTIVE_PANEL.get(), activePanel);
		LOGGER.debug("set active panel to {}", activePanel);
	}
}
