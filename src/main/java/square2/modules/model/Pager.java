package square2.modules.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class Pager {
	private Integer offset;
	private Integer pagelength;

	/**
	 * get the page of the item
	 * 
	 * @param itemIndex
	 *          the index of the item
	 * @return the page
	 */
	public Integer getPageOfItem(Integer itemIndex) {
		return pagelength == null || pagelength == 0 ? 0 : ((itemIndex - 1) / pagelength) + 1; 
	}

	/**
	 * determine of the snipplets have a paginator
	 * 
	 * @param itemSize
	 *          the number of elements for this pager
	 * 
	 * @return true or false
	 */
	public Boolean getHasPaginator(Integer itemSize) {
		return pagelength == null || offset == null || pagelength < itemSize;
	}

	/**
	 * get the list of pages
	 * 
	 * @param itemSize
	 *          the number of elements for this pager
	 * 
	 * @return list of pages; an empty list at least
	 */
	public List<Integer> getPages(Integer itemSize) {
		List<Integer> list = new ArrayList<>();
		if (getHasPaginator(itemSize)) {
			Integer pages = itemSize / pagelength;
			if (itemSize % pagelength > 0) {
				pages++;
			}
			for (int i = 0; i < pages; i++) {
				list.add(i);
			}
		}
		return list;
	}

	/**
	 * set the last page
	 * 
	 * @param itemSize
	 *          the number of elements for this pager
	 */
	public void setLastPage(Integer itemSize) {
		Integer pages = itemSize / pagelength;
		if (itemSize % pagelength > 0) {
			pages++;
		}
		this.offset = (pages - 1) * pagelength;
	}

	/**
	 * check if the view is on the last page
	 * 
	 * @param itemSize
	 *          the number of elements for this pager
	 * 
	 * @return true or false
	 */
	public Boolean getIsOnLastPage(Integer itemSize) {
		Integer pages = itemSize / pagelength;
		if (itemSize % pagelength > 0) {
			pages++;
		}
		Integer currentPage = offset / pagelength;
		if (offset % pagelength > 0) {
			currentPage++;
		}
		return currentPage >= (pages - 1);
	}

	/**
	 * set the page by setting the offset
	 * 
	 * @param page
	 *          the page
	 */
	public void setPage(Integer page) {
		this.offset = page * getPagelength();
	}

	/**
	 * check if this is the current page
	 * 
	 * @param page
	 *          the page
	 * @return true or false
	 */
	public Boolean isCurrentPage(Integer page) {
		return (page * getPagelength()) == getOffset();
	}

	/**
	 * @return the offset
	 */
	public Integer getOffset() {
		return offset;
	}

	/**
	 * @param offset
	 *          the offset to set
	 */
	public void setOffset(Integer offset) {
		this.offset = offset == null ? 0 : offset;
	}

	/**
	 * @return the pagelength
	 */
	public Integer getPagelength() {
		return pagelength;
	}

	/**
	 * @param pagelength
	 *          the pagelength to set
	 */
	public void setPagelength(Integer pagelength) {
		this.pagelength = pagelength;
	}
}
