package square2.modules.model;

/**
 * 
 * @author henkej
 *
 */
public class RequiredExecutionsBean extends AnaparfunOutputBean {
	private static final long serialVersionUID = 1L;

	private Boolean chosen;

	/**
	 * @param fkAnaparfun
	 *          the anaparfun id
	 * @param fkFunctionoutput
	 *          the function output id
	 * @param anaparfunName
	 *          the anaparfun name
	 * @param functionoutputName
	 *          the function output name
	 * @param orderNr
	 *          the order number
	 */
	public RequiredExecutionsBean(Integer fkAnaparfun, Integer fkFunctionoutput, String anaparfunName,
			String functionoutputName, Integer orderNr) {
		super(fkAnaparfun, fkFunctionoutput, anaparfunName, functionoutputName, orderNr);
		this.chosen = false;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder("RequiredExecutionsBean");
		buf.append("@{chosen=").append(chosen);
		buf.append(",super=").append(super.toString());
		buf.append("}");
		return buf.toString();
	}
	
	/**
	 * @return the chosen
	 */
	public Boolean getChosen() {
		return chosen;
	}

	/**
	 * @param chosen
	 *          the chosen to set
	 */
	public void setChosen(Boolean chosen) {
		this.chosen = chosen;
	}
}
