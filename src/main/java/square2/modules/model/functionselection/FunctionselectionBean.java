package square2.modules.model.functionselection;

import square2.modules.model.AclBean;

/**
 * 
 * @author henkej
 *
 */
public class FunctionselectionBean {
	private Integer fkFunctionlist;
	private Integer parent;
	private String name;
	private String description;
	private String acronym;
	private final AclBean acl;
	private Boolean isAbstract;
	
	/**
	 * for primefaces ajax filter (data table), a constructor without arguments is needed
	 */
	public FunctionselectionBean() {
		this.acl = new AclBean();
	}

	/**
	 * create new analysis template bean
	 * 
	 * @param fkFunctionlist
	 *          the id of the function list
	 */
	public FunctionselectionBean(Integer fkFunctionlist) {
		super();
		this.fkFunctionlist = fkFunctionlist;
		this.acl = new AclBean();
	}

	/**
	 * @param pk
	 *          the fkFunctionlist
	 */
	public void resetPk(Integer pk) {
		this.fkFunctionlist = pk;
	}

	/**
	 * @return the parent
	 */
	public Integer getParent() {
		return parent;
	}

	/**
	 * @param parent
	 *          the parent to set
	 */
	public void setParent(Integer parent) {
		this.parent = parent;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *          the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the isAbstract
	 */
	public Boolean getIsAbstract() {
		return isAbstract;
	}

	/**
	 * @param isAbstract
	 *          the isAbstract to set
	 */
	public void setIsAbstract(Boolean isAbstract) {
		this.isAbstract = isAbstract;
	}

	/**
	 * @return the fkFunctionlist
	 */
	public Integer getFkFunctionlist() {
		return fkFunctionlist;
	}

	/**
	 * @return the acl
	 */
	public AclBean getAcl() {
		return acl;
	}

	/**
	 * @return the acronym
	 */
	public String getAcronym() {
		return acronym;
	}

	/**
	 * @param acronym the acronym to set
	 */
	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}
}
