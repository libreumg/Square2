package square2.modules.model.datamanagement;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class ReleaseInfoBean extends ReleaseBean {
	private static final long serialVersionUID = 1L;
	private final List<String> vargroupNames;
	private String mergecolumnname;

	/**
	 * @param pk the id
	 */
	public ReleaseInfoBean(Integer pk) {
		super(pk);
		this.vargroupNames = new ArrayList<>();
	}
	
	/**
	 * @param bean the bean template
	 */
	public ReleaseInfoBean(ReleaseBean bean) {
		super(bean == null ? null : bean.getPk());
		this.vargroupNames = new ArrayList<>();
		if (bean != null) {
			this.setName(bean.getName());
			this.setFkElement(bean.getFkElement());
			this.setStudyName(bean.getStudyName());
			this.setDescription(bean.getDescription());
			this.setReleasedate(bean.getReleasedate());
			this.setRolling(bean.getRolling());
			this.setLocation(bean.getLocation());
			this.setBackup(bean.getBackup());
			this.setActive(bean.getActive());
			this.setMergecolumn(bean.getMergecolumn());
		}
	}

	/**
	 * @return the mergecolumnname
	 */
	public String getMergecolumnname() {
		return mergecolumnname;
	}

	/**
	 * @param mergecolumnname the mergecolumnname to set
	 */
	public void setMergecolumnname(String mergecolumnname) {
		this.mergecolumnname = mergecolumnname;
	}

	/**
	 * @return the vargroupNames
	 */
	public List<String> getVargroupNames() {
		return vargroupNames;
	}
}
