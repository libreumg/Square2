package square2.modules.model.datamanagement;

/**
 * 
 * @author henkej
 *
 */
public class ColumnBean {
	private final String columnName;
	private final String dataType;

	/**
	 * create new column bean
	 * 
	 * @param columnName
	 *          then name
	 * @param dataType
	 *          the data type
	 */
	public ColumnBean(String columnName, String dataType) {
		this.columnName = columnName;
		this.dataType = dataType;
	}

	/**
	 * @return the name
	 */
	public String getColumnName() {
		return columnName;
	}

	/**
	 * @return the data type
	 */
	public String getDataType() {
		return dataType;
	}
}
