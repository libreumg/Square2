package square2.modules.model.datamanagement;

/**
 * 
 * @author henkej
 *
 */
public class ElementBean {
	private final Integer id;
	private final String name;
	private final String description;

	/**
	 * @param id
	 *          the id
	 * @param name
	 *          the name
	 * @param description
	 *          the description
	 */
	public ElementBean(Integer id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
}
