package square2.modules.model.report.analysis.matrix;

import square2.modules.model.report.analysis.AnalysisVariableParameterBean;

/**
 * 
 * @author henkej
 *
 */
public class MatrixVariableParameterInput implements MatrixObject {
	private AnalysisVariableParameterBean bean;

	/**
	 * create new matrix variable parameter input
	 * 
	 * @param bean
	 *          the bean
	 */
	public MatrixVariableParameterInput(AnalysisVariableParameterBean bean) {
		this.bean = bean;
	}

	/**
	 * @return the bean
	 */
	public AnalysisVariableParameterBean getBean() {
		return bean;
	}

	@Override
	public boolean getIsButton() {
		return false;
	}

	@Override
	public boolean getIsLabel() {
		return false;
	}

	@Override
	public boolean getIsInput() {
		return true;
	}

	@Override
	public boolean getIsIconButton() {
		return false;
	}

	@Override
	public boolean getIsDisabled() {
		return false;
	}

	@Override
	public boolean getIsTitle() {
		return false;
	}

	@Override
	public boolean getIsVariable() {
		return false;
	}
}
