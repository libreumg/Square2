package square2.modules.model.report.analysis;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class AnalysisMatrix {
	private Integer id;
	private final String name;
	private Integer fkRight;
	private AnalysisTemplate analysisTemplate;
	private List<ParameterVariable> parameterVariables;

	/**
	 * create nw analysis matrix
	 * 
	 * @param name
	 *          the name
	 */
	public AnalysisMatrix(String name) {
		this.name = name;
		parameterVariables = new ArrayList<>();
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *          the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the fkRight
	 */
	public Integer getFkRight() {
		return fkRight;
	}

	/**
	 * @param fkRight
	 *          the fkRight to set
	 */
	public void setFkRight(Integer fkRight) {
		this.fkRight = fkRight;
	}

	/**
	 * @return the analysisTemplate
	 */
	public AnalysisTemplate getAnalysisTemplate() {
		return analysisTemplate;
	}

	/**
	 * @param analysisTemplate
	 *          the analysisTemplate to set
	 */
	public void setAnalysisTemplate(AnalysisTemplate analysisTemplate) {
		this.analysisTemplate = analysisTemplate;
	}

	/**
	 * @return the parameterVariables
	 */
	public List<ParameterVariable> getParameterVariables() {
		return parameterVariables;
	}

	/**
	 * @param parameterVariables
	 *          the parameterVariables to set
	 */
	public void setParameterVariables(List<ParameterVariable> parameterVariables) {
		this.parameterVariables = parameterVariables;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
