package square2.modules.model.report.analysis;

import java.util.ArrayList;
import java.util.List;

import square2.modules.model.functionselection.FunctionselectionBean;

/**
 * 
 * @author henkej
 *
 */
public class AnalysisWelcomePageBean {
	private List<String> matrixes;
	private List<FunctionselectionBean> templates;

	/**
	 * create new analysis welcome page bean
	 */
	public AnalysisWelcomePageBean() {
		this.matrixes = new ArrayList<>();
		this.templates = new ArrayList<>();
	}

	/**
	 * @return the matrixes
	 */
	public List<String> getMatrixes() {
		return matrixes;
	}

	/**
	 * @return the templates
	 */
	public List<FunctionselectionBean> getTemplates() {
		return templates;
	}
}
