package square2.modules.model.report.analysis;

import java.util.ArrayList;
import java.util.List;

import square2.modules.model.statistic.FunctionBean;

/**
 * 
 * @author henkej
 *
 */
public class AnalysisTemplatePageBean {
	private final Integer fkFunctionlist;
	private String name;
	private String description;
	private List<AnalysisFunctionBean> chosen;
	private List<FunctionBean> all;

	/**
	 * create new analysis template page bean
	 * 
	 * @param t
	 *          the analysis template
	 */
	public AnalysisTemplatePageBean(AnalysisTemplate t) {
		chosen = new ArrayList<>();
		all = new ArrayList<FunctionBean>();
		this.fkFunctionlist = t.getFkFunctionlist();
		this.name = t.getName();
		this.description = t.getDescription();
	}

	/**
	 * @return the id of the function list
	 */
	public Integer getFkFunctionlist() {
		return fkFunctionlist;
	}

	/**
	 * @return the chosen flag
	 */
	public List<AnalysisFunctionBean> getChosen() {
		return chosen;
	}

	/**
	 * @return the list of functions
	 */
	public List<FunctionBean> getAll() {
		return all;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *          the description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param name
	 *          the name
	 */
	public void setName(String name) {
		this.name = name;
	}
}
