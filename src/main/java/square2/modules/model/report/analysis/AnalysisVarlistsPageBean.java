package square2.modules.model.report.analysis;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class AnalysisVarlistsPageBean {
	private List<MatrixLine> matrix;

	/**
	 * create nea analysis varlists page bean
	 */
	public AnalysisVarlistsPageBean() {
		matrix = new ArrayList<>();
	}

	/**
	 * @return the matrix
	 */
	public List<MatrixLine> getMatrix() {
		return matrix;
	}
}
