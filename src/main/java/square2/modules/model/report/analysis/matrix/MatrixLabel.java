package square2.modules.model.report.analysis.matrix;

/**
 * 
 * @author henkej
 *
 */
public class MatrixLabel implements MatrixObject {
	private final String text;
	private final String tooltip;
	private final Integer id;

	/**
	 * create new matrix label
	 * 
	 * @param id
	 *          the id
	 * @param text
	 *          the text
	 * @param tooltip
	 *          the tooltip
	 */
	public MatrixLabel(Integer id, String text, String tooltip) {
		this.id = id;
		this.text = text;
		this.tooltip = tooltip;
	}

	@Override
	public boolean getIsButton() {
		return false;
	}

	@Override
	public boolean getIsLabel() {
		return true;
	}

	@Override
	public boolean getIsInput() {
		return false;
	}

	@Override
	public boolean getIsIconButton() {
		return false;
	}

	@Override
	public boolean getIsDisabled() {
		return false;
	}

	@Override
	public boolean getIsTitle() {
		return false;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return the tooltip
	 */
	public String getTooltip() {
		return tooltip;
	}

	@Override
	public boolean getIsVariable() {
		return false;
	}
}
