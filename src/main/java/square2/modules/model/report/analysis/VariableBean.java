package square2.modules.model.report.analysis;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class VariableBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private final String name;
	private final String translation;
	private final String valuelist;
	private String dn;

	/**
	 * create new variable bean
	 * 
	 * @param name
	 *          the name
	 * @param translation
	 *          the translation
	 * @param valuelist
	 *          the value list
	 */
	public VariableBean(String name, String translation, String valuelist) {
		super();
		this.name = name;
		this.translation = translation;
		this.valuelist = valuelist;
	}

	/**
	 * @return the dn
	 */
	public String getDn() {
		return dn;
	}

	/**
	 * @param dn
	 *          the dn to set
	 */
	public void setDn(String dn) {
		this.dn = dn;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the translation
	 */
	public String getTranslation() {
		return translation;
	}

	/**
	 * @return the valuelist
	 */
	public String getValuelist() {
		return valuelist;
	}
}
