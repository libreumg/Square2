package square2.modules.model.report.analysis.matrix;

/**
 * 
 * @author henkej
 *
 */
public class MatrixEmptyCell implements MatrixObject {
	@Override
	public boolean getIsButton() {
		return false;
	}

	@Override
	public boolean getIsLabel() {
		return false;
	}

	@Override
	public boolean getIsInput() {
		return false;
	}

	@Override
	public boolean getIsIconButton() {
		return false;
	}

	@Override
	public boolean getIsDisabled() {
		return false;
	}

	@Override
	public boolean getIsTitle() {
		return false;
	}

	@Override
	public boolean getIsVariable() {
		return false;
	}
}
