package square2.modules.model.report.analysis;

/**
 * 
 * @author henkej
 *
 */
public class AnavarparBean {
	private final Integer pk;
	private final String name;
	private String value;

	/**
	 * create new anavarparBean
	 * 
	 * @param pk
	 *          id of the database entry
	 * @param name
	 *          name of the parameter
	 * @param value
	 *          value of the parameter
	 */
	public AnavarparBean(Integer pk, String name, String value) {
		super();
		this.pk = pk;
		this.name = name;
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *          the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
