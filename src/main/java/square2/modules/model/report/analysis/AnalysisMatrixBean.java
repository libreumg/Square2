package square2.modules.model.report.analysis;

import java.util.Date;

import square2.modules.model.AclBean;

/**
 * 
 * @author henkej
 *
 */
public class AnalysisMatrixBean {
	private final Integer report;
	private String name;
	private final Date lastchange;
	private final AclBean acl;
	private Boolean isAbstract;

	/**
	 * generate new analysis matrix bean
	 * 
	 * @param report
	 *          the id of the report
	 * @param name
	 *          the name
	 * @param lastchange
	 *          the lastchange of the report
	 * @param accessLevel
	 *          the acl
	 */
	public AnalysisMatrixBean(Integer report, String name, Date lastchange, AclBean accessLevel) {
		super();
		this.report = report;
		this.name = name;
		this.lastchange = lastchange;
		this.acl = accessLevel;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the isAbstract
	 */
	public Boolean getIsAbstract() {
		return isAbstract;
	}

	/**
	 * @param isAbstract
	 *          the isAbstract to set
	 */
	public void setIsAbstract(Boolean isAbstract) {
		this.isAbstract = isAbstract;
	}

	/**
	 * @return the report
	 */
	public Integer getReport() {
		return report;
	}

	/**
	 * @return the acl
	 */
	public AclBean getAcl() {
		return acl;
	}

	/**
	 * @return the lastchange
	 */
	public Date getLastchange() {
		return lastchange;
	}
}
