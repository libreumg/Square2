package square2.modules.model.report.analysis.matrix;

/**
 * 
 * @author henkej
 *
 */
public class MatrixCheckbox implements MatrixObject {
	private final String id;
	private final boolean origValue;
	private final String tooltip;
	private boolean value;

	/**
	 * create new matrix checkbox
	 * 
	 * @param id
	 *          the id
	 * @param value
	 *          the value
	 * @param tooltip
	 *          the tooltip
	 */
	public MatrixCheckbox(String id, boolean value, String tooltip) {
		super();
		this.id = id;
		this.value = value;
		this.origValue = value;
		this.tooltip = tooltip;
	}

	/**
	 * toggle the value
	 */
	public void toggle() {
		value = value == false;
	}

	@Override
	public boolean getIsButton() {
		return false;
	}

	@Override
	public boolean getIsLabel() {
		return false;
	}

	@Override
	public boolean getIsInput() {
		return true;
	}

	@Override
	public boolean getIsIconButton() {
		return false;
	}

	@Override
	public boolean getIsDisabled() {
		return false;
	}

	@Override
	public boolean getIsTitle() {
		return false;
	}

	/**
	 * @return true if the original value is not the new value
	 */
	public boolean getDirty() {
		return origValue != value;
	}

	/**
	 * @return the value
	 */
	public boolean getValue() {
		return value;
	}

	/**
	 * @param value
	 *          the value
	 */
	public void setValue(boolean value) {
		this.value = value;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	@Override
	public boolean getIsVariable() {
		return false;
	}

	/**
	 * @return the tooltip
	 */
	public String getTooltip() {
		return tooltip;
	}

}
