package square2.modules.model.report.analysis;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class VariablegroupBean implements Serializable, Comparable<VariablegroupBean> {
	private static final long serialVersionUID = 1L;

	private final Integer id;
	private final String name;
	private final List<String> variables;

	/**
	 * create new variable group bean
	 * 
	 * @param id
	 *          the id
	 * @param name
	 *          the name
	 */
	public VariablegroupBean(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
		this.variables = new ArrayList<>();
	}

	@Override
	public int compareTo(VariablegroupBean o) {
		return name == null || o == null || o.getName() == null ? 0
				: name.toLowerCase().compareTo(o.getName().toLowerCase());
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the variables
	 */
	public List<String> getVariables() {
		return variables;
	}
}
