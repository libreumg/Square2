package square2.modules.model.report.analysis;

import square2.modules.model.functionselection.FunctionselectionBean;

/**
 * 
 * @author henkej
 *
 */
public class MatrixBean {
	private final Integer report;
	private String name;
	private String description;
	private FunctionselectionBean analysisTemplateBean;
	private VariablegroupBean variablegroup;

	/**
	 * create new matrix bean
	 * 
	 * @param report
	 *          the id of the report
	 * @param name
	 *          the name
	 * @param description
	 *          the description
	 * @param analysisTemplateBean
	 *          the analysis template
	 * @param variablegroup
	 *          the variable group
	 */
	public MatrixBean(Integer report, String name, String description, FunctionselectionBean analysisTemplateBean,
			VariablegroupBean variablegroup) {
		super();
		this.report = report;
		this.name = name;
		this.description = description;
		this.analysisTemplateBean = analysisTemplateBean;
		this.variablegroup = variablegroup;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *          the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the analysisTemplateBean
	 */
	public FunctionselectionBean getAnalysisTemplateBean() {
		return analysisTemplateBean;
	}

	/**
	 * @param analysisTemplateBean
	 *          the analysisTemplateBean to set
	 */
	public void setAnalysisTemplateBean(FunctionselectionBean analysisTemplateBean) {
		this.analysisTemplateBean = analysisTemplateBean;
	}

	/**
	 * @return the variablegroup
	 */
	public VariablegroupBean getVariablegroup() {
		return variablegroup;
	}

	/**
	 * @param variablegroup
	 *          the variablegroup to set
	 */
	public void setVariablegroup(VariablegroupBean variablegroup) {
		this.variablegroup = variablegroup;
	}

	/**
	 * @return the report
	 */
	public Integer getReport() {
		return report;
	}
}
