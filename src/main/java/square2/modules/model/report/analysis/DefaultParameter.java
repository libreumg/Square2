package square2.modules.model.report.analysis;

import java.io.Serializable;

import de.ship.dbppsquare.square.enums.EnumAnadef;

/**
 * 
 * @author henkej
 *
 */
public class DefaultParameter implements Serializable {
	private static final long serialVersionUID = 1L;

	private final String name;
	private final Integer fkFunctioninput;
	private String value;
	private EnumAnadef fixed;

	/**
	 * create new default parameter
	 * 
	 * @param name
	 *          the name of the functioninput
	 * @param fkFunctioninput
	 *          the pk of the functioninput
	 */
	public DefaultParameter(String name, Integer fkFunctioninput) {
		this.name = name;
		this.fkFunctioninput = fkFunctioninput;
	}

	public String toString() {
		StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
		buf.append("@{name=").append(name);
		buf.append(",fkFunctioninput=").append(fkFunctioninput);
		buf.append(",value=").append(value);
		buf.append(",fixed=").append(fixed);
		buf.append("}");
		return buf.toString();
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *          the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the fixed
	 */
	public EnumAnadef getForced() {
		return fixed;
	}

	/**
	 * @param fixed
	 *          the fixed to set
	 */
	public void setFixed(EnumAnadef fixed) {
		this.fixed = fixed;
	}

	/**
	 * @return the fkFunctioninput
	 */
	public Integer getFkFunctioninput() {
		return fkFunctioninput;
	}
}
