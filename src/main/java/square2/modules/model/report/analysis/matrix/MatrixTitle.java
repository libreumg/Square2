package square2.modules.model.report.analysis.matrix;

/**
 * 
 * @author henkej
 *
 */
public class MatrixTitle implements MatrixObject {
	private final String title;

	/**
	 * create new matrix title
	 * 
	 * @param title
	 *          the title
	 */
	public MatrixTitle(String title) {
		this.title = title;
	}

	@Override
	public boolean getIsButton() {
		return false;
	}

	@Override
	public boolean getIsLabel() {
		return false;
	}

	@Override
	public boolean getIsInput() {
		return false;
	}

	@Override
	public boolean getIsIconButton() {
		return false;
	}

	@Override
	public boolean getIsDisabled() {
		return false;
	}

	@Override
	public boolean getIsTitle() {
		return true;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	@Override
	public boolean getIsVariable() {
		return false;
	}
}
