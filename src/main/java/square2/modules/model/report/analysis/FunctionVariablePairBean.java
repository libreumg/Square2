package square2.modules.model.report.analysis;

/**
 * 
 * @author henkej
 *
 */
public class FunctionVariablePairBean {
	private final Integer anaparfun;
	private final Integer variableId;

	/**
	 * create new function variable pair bean
	 * 
	 * @param anaparfun
	 *          the id of the analysis function
	 * @param variableId
	 *          the id of the variable
	 */
	public FunctionVariablePairBean(Integer anaparfun, Integer variableId) {
		super();
		this.anaparfun = anaparfun;
		this.variableId = variableId;
	}

	/**
	 * @return the anaparfun
	 */
	public Integer getAnaparfun() {
		return anaparfun;
	}

	/**
	 * @return the variableId
	 */
	public Integer getVariableId() {
		return variableId;
	}
}
