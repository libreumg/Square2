package square2.modules.model.report.factory;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class RuntimeVariable {
	private String name;
	private final List<RuntimeFunction> functions;

	/**
	 * create new runtime variable
	 */
	public RuntimeVariable() {
		functions = new ArrayList<>();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the functions
	 */
	public List<RuntimeFunction> getFunctions() {
		return functions;
	}
}
