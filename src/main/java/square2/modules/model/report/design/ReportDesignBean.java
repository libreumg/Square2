package square2.modules.model.report.design;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.model.DataModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.PaginationPager;

/**
 * 
 * @author henkej
 *
 */
public class ReportDesignBean extends PaginationPager {
	private static final Logger LOGGER = LogManager.getLogger(ReportDesignBean.class);

	private Integer id;
	private String name;
	private String description;
	private final Date lastchange;
	private final AclBean acl;
	private final Integer vargroupPrivilege;
	private Integer parent;
	private String parentName;
	private Integer fkFunctionlist;
	private Integer variablegroup;
	private List<SnippletBean> snipplets;
	private DataModelSnippletBeans dataModelSnippletBeans;
	private List<AnaparfunBean> anaparfuns;

	/**
	 * generate new report bean
	 * 
	 * @param acl
	 *          the privileges on the corresponding variable group
	 * @param vargroupPrivilege
	 *          the id of the corresponding variable group
	 * @param pagelength
	 *          the magnitude of rendered snipplets on the report template page
	 * @param lastchange
	 *          the lastchange of the report
	 * @param offset
	 *          the offset of the pages in the paginator
	 */
	public ReportDesignBean(AclBean acl, Integer vargroupPrivilege, Integer pagelength, Date lastchange, Integer offset) {
		this.vargroupPrivilege = vargroupPrivilege;
		this.acl = acl;
		setOffset(offset == null ? 0 : offset);
		setPagelength(pagelength);
		this.lastchange = lastchange;
		snipplets = new ArrayList<>();
		anaparfuns = new ArrayList<>();
	}
	
	/**
	 * @param json the json
	 */
	public ReportDesignBean(String json) {
		ReportDesignBean bean = new Gson().fromJson(json, ReportDesignBean.class);
		this.id = bean.getId();
		this.name = bean.getName();
		this.description = bean.getDescription();
		this.lastchange = bean.getLastchange();
		this.acl = bean.getAcl();
		this.vargroupPrivilege = bean.getVargroupPrivilege();
		this.parent = bean.getParent();
		this.parentName = bean.getParentName();
		this.fkFunctionlist = bean.getFkFunctionlist();
		this.variablegroup = bean.getVariablegroup();
		this.snipplets = bean.getSnipplets();
		this.anaparfuns = bean.getAnaparfuns();
	}

	/**
	 * convert this object to a json representation
	 * 
	 * @return the json representation
	 */
	public String toJson() {
		return new Gson().toJson(this);
	}
	
	/**
	 * get all order numbers of the snipplets
	 * 
	 * @return list of order numbers
	 */
	public List<Integer> getOrders() {
		List<Integer> list = new ArrayList<>();
		for (SnippletBean bean : snipplets) {
			list.add(bean.getOrderNr());
		}
		list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
		return list;
	}

	/**
	 * return number of available functions by number of used ones in this report
	 * 
	 * @param facesContext
	 *          used for translation
	 * 
	 * @return n of m or such
	 * @deprecated use getUsed and getTotal instead
	 */
	@Deprecated
	public String getUsedFunctions(SquareFacesContext facesContext) {
		Set<Integer> wanted = new HashSet<>();
		for (AnaparfunBean bean : anaparfuns) {
			wanted.add(bean.getAnaparfunPk());
		}
		Set<Integer> found = new HashSet<>();
		for (SnippletBean bean : snipplets) {
			if (bean.getMatrixcolumnBean().getFkMatrixcolumn() != null) {
				found.add(bean.getMatrixcolumnBean().getFkMatrixcolumn());
			}
		}
		return facesContext.translate("label.of", found.size(), wanted.size());
	}
	
	/**
	 * get number of used R functions in this report design
	 * 
	 * @return the number of used R functions
	 */
	public Integer getUsed() {
	  Set<Integer> found = new HashSet<>();
    for (SnippletBean bean : snipplets) {
      if (bean.getMatrixcolumnBean().getFkMatrixcolumn() != null) {
        found.add(bean.getMatrixcolumnBean().getFkMatrixcolumn());
      }
    }
    return found.size();
	}
	
	/**
	 * get number of total R functions in this report design
	 * 
	 * @return the number of found R functions
	 */
	public Integer getTotal() {
	  Set<Integer> wanted = new HashSet<>();
    for (AnaparfunBean bean : anaparfuns) {
      wanted.add(bean.getAnaparfunPk());
    }
    return wanted.size();
	}

	/**
	 * get the snipplet with that uuid
	 * 
	 * @param uuid
	 *          the universal unique id if the snipplet
	 * @return the snipplet bean or null if not found
	 */
	public SnippletBean getSnipplet(String uuid) {
		for (SnippletBean bean : snipplets) {
			if (bean.getUniqueId().equals(uuid)) {
				return bean;
			}
		}
		return null;
	}

	/**
	 * reorder snipplets
	 * 
	 * @param bean
	 *          the snipplet bean
	 */
	public void reorderSnipplets(SnippletBean bean) {
		if (bean.getOrderNr() == null) {
			return;
		} else if (bean.getOrderNrNew() == null) {
			return;
		} else if (!bean.getOrderNr().equals(bean.getOrderNrNew())) {
			Integer oldNr = bean.getOrderNr();
			Integer newNr = bean.getOrderNrNew();
			List<SnippletBean> list = getSnipplets();
			for (SnippletBean s : list) {
				Integer currentNr = s.getOrderNr();
				if (currentNr == null) {
					s.setOrderNr(0); // null should never happen
					LOGGER.error("found an unordered snipplet with id = {}; corrected by setting it to 0", bean.getId());
				} else if (currentNr.equals(oldNr)) {
					s.setOrderNr(newNr);
				} else {
					if (oldNr > newNr) {
						if (currentNr >= newNr && currentNr < oldNr) {
							s.setOrderNr(s.getOrderNr() + 1);
						}
					} else {
						if (currentNr <= newNr && currentNr > oldNr) {
							s.setOrderNr(s.getOrderNr() - 1);
						}
					}
				}
			}
		}
		bean.setOrderNrNew(null); // reset for the view
		getSnipplets().sort((o1, o2) -> o1 == null || o2 == null || o1.getOrderNr() == null ? 0 : o1.compareTo(o2));
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(name).append("(").append(id);
		builder.append(")").append(snipplets);
		return builder.toString();
	}

	/**
	 * create a gap in the order number of snipplets by moving them to the right
	 * position
	 * 
	 * @param orderNr
	 *          the orderNr to be used as a gap
	 */
	public void createOrderGap(Integer orderNr) {
		for (SnippletBean bean : snipplets) {
			if (bean.getOrderNr() >= orderNr) {
				bean.setOrderNr(bean.getOrderNr() + 1);
			}
		}
	}

	/**
	 * find maximum of snipplet order numbers and increases it by 1
	 * 
	 * @return next snipplet order number
	 */
	public Integer getNextSnippletOrderNr() {
		Integer max = -1;
		for (SnippletBean bean : snipplets) {
			max = max < bean.getOrderNr() ? bean.getOrderNr() : max;
		}
		return max + 1;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *          the id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the snipplet model
	 */
	public DataModel<SnippletBean> getSnippletsModel() {
		if (dataModelSnippletBeans == null) {
			dataModelSnippletBeans = new DataModelSnippletBeans(snipplets);
		}
		return dataModelSnippletBeans;
	}

	/**
	 * get the page of the snipplets from offset to offset + pagelength
	 * 
	 * @return the page of the snipplets
	 */
	public List<SnippletBean> getPaginatedSnipplets() {
		if (getPagelength() == null) { // speed up
			return getSnipplets();
		}
		List<SnippletBean> list = new ArrayList<>();
		for (Integer i = 0; i < getPagelength(); i++) {
			Integer calculatedI = i + getOffset();
			if (getSnipplets().size() > calculatedI) {
				SnippletBean bean = getSnipplets().get(calculatedI);
				list.add(bean);
			}
		}
		return list;
	}

	/**
	 * check if the view is on the last page
	 * 
	 * @return true or false
	 */
	public Boolean getIsOnLastPage() {
		return getIsOnLastPage(getSnipplets().size());
	}

	/**
	 * set the lastpage
	 */
	public void setLastPage() {
		setLastPage(getSnipplets().size());
	}

	/**
	 * get the list of pages
	 * 
	 * @return list of pages; an empty list at least
	 */
	public List<Integer> getPages() {
		return getPages(getSnipplets().size());
	}

	/**
	 * determine of the snipplets have a paginator
	 * 
	 * @return true or false
	 */
	public Boolean getHasPaginator() {
		return getHasPaginator(snipplets.size());
	}

	/**
	 * increase the paginator page by one
	 */
	public void increasePaginationPage() {
		increasePaginationPage(snipplets.size());
	}

	/**
	 * tell me if there are more paginator pages and we are not on the last one
	 * 
	 * @return true or false
	 */
	public boolean getTooManyPagesRight() {
		return getTooManyPagesRight(snipplets.size());
	}

	/**
	 * @return the snipplets
	 */
	public List<SnippletBean> getSnipplets() {
		return snipplets;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *          the description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the parent
	 */
	public Integer getParent() {
		return parent;
	}

	/**
	 * @param parent
	 *          the parent
	 */
	public void setParent(Integer parent) {
		this.parent = parent;
	}

	/**
	 * @return the variable group id
	 */
	public Integer getVariablegroup() {
		return variablegroup;
	}

	/**
	 * @param variablegroup
	 *          the variable group id
	 */
	public void setVariablegroup(Integer variablegroup) {
		this.variablegroup = variablegroup;
	}

	/**
	 * @return the report template id
	 */
	public Integer getFkFunctionlist() {
		return fkFunctionlist;
	}

	/**
	 * @param fkFunctionlist
	 *          the report template id
	 */
	public void setFkFunctionlist(Integer fkFunctionlist) {
		this.fkFunctionlist = fkFunctionlist;
	}

	/**
	 * @return the parent name
	 */
	public String getParentName() {
		return parentName;
	}

	/**
	 * @param parentName
	 *          the parent name
	 */
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	/**
	 * @return the privilege id of the variable group
	 */
	public Integer getVargroupPrivilege() {
		return vargroupPrivilege;
	}

	/**
	 * @return the acl
	 */
	public AclBean getAcl() {
		return acl;
	}

	/**
	 * @return the analysis function beans
	 */
	public List<AnaparfunBean> getAnaparfuns() {
		return anaparfuns;
	}

	/**
	 * @return the pagelength
	 */
	@Override
	public Integer getPagelength() {
		Integer pagelength = super.getPagelength();
		return pagelength == null ? snipplets.size() : pagelength;
	}

	/**
	 * @return the lastchange
	 */
	public Date getLastchange() {
		return lastchange;
	}
}
