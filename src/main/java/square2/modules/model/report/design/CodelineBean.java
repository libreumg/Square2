package square2.modules.model.report.design;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class CodelineBean implements Serializable, Comparable<CodelineBean>{
	private static final long serialVersionUID = 1L;
	
	private final Integer index;
	private final String result;
	private final String latexBefore;
	private final String latexAfter;
	private final String resultAnnotation;

	/**
	 * create a new codeline bean
	 * 
	 * @param index
	 *          the index
	 * @param result
	 *          the result
	 * @param latexBefore
	 *          the latexBefore
	 * @param latexAfter
	 *          the latexAfter
	 * @param resultAnnotation
	 *          the result annotation
	 */
	public CodelineBean(Integer index, String result, String latexBefore, String latexAfter, String resultAnnotation) {
		super();
		this.index = index;
		this.result = result;
		this.latexBefore = latexBefore;
		this.latexAfter = latexAfter;
		this.resultAnnotation = resultAnnotation;
	}

	@Override
	public int compareTo(CodelineBean o) {
		return index == null || o == null || o.getIndex() == null ? 0 : index.compareTo(o.getIndex());
	}

	/**
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @return the latex before
	 */
	public String getLatexBefore() {
		return latexBefore;
	}

	/**
	 * @return the latex after
	 */
	public String getLatexAfter() {
		return latexAfter;
	}

	/**
	 * @return the index
	 */
	public Integer getIndex() {
		return index;
	}

	/**
	 * @return the resultAnnotation
	 */
	public String getResultAnnotation() {
		return resultAnnotation;
	}
}
