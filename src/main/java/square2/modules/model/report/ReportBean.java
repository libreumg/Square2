package square2.modules.model.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;

import square2.converter.Selectable;
import square2.modules.model.datamanagement.ReleaseBean;
import square2.modules.model.report.design.ReportPeriodBean;

/**
 * 
 * @author henkej
 *
 */
public class ReportBean implements Serializable, Comparable<ReportBean>, Selectable<ReportBean> {
	private static final long serialVersionUID = 1L;

	private final Integer pk;
	private String name;
	private String description;
	private Integer fkParent;
	private Integer fkLatexdef;
	private Integer fkFunctionlist;
	private Integer fkVariablegroupdef;
	private String vargroupname;
	private Date started;
	private Date stopped;
	private String runlog;
	private String runsum;
	private String latexcode;
	private Date lastchange;
	private List<ReleaseBean> releases;
	private List<ReportPeriodBean> intervals;

	/**
	 * @param pk
	 *          the id of the report
	 */
	public ReportBean(Integer pk) {
		this.pk = pk;
		this.releases = new ArrayList<>();
		this.intervals = new ArrayList<>();
	}
	
	@Override
	public String toJson() {
		return new Gson().toJson(this);
	}

	@Override
	public ReportBean fromJson(String json) {
		return new Gson().fromJson(json, ReportBean.class);
	}

	@Override
	public int compareTo(ReportBean o) {
		return o == null || name == null ? 0 : name.compareTo(o.getName());
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append("ReportBean@{pk=").append(pk);
		buf.append(", name=").append(name);
		buf.append(", description=").append(description);
		buf.append(", fkParent=").append(fkParent);
		buf.append(", fkLatexdef=").append(fkLatexdef);
		buf.append(", fkFunctionlist=").append(fkFunctionlist);
		buf.append(", fkVariablegroupdef=").append(fkVariablegroupdef);
		buf.append(", started=").append(started);
		buf.append(", stopped=").append(stopped);
		buf.append(", runlog=").append(runlog);
		buf.append(", runsum=").append(runsum);
		buf.append(", latexcode=").append(latexcode);
		buf.append(", lastchange=").append(lastchange);
		buf.append(", releases=").append(releases);
		buf.append(", intervals=").append(intervals).append("}");
		return buf.toString();
	}

	/**
	 * add the release bean to the list of releases
	 * 
	 * @param bean the release bean
	 */
  public void addRelease(ReleaseBean bean) {
    if (releases == null) {
      releases = new ArrayList<>();
    }
    releases.add(bean);
  }

	/**
	 * check if the report is being calculated currently
	 * 
	 * @return true or false
	 */
	public boolean getIsRunning() {
		return this.started != null && this.stopped == null;
	}

	/**
	 * tell me if this report has releases
	 * 
	 * @return true or false
	 */
	public boolean getHasNoReleases() {
		return this.releases == null || this.releases.isEmpty();
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *          the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the fkParent
	 */
	public Integer getFkParent() {
		return fkParent;
	}

	/**
	 * @param fkParent
	 *          the fkParent to set
	 */
	public void setFkParent(Integer fkParent) {
		this.fkParent = fkParent;
	}

	/**
	 * @return the fkLatexdef
	 */
	public Integer getFkLatexdef() {
		return fkLatexdef;
	}

	/**
	 * @param fkLatexdef
	 *          the fkLatexdef to set
	 */
	public void setFkLatexdef(Integer fkLatexdef) {
		this.fkLatexdef = fkLatexdef;
	}

	/**
	 * @return the fkFunctionlist
	 */
	public Integer getFkFunctionlist() {
		return fkFunctionlist;
	}

	/**
	 * @param fkFunctionlist
	 *          the functionlist to set
	 */
	public void setFkFunctionlist(Integer fkFunctionlist) {
		this.fkFunctionlist = fkFunctionlist;
	}

	/**
	 * @return the fkVariablegroupdef
	 */
	public Integer getFkVariablegroupdef() {
		return fkVariablegroupdef;
	}

	/**
	 * @param fkVariablegroupdef
	 *          the fkVariablegroupdef to set
	 */
	public void setFkVariablegroupdef(Integer fkVariablegroupdef) {
		this.fkVariablegroupdef = fkVariablegroupdef;
	}

	/**
	 * @return the started
	 */
	public Date getStarted() {
		return started;
	}

	/**
	 * @param started
	 *          the started to set
	 */
	public void setStarted(Date started) {
		this.started = started;
	}

	/**
	 * @return the stopped
	 */
	public Date getStopped() {
		return stopped;
	}

	/**
	 * @param stopped
	 *          the stopped to set
	 */
	public void setStopped(Date stopped) {
		this.stopped = stopped;
	}

	/**
	 * @return the runlog
	 */
	public String getRunlog() {
		return runlog;
	}

	/**
	 * @param runlog
	 *          the runlog to set
	 */
	public void setRunlog(String runlog) {
		this.runlog = runlog;
	}

	/**
	 * @return the runsum
	 */
	public String getRunsum() {
		return runsum;
	}

	/**
	 * @param runsum
	 *          the runsum to set
	 */
	public void setRunsum(String runsum) {
		this.runsum = runsum;
	}

	/**
	 * @return the latexcode
	 */
	public String getLatexcode() {
		return latexcode;
	}

	/**
	 * @param latexcode
	 *          the latexcode to set
	 */
	public void setLatexcode(String latexcode) {
		this.latexcode = latexcode;
	}

	/**
	 * @return the lastchange
	 */
	public Date getLastchange() {
		return lastchange;
	}

	/**
	 * @param lastchange
	 *          the lastchange to set
	 */
	public void setLastchange(Date lastchange) {
		this.lastchange = lastchange;
	}

	/**
	 * @return the releases
	 */
	public List<ReleaseBean> getReleases() {
		return releases;
	}

	/**
	 * @return the intervals
	 */
	public List<ReportPeriodBean> getIntervals() {
		return intervals;
	}

	/**
	 * @return the vargroupname
	 */
	public String getVargroupname() {
		return vargroupname;
	}

	/**
	 * @param vargroupname the vargroupname to set
	 */
	public void setVargroupname(String vargroupname) {
		this.vargroupname = vargroupname;
	}
}
