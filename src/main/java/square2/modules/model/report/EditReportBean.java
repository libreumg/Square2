package square2.modules.model.report;

import java.util.ArrayList;
import java.util.List;

import square2.modules.model.report.design.SnippletBean;

/**
 * 
 * @author henkej
 *
 */
public class EditReportBean extends NamedReportBean {
  private static final long serialVersionUID = 1L;

  private final Integer numberOfSelected;
  private final Integer numberOfTotal;
  private Integer offset;
  private Integer pageLength;

  /**
   * @param bean the report bean
   * @param functionlistName the function list name
   * @param variablegroupName the variable group name
   * @param snipplets the snipplets
   * @param pageLength length of a snipplets page
   * @param offset position of snipplet for the page
   * @param numberOfSelected number of selected variables in the variable group
   * @param numberOfTotal number of total variables in the variable group
   */
  public EditReportBean(ReportBean bean, String functionlistName, String variablegroupName,
      List<SnippletBean> snipplets, Integer pageLength, Integer offset, Integer numberOfSelected,
      Integer numberOfTotal) {
    super(bean, functionlistName, variablegroupName, snipplets);
    this.offset = offset == null ? 1 : offset;
    this.pageLength = pageLength == null ? 8 : pageLength;
    this.numberOfSelected = numberOfSelected;
    this.numberOfTotal = numberOfTotal;
  }

  /**
   * @return the number of selected variables
   */
  public Integer getNumberOfSelected() {
    return numberOfSelected;
  }

  /**
   * @return number of total variables
   */
  public Integer getNumberOfTotal() {
    return numberOfTotal;
  }

  private Integer getSnippletSize() {
    return getSnipplets().getRowCount();
  }

  /**
   * get the list of snipplets of the current page
   * 
   * @return list of snipplets
   */
  public List<SnippletBean> getPageSnipplets() {
    List<SnippletBean> list = new ArrayList<>();
    Integer lowerBorder = offset;
    Integer upperBorder = offset + pageLength;
    for (SnippletBean bean : getSnipplets()) {
      Integer orderNr = bean.getOrderNr();
      if ((lowerBorder <= orderNr) && (upperBorder > orderNr)) {
        list.add(bean);
      }
    }
    list.sort((o1, o2) -> o1 == null || o2 == null ? 0 : o1.compareTo(o2));
    return list;
  }

  /**
   * set the page by setting the offset
   * 
   * @param page the page
   */
  public void setPage(Integer page) {
    this.offset = page * pageLength;
  }

  /**
   * set the lastpage
   */
  public void setLastPage() {
    Integer snippletSize = getSnippletSize();
    Integer pages = snippletSize / pageLength;
    if (snippletSize % pageLength > 0) {
      pages++;
    }
    this.offset = (pages - 1) * pageLength;
  }

  /**
   * check if this is the current page
   * 
   * @param page the page
   * @return true or false
   */
  public Boolean isCurrentPage(Integer page) {
    return (page * pageLength) == this.offset;
  }

  /**
   * get the list of pages
   * 
   * @return list of pages; an empty list at least
   */
  public List<Integer> getPages() {
    List<Integer> list = new ArrayList<>();
    if (getHasPaginator()) {
      Integer pages = getSnippletSize() / pageLength;
      if (getSnippletSize() % pageLength > 0) {
        pages++;
      }
      for (int i = 0; i < pages; i++) {
        list.add(i);
      }
    }
    return list;
  }

  /**
   * determine of the snipplets have a paginator
   * 
   * @return true or false
   */
  public Boolean getHasPaginator() {
    return pageLength == null || offset == null || pageLength < getSnippletSize();
  }

  /**
   * @return the pageLength
   */
  public Integer getPageLength() {
    return pageLength;
  }
}
