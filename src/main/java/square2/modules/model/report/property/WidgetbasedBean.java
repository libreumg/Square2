package square2.modules.model.report.property;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class WidgetbasedBean implements Serializable, Comparable<WidgetbasedBean> {
  private static final long serialVersionUID = 1L;

  private final Integer pk;
  private final String name;
  private final Integer fkFunctioninputtype;
  private final String functioninputtypeName;

  /**
   * create a new widgetbased bean
   * 
   * @param pk the ID of the database
   * @param name the name of the element
   * @param fkFunctioninputtype the reference to the input type
   * @param functioninputtypeName the name of the input type
   */
  public WidgetbasedBean(Integer pk, String name, Integer fkFunctioninputtype, String functioninputtypeName) {
    super();
    this.pk = pk;
    this.name = name;
    this.fkFunctioninputtype = fkFunctioninputtype;
    this.functioninputtypeName = functioninputtypeName;
  }

  @Override
  public int compareTo(WidgetbasedBean o) {
    return o == null || o.getName() == null ? 0 : o.getName().compareTo(name);
  }

  /**
   * @return the pk
   */
  public Integer getPk() {
    return pk;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @return the fkFunctioninputtype
   */
  public Integer getFkFunctioninputtype() {
    return fkFunctioninputtype;
  }

  /**
   * @return the functioninputtypeName
   */
  public String getFunctioninputtypeName() {
    return functioninputtypeName;
  }

}
