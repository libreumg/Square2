package square2.modules.model.report.property;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class ReportPropertyBean implements Serializable, Comparable<ReportPropertyBean> {
  private static final long serialVersionUID = 1L;

  private final Integer pk;
  private String key;
  private String value;
  private String defaultValue;
  private Integer orderNr;
  private String typeRestriction;
  private String functioninputtypeName;
  private Integer fkWidgetbased;

  private boolean dirty;

  /**
   * create a new report property bean
   * 
   * @param pk the id of the data set; null if this does not yet exist in the database
   * @param key the key
   */
  public ReportPropertyBean(Integer pk, String key) {
    this.pk = pk;
    this.key = key;
    this.dirty = false;
  }
  
  @Override
  public String toString() {
    StringBuilder buf = new StringBuilder();
    buf.append("ReportPropertyBean@{pk=").append(pk);
    buf.append(", key=").append(key);
    buf.append(", value=").append(value);
    buf.append(", defaultValue=").append(defaultValue);
    buf.append(", orderNr=").append(orderNr);
    buf.append(", typeRestriction=").append(typeRestriction);
    buf.append(", functioninputtypeName=").append(functioninputtypeName);
    buf.append(", fkWidgetbased=").append(fkWidgetbased);
    buf.append(", dirty=").append(dirty);
    buf.append("}");
    return buf.toString();
  }

  @Override
  public int compareTo(ReportPropertyBean o) {
    return o.getKey() == null ? 0 : o.getKey().compareTo(getKey());
  }
  
  /**
   * create a new instance with copied content
   */
  public ReportPropertyBean clone() {
    ReportPropertyBean bean = new ReportPropertyBean(this.getPk(), this.getKey());
    bean.setValue(this.getValue());
    bean.setDefaultValue(this.getDefaultValue());
    bean.setFkWidgetbased(this.getFkWidgetbased());
    bean.setFunctioninputtypeName(this.getFunctioninputtypeName());
    bean.setOrderNr(this.getOrderNr());
    bean.setTypeRestriction(this.getTypeRestriction());
    return bean;
  }

  /**
   * makes the entry dirty; e.g. for creating new beans that do not come from the database
   * 
   * @return this class
   */
  public ReportPropertyBean makeDirty() {
    this.dirty = true;
    return this;
  }

  /**
   * if changes are not stored to the database yet
   * 
   * @return true or false
   */
  public boolean isDirty() {
    return dirty;
  }

  /**
   * @return the value
   */
  public String getValue() {
    return value;
  }

  /**
   * @param value the value to set
   */
  public void setValue(String value) {
    this.value = value;
  }

  /**
   * @return the defaultValue
   */
  public String getDefaultValue() {
    return defaultValue;
  }

  /**
   * @param defaultValue the defaultValue to set
   */
  public void setDefaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
  }

  /**
   * @return the orderNr
   */
  public Integer getOrderNr() {
    return orderNr;
  }

  /**
   * @param orderNr the orderNr to set
   */
  public void setOrderNr(Integer orderNr) {
    this.orderNr = orderNr;
  }

  /**
   * @return the typeRestriction
   */
  public String getTypeRestriction() {
    return typeRestriction;
  }

  /**
   * @param typeRestriction the typeRestriction to set
   */
  public void setTypeRestriction(String typeRestriction) {
    this.typeRestriction = typeRestriction;
  }

  /**
   * @return the functioninputtypeName
   */
  public String getFunctioninputtypeName() {
    return functioninputtypeName;
  }

  /**
   * @param functioninputtypeName the functioninputtypeName to set
   */
  public void setFunctioninputtypeName(String functioninputtypeName) {
    this.functioninputtypeName = functioninputtypeName;
  }

  /**
   * @return the pk
   */
  public Integer getPk() {
    return pk;
  }

  /**
   * @return the key
   */
  public String getKey() {
    return key;
  }

  /**
   * @param key the key to set
   */
  public void setKey(String key) {
    this.key = key;
  }

  /**
   * @return the fkWidgetbased
   */
  public Integer getFkWidgetbased() {
    return fkWidgetbased;
  }

  /**
   * @param fkWidgetbased the fkWidgetbased to set
   */
  public void setFkWidgetbased(Integer fkWidgetbased) {
    this.fkWidgetbased = fkWidgetbased;
  }
}
