package square2.modules.model.statistic;

import java.util.ArrayList;
import java.util.List;

import square2.modules.model.UserAccessLevelBean;

/**
 * 
 * @author henkej
 *
 */
public class FunctionRightBean {
	private FunctionBean function;
	private final List<UserAccessLevelBean> users;
	private Integer newUsnr;
	private Integer newAccessLevel;

	/**
	 * create new function right bean
	 */
	public FunctionRightBean() {
		users = new ArrayList<>();
	}

	/**
	 * @return the function
	 */
	public FunctionBean getFunction() {
		return function;
	}

	/**
	 * @param function
	 *          the function to set
	 */
	public void setFunction(FunctionBean function) {
		this.function = function;
	}

	/**
	 * @return the newUsnr
	 */
	public Integer getNewUsnr() {
		return newUsnr;
	}

	/**
	 * @param newUsnr
	 *          the newUsnr to set
	 */
	public void setNewUsnr(Integer newUsnr) {
		this.newUsnr = newUsnr;
	}

	/**
	 * @return the newAccessLevel
	 */
	public Integer getNewAccessLevel() {
		return newAccessLevel;
	}

	/**
	 * @param newAccessLevel
	 *          the newAccessLevel to set
	 */
	public void setNewAccessLevel(Integer newAccessLevel) {
		this.newAccessLevel = newAccessLevel;
	}

	/**
	 * @return the users
	 */
	public List<UserAccessLevelBean> getUsers() {
		return users;
	}
}
