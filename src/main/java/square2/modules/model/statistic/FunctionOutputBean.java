package square2.modules.model.statistic;

import java.io.Serializable;

/**
 * 
 * @author henkej
 * 
 */
public class FunctionOutputBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private final Integer id;
	private String name;
	private String description;
	private Integer usage;

	/**
	 * create new function output bean
	 * 
	 * @param id
	 *          the id
	 */
	public FunctionOutputBean(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
		buf.append("@{id=").append(id);
		buf.append(",name=").append(name);
		buf.append(",description=").append(description);
		buf.append(",usage=").append(usage);
		buf.append("}");
		return buf.toString();
	}

	/**
	 * @return the usage label
	 */
	public String getUsageLabel() {
		return new StringBuilder("label.statistic.outputusage.").append(usage).toString();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *          the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the usage
	 */
	public Integer getUsage() {
		return usage;
	}

	/**
	 * @param usage
	 *          the usage to set
	 */
	public void setUsage(Integer usage) {
		this.usage = usage;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
}
