package square2.modules.model.template.report;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class FunctionoutputBean implements Serializable, Comparable<FunctionoutputBean> {
	private static final long serialVersionUID = 1L;

	private final Integer fkFunctionoutput;
	private final String name;
	
	/**
	 * @param fkFunctionoutput the id of the function output
	 * @param name the name of the function output
	 */
	public FunctionoutputBean(Integer fkFunctionoutput, String name) {
		this.fkFunctionoutput = fkFunctionoutput;
		this.name = name;
	}

  @Override
  public int compareTo(FunctionoutputBean bean) {
    return bean == null || name == null || bean.getName() == null ? 0 : name.compareTo(bean.getName());
  }

  /**
	 * @return the fkFunctionoutput
	 */
	public Integer getFkFunctionoutput() {
		return fkFunctionoutput;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
