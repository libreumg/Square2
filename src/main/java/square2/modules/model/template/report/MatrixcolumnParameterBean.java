package square2.modules.model.template.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import square2.converter.Selectable;

/**
 * 
 * @author henkej
 *
 */
public class MatrixcolumnParameterBean implements Serializable, Selectable<MatrixcolumnParameterBean> {
  private static final long serialVersionUID = 1L;

  private final Integer fkMatrixcolumn;
  private final String name;
  private final String description;
  private final String vignette;
  private final List<FunctionoutputBean> functionoutputs;

  /**
   * @param fkMatrixcolumn the matrixcolumn pk
   * @param name the name of the matrixcolumn function
   * @param description the description
   * @param vignette the vignette of the corresponding R function
   */
  public MatrixcolumnParameterBean(Integer fkMatrixcolumn, String name, String description, String vignette) {
    this.fkMatrixcolumn = fkMatrixcolumn;
    this.name = name;
    this.description = description;
    this.vignette = vignette;
    this.functionoutputs = new ArrayList<FunctionoutputBean>();
  }

  @Override
  public String toJson() {
    return new Gson().toJson(this);
  }

  @Override
  public MatrixcolumnParameterBean fromJson(String json) {
    return new Gson().fromJson(json, MatrixcolumnParameterBean.class);
  }

  @Override
  public String toString() {
    StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
    buf.append("@{fkMatrixcolumn=").append(fkMatrixcolumn);
    buf.append(", name=").append(name);
    buf.append(", description=").append(description);
    buf.append(", vignette=").append(vignette);
    buf.append(", functionoutputs=").append(functionoutputs);
    buf.append("}");
    return buf.toString();
  }

  /**
   * @return the fkMatrixcolumn
   */
  public Integer getFkMatrixcolumn() {
    return fkMatrixcolumn;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @return the functionoutputs
   */
  public List<FunctionoutputBean> getFunctionoutputs() {
    return functionoutputs;
  }

  /**
   * @return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @return the vignette
   */
  public String getVignette() {
    return vignette;
  }
}
