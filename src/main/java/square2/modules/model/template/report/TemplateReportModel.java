package square2.modules.model.template.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;

import square2.db.control.ReportGateway;
import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.report.design.ReportDesignBean;
import square2.modules.model.report.design.ReportDesignModel;
import square2.modules.model.report.design.SnippletBean;
import square2.modules.model.report.design.SnipplettypeBean;
import square2.modules.model.template.UsageBean;

/**
 * 
 * @author henkej
 *
 */
@Named
@SessionScoped
public class TemplateReportModel extends ReportDesignModel implements Serializable {
  private static final long serialVersionUID = 1L;
  private static final Logger LOGGER = LogManager.getLogger(TemplateReportModel.class);

  /**
   * load information about usage of this report template
   * 
   * @param facesContext of this current function call
   * @param report pk of t_qs_report
   * @return true if no error occurred, false otherwise
   */
  public boolean loadUsage(SquareFacesContext facesContext, Integer report) {
    try {
      List<String> reportList = new ReportGateway(facesContext).getAllReportNamesOfReportTemplate(report);
      setUsage(new ArrayList<>());
      for (String item : reportList) {
        getUsage().add(new UsageBean(item, UsageBean.REPORT));
      }
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * copy a report template
   * 
   * @param facesContext of this current function call
   * @return true if successful, false otherwise
   */
  public boolean copyReportTemplate(SquareFacesContext facesContext) {
    try {
      new ReportGateway(facesContext).copyReportTemplate(getReport());
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * update type of snipplet
   * 
   * @param facesContext of this current function call
   * @param bean to be updated
   * @param type to be used
   * @return true if successful, false otherwise
   */
  public boolean changeSnippletType(SquareFacesContext facesContext, SnippletBean bean, SnipplettypeBean type) {
    bean.setType(type);
    return true;
  }

  /**
   * prepare all to load the add page
   * 
   * @param facesContext the context of this function call
   */
  public void toAdd(SquareFacesContext facesContext) {
    Integer pagelength = facesContext.getProfile().getConfigInteger("config.template.paginator.pagelength", 20);
    setReport(new ReportDesignBean(new AclBean(), null, pagelength, null, 0));
  }

  /**
   * get the function output beans for fkMatrixcolumn
   * 
   * @param fkMatrixcolumn the id of the matrix column function
   * @return a list of function output beans; an empty one at least
   */
  public List<FunctionoutputBean> getCurrentExportFunctionoutputBeans(Integer fkMatrixcolumn) {
    if (fkMatrixcolumn == null) {
      return new ArrayList<>();
    } else {
      for (MatrixcolumnParameterBean bean : getrExports()) {
        if (bean.getFkMatrixcolumn().equals(fkMatrixcolumn)) {
          return bean.getFunctionoutputs(); // interrupt immediately to reduce calculation time
        }
      }
      LOGGER.warn("fkMatrixcolumn {} not found in list of exports", fkMatrixcolumn);
      return new ArrayList<>();
    }
  }

  /**
   * get all matrix column parameter beans of the snipplet
   * 
   * @param bean the snipplet bean
   * @return the list
   */
  public List<MatrixcolumnParameterBean> getMatrixColumnParameterBeans(SnippletBean bean) {
    List<MatrixcolumnParameterBean> list = getrExports();
    if (bean.getMatrixcolumnBean() != null) {
      for (MatrixcolumnParameterBean item : list) {
        if (item != null) {
          String itemName = item.getName();
          if (itemName != null) { // for not yet selected items, this is null
            String mcName = bean.getMatrixcolumnBean().getName();
            if (mcName != null) { // for empty lists of functions, this is null
              if (itemName.equals(mcName)) {
                bean.setMatrixcolumnBean(item); // replace the referenced bean so that the dropdown can use it
              }
            }
          } 
        } else {
          LOGGER.warn("item in list of matrixcolumnparameterbeans is null");
        }
      }
    }
    return list;
  }

  /**
   * generate the auto complete for markdown snipplets
   * @param query the query
   * 
   * @return a list of string of markdown result calls usable by power users
   */
  public List<String> tryautocomplete(String query){
    List<String> list = new ArrayList<String>();
    for (MatrixcolumnParameterBean mpBean : getrExports()) {
      for (FunctionoutputBean foBean : mpBean.getFunctionoutputs()) {
        StringBuilder buf = new StringBuilder();
        buf.append(mpBean.getName());
        buf.append(".");
        buf.append(foBean.getName());
        if (buf.toString().toLowerCase().contains(query == null ? "" : query.toLowerCase())) {
          list.add(buf.toString());
        }
      }
    }
    return list;
  }
}
