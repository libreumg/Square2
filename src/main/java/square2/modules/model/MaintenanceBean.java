package square2.modules.model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * 
 * @author henkej
 *
 */
public class MaintenanceBean {
	private final String message;
	private final Timestamp from;
	private final Timestamp to;

	/**
	 * create new maintenance bean
	 * 
	 * @param message
	 *          the message
	 * @param from
	 *          the from
	 * @param to
	 *          the to
	 */
	public MaintenanceBean(String message, Timestamp from, Timestamp to) {
		super();
		this.message = message;
		this.from = from;
		this.to = to;
	}

	/**
	 * return a human readable version
	 */
	public String toString() {
	  StringBuilder buf = new StringBuilder();
	  buf.append(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(from));
	  buf.append(" - ");
    buf.append(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(to));
	  buf.append(": ");
	  buf.append(message);
	  return buf.toString();
	}
	
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return the from
	 */
	public Timestamp getFrom() {
		return from;
	}

	/**
	 * @return the to
	 */
	public Timestamp getTo() {
		return to;
	}
}
