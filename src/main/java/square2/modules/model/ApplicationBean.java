package square2.modules.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import square2.help.SquareFacesContext;
import square2.modules.model.admin.UserLoginBean;

/**
 * 
 * @author henkej
 *
 */
@Named(value = "applicationBean")
@ApplicationScoped
public class ApplicationBean {
	private List<UserLoginBean> userLogins;

	/**
	 * create new application bean
	 */
	public ApplicationBean() {
		userLogins = new ArrayList<>();
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
		buf.append("@{userLogins=").append(userLogins);
		buf.append("}");
		return buf.toString();
	}

	/**
	 * add profile
	 * 
	 * @param bean
	 *          the profile
	 * @param facesContext
	 *          the context of this function call
	 */
	public void addProfile(ProfileBean bean, SquareFacesContext facesContext) {
		Iterator<UserLoginBean> i = userLogins.iterator();
		while (i.hasNext()) {
			UserLoginBean uBean = i.next();
			if (uBean.getUsnr().equals(bean.getUsnr())) {
				facesContext.notifyWarning("warning.login.missing.logout",
						new SimpleDateFormat("dd.MM.yyyy, HH:mm").format(uBean.getStart()));
				i.remove();
			}
		}
		userLogins.add(new UserLoginBean(bean));
	}

	/**
	 * @return the profiles
	 */
	public List<UserLoginBean> getProfiles() {
		return userLogins;
	}

	/**
	 * remove profile from list
	 * 
	 * @param usnr
	 *          the id of the user
	 */
	public void removeProfile(Integer usnr) {
		Iterator<UserLoginBean> i = userLogins.iterator();
		while (i.hasNext()) {
			if (i.next().getUsnr().equals(usnr)) {
				i.remove();
				return;
			}
		}
	}
}