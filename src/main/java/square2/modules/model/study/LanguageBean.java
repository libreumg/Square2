package square2.modules.model.study;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author henkej
 *
 */
public class LanguageBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private final String langKey;
	private Map<String, String> map;

	/**
	 * create new language bean
	 * 
	 * @param langKey
	 *          the language key
	 */
	public LanguageBean(String langKey) {
		this.langKey = langKey;
		map = new HashMap<>();
	}

	/**
	 * @return the language key
	 */
	public String getLangKey() {
		return langKey;
	}

	/**
	 * @return the translations
	 */
	public Set<Map.Entry<String, String>> getTranslations() {
		return map.entrySet();
	}

	/**
	 * remove a translation
	 * 
	 * @param isocode
	 *          the isocode
	 */
	public void deleteLangValue(String isocode) {
		map.remove(isocode);
	}

	/**
	 * add a translation
	 * 
	 * @param isocode
	 *          the isocode
	 * @param value
	 *          the value
	 */
	public void setLangValue(String isocode, String value) {
		map.put(isocode, value);
	}

	/**
	 * get a translation
	 * 
	 * @param isocode
	 *          the isocode
	 * @return the value
	 */
	public String getLangValue(String isocode) {
		return map.get(isocode);
	}
}
