package square2.modules.model.study;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class ConfounderBean implements Serializable {
  private static final long serialVersionUID = 1L;

  private MetadatatypeBean metadataType;
  private Integer fkReferenceElement;
  private List<ElementBean> selectedVars;

  /**
   * create a new metadata bean with an empty list of selected vars
   */
  public ConfounderBean() {
    this.selectedVars = new ArrayList<>();
    this.metadataType = new MetadatatypeBean(null);
  }

  @Override
  public ConfounderBean clone() {
    ConfounderBean bean = new ConfounderBean();
    bean.setMetadataType(metadataType);
    bean.setFkReferenceElement(fkReferenceElement);
    bean.getSelectedVars().addAll(selectedVars);
    return bean;
  }
  
  /**
   * @return the meta data type
   */
  public String getMetadataTypeJson() {
    return metadataType == null ? null : metadataType.toJson();
  }

  /**
   * @param metadataType the meta data type
   */
  public void setMetadataTypeJson(String metadataType) {
    this.metadataType = new MetadatatypeBean(null).fromJson(metadataType);
  }

  /**
   * @return the meta data type
   */
  public MetadatatypeBean getMetadataType() {
    return metadataType;
  }

  /**
   * @param metadataType the meta data type
   */
  public void setMetadataType(MetadatatypeBean metadataType) {
    this.metadataType = metadataType;
  }

  /**
   * @return the reference element id
   */
  public Integer getFkReferenceElement() {
    return fkReferenceElement;
  }

  /**
   * @param fkReferenceElement the reference element id
   */
  public void setFkReferenceElement(Integer fkReferenceElement) {
    this.fkReferenceElement = fkReferenceElement;
  }

  /**
   * @return the selectedVars
   */
  public List<ElementBean> getSelectedVars() {
    return selectedVars;
  }
}
