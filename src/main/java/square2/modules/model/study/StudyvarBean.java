package square2.modules.model.study;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class StudyvarBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private final Integer id;
	private final String name;
	private Integer timevar;

	/**
	 * @param id the id
	 * @param name the name
	 */
	public StudyvarBean(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	/**
	 * 
	 * @param id the id
	 * @param name the name
	 * @param timevar the timevar
	 */
	public StudyvarBean(Integer id, String name, Integer timevar) {
		super();
		this.id = id;
		this.name = name;
		this.timevar = timevar;
	}

	/**
	 * @return the timevar
	 */
	public Integer getTimevar() {
		return timevar;
	}

	/**
	 * @param timevarString the timevar to set
	 */
	public void setTimevar(String timevarString) {
		this.timevar = Integer.valueOf(timevarString);
	}
	
	/**
	 * @param timevar the timevar to set
	 */
	public void setTimevar(Integer timevar) {
		this.timevar = timevar;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
