package square2.modules.model.study;

import java.util.ArrayList;
import java.util.List;

import ship.jsf.components.selectFilterableMenu.BeanWrapperInterface;
import ship.jsf.components.selectFilterableMenu.FilterableListInterface;

/**
 * 
 * @author henkej
 *
 */
public class FilterableListVariableList extends ArrayList<BeanWrapperInterface> implements FilterableListInterface {
	private static final long serialVersionUID = 4235776397650317520L;

	/**
	 * create new filterable list of a variable list
	 * 
	 * @param list
	 *          the list
	 */
	public FilterableListVariableList(List<ListElement> list) {
		super(convertToWrapper(list));
	}

	/**
	 * convert list to filterable one
	 * 
	 * @param list
	 * @return
	 */
	private static List<BeanWrapperInterface> convertToWrapper(List<ListElement> list) {
		List<BeanWrapperInterface> l = new ArrayList<>();
		for (ListElement bean : list) {
			l.add(new ListVariableFilterWrapper(bean));
		}
		return l;
	}
}
