package square2.modules.model.study.io;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import square2.help.SquareFacesContext;

/**
 * @author henkej
 *
 */
public class OpalBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private final List<String> urls;
	private final List<String> projects;
	private final List<String> tables;
	private String url;
	private String project;
	private String table;
	private String parentUniqueName;

	/**
	 * create a new opal bean, containing the opal urls
	 * 
	 * @param urls the possible opal urls
	 */
	public OpalBean(List<String> urls) {
		this.urls = urls;
		this.projects = new ArrayList<>();
		this.tables = new ArrayList<>();
	}

	/**
	 * check if all fields are filled; if not, throw an exception
	 * 
	 * @param facesContext the context of this call
	 * @throws IOException on errors
	 */
	public void checkNotEmpty(SquareFacesContext facesContext) throws IOException {
		if (this.url == null) {
			throw new IOException(facesContext.translate("error.missing.import", "label.study.io.opal.select"));
		} else if (this.project == null) {
			throw new IOException(facesContext.translate("error.missing.import", "label.study.io.opal.project"));
		} else if (this.table == null) {
			throw new IOException(facesContext.translate("error.missing.import", "label.study.io.opal.table"));
		} else if (this.parentUniqueName == null) {
			throw new IOException(facesContext.translate("error.missing.import", "label.study.io.opal.parent"));
		}
	}

	/**
	 * @return tru if the urls is not null and not empty
	 */
	public boolean getHasUrls() {
		return this.urls != null && this.urls.size() > 0;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the project
	 */
	public String getProject() {
		return project;
	}

	/**
	 * @param project the project to set
	 */
	public void setProject(String project) {
		this.project = project;
	}

	/**
	 * @return the table
	 */
	public String getTable() {
		return table;
	}

	/**
	 * @param table the table to set
	 */
	public void setTable(String table) {
		this.table = table;
	}

	/**
	 * @return the urls
	 */
	public List<String> getUrls() {
		return urls;
	}

	/**
	 * @return the projects
	 */
	public List<String> getProjects() {
		return projects;
	}

	/**
	 * @return the tables
	 */
	public List<String> getTables() {
		return tables;
	}

	/**
	 * @return the parent unique name
	 */
	public String getParentUniqueName() {
		return parentUniqueName;
	}

	/**
	 * @param parentUniqueName the name
	 */
	public void setParentUniqueName(String parentUniqueName) {
		this.parentUniqueName = parentUniqueName;
	}
}
