package square2.modules.model.study;

import java.io.Serializable;

import com.google.gson.Gson;

import square2.converter.Selectable;

/**
 * 
 * @author henkej
 *
 */
public class MetadatatypeBean implements Serializable, Selectable<MetadatatypeBean> {
	private static final long serialVersionUID = 1L;

	private final Integer pk;

	private String name;
	private String description;
	
	/**
	 * @param pk the primary key of the metadatatype in the db; null if new
	 */
	public MetadatatypeBean(Integer pk) {
		this.pk = pk;
	}
	
	@Override
	public String toJson() {
		return new Gson().toJson(this);
	}

	@Override
	public MetadatatypeBean fromJson(String json) {
		return new Gson().fromJson(json, MetadatatypeBean.class);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}
}
