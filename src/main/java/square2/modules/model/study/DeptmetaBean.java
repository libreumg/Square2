package square2.modules.model.study;

import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class DeptmetaBean {
	private final Integer pk;
	private final String uniqueName;
	private final Integer fkMetadatatype;
	private final String typeName;
	private final String affected;
	private final List<String> affectedUniqueNames;
	private final List<Integer> affectedPks;
	private final boolean persisted;

	/**
	 * create new dept meta bean
	 * 
	 * @param pk the id
	 * @param uniqueName the unique name
	 * @param typeName the type name
	 * @param affected the affected
	 * @param affectedUniqueNames the affected unique names
	 * @param fkMetadatatype the metadatatype id
	 * @param affectedPks the affected pks
	 * @param persisted if true, that one is found in the database
	 */
	public DeptmetaBean(Integer pk, String uniqueName, String typeName, String affected, List<String> affectedUniqueNames,
			Integer fkMetadatatype, List<Integer> affectedPks, boolean persisted) {
		super();
		this.pk = pk;
		this.uniqueName = uniqueName;
		this.typeName = typeName;
		this.affected = affected;
		this.affectedUniqueNames = affectedUniqueNames;
		this.fkMetadatatype = fkMetadatatype;
		this.affectedPks = affectedPks;
		this.persisted = persisted;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append("DeptmetaBean@{pk=").append(pk);
		buf.append(", uniqueName=").append(uniqueName);
		buf.append(", fkMetadatatype=").append(fkMetadatatype);
		buf.append(", typeName=").append(typeName);
		buf.append(", affected=").append(affected);
		buf.append(", affectedUniqueNames=").append(affectedUniqueNames);
		buf.append(", affectedPks=").append(affectedPks).append("}");
		return buf.toString();
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @return the name
	 */
	public String getUniqueName() {
		return uniqueName;
	}

	/**
	 * @return the type name
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * @return the affected
	 */
	public String getAffected() {
		return affected;
	}

	/**
	 * @return the affected unique names
	 */
	public List<String> getAffectedUniqueNames() {
		return affectedUniqueNames;
	}

	/**
	 * @return the fkMetadatatype
	 */
	public Integer getFkMetadatatype() {
		return fkMetadatatype;
	}

	/**
	 * @return the affectedPks
	 */
	public List<Integer> getAffectedPks() {
		return affectedPks;
	}

	/**
	 * @return the persisted
	 */
	public boolean getPersisted() {
		return persisted;
	}
}
