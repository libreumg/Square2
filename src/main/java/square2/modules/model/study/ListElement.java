package square2.modules.model.study;

import java.io.Serializable;

import org.jooq.JSONB;

/**
 * 
 * @author henkej
 *
 */
public class ListElement implements Serializable, Comparable<ListElement> {
	private static final long serialVersionUID = 1L;

	private Integer fkElement;
	private JSONB translation;
	private final String name;
	private Boolean chosen;
	private Boolean dirty;

	/**
	 * generate new list element
	 * 
	 * @param fkElement
	 *          the id of the element
	 * @param translation
	 *          the translation
	 * @param name
	 *          the name
	 * @param chosen
	 *          the chosen flag
	 * @param dirty
	 *          the dirty flag
	 */
	public ListElement(Integer fkElement, JSONB translation, String name, Boolean chosen, Boolean dirty) {
		this.fkElement = fkElement;
		this.translation = translation;
		this.name = name;
		this.chosen = chosen;
		this.dirty = dirty == null ? false : dirty;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append(fkElement);
		buf.append("{").append(name);
		buf.append(",").append(chosen);
		buf.append("}");
		return buf.toString();
	}

	/**
	 * @deprecated use compareByNameTo instead
	 */
	@Deprecated
	@Override
	public int compareTo(ListElement o) {
		return compareByNameTo(o);
	}

	/**
	 * compare this element with o by its names
	 * 
	 * @param o
	 *          the element to be compared with
	 * @return the comparison, 0 for equality
	 */
	public int compareByNameTo(ListElement o) {
		return name == null || o == null || o.getName() == null ? 0 : name.compareTo(o.getName());
	}

	/**
	 * compare this element with o by its fkElements
	 * 
	 * @param o
	 *          the element to be compared with
	 * @return the comparison, 0 for equality
	 */
	public int compareByFkElementTo(ListElement o) {
		return fkElement == null || o == null || o.getFkElement() == null ? 0 : fkElement.compareTo(o.getFkElement());
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param fkElement
	 *          the id of the element
	 */
	public void setFkElement(Integer fkElement) {
		this.fkElement = fkElement;
	}

	/**
	 * @return the id of the element
	 */
	public Integer getFkElement() {
		return fkElement;
	}

	/**
	 * @return the chosen flag
	 */
	public Boolean getChosen() {
		return chosen;
	}

	/**
	 * @param chosen
	 *          the chosen flag
	 */
	public void setChosen(Boolean chosen) {
		this.dirty = true;
		this.chosen = chosen;
	}

	/**
	 * @return the dirty flag
	 */
	public Boolean getDirty() {
		return dirty;
	}

	/**
	 * @return the translation
	 */
	public JSONB getTranslation() {
		return translation;
	}
}
