package square2.modules.model.study;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class VariablegrouplistBean {
	private List<VariablegroupBean> groups;

	private VariablegroupBean newBean;

	/**
	 * create new variable group list bean
	 */
	public VariablegrouplistBean() {
		this.groups = new ArrayList<>();
		this.newBean = new VariablegroupBean();
	}

	/**
	 * @return the variable groups
	 */
	public List<VariablegroupBean> getGroups() {
		return groups;
	}

	/**
	 * reset the new bean
	 */
	public void resetNewBean() {
		this.newBean = new VariablegroupBean();
	}

	/**
	 * @return the new bean
	 */
	public VariablegroupBean getNewBean() {
		return newBean;
	}
}
