package square2.modules.model.study;

/**
 * 
 * @author henkej
 *
 */
public class ListVariable {
	private Integer fkElement;
	private final String name;
	private Integer rightId;

	/**
	 * create new list variable
	 * 
	 * @param fkElement
	 *          the id of the element
	 * @param name
	 *          the name
	 * @param rightId
	 *          the id of the privilege
	 */
	public ListVariable(Integer fkElement, String name, Integer rightId) {
		this.fkElement = fkElement;
		this.name = name;
		this.rightId = rightId;
	}

	/**
	 * @return the fkElement
	 */
	public Integer getFkElement() {
		return fkElement;
	}

	/**
	 * @param fkElement
	 *          the fkElement to set
	 */
	public void setFkElement(Integer fkElement) {
		this.fkElement = fkElement;
	}

	/**
	 * @return the rightId
	 */
	public Integer getRightId() {
		return rightId;
	}

	/**
	 * @param rightId
	 *          the rightId to set
	 */
	public void setRightId(Integer rightId) {
		this.rightId = rightId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
