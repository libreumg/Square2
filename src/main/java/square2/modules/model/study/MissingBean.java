package square2.modules.model.study;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class MissingBean implements Serializable {
  private static final long serialVersionUID = 3L;

  private final Integer pkMissing;
  private String code;
  private String nameInStudy;
  private Integer pkType;
  private String typeName;

  /**
   * generate a new missing bean
   * 
   * @param pkMissing the primary key if available, use null otherwise
   * @param code the missing code
   * @param nameInStudy the name of the missing
   * @param pkType the primary key of the type if available, use null otherwise
   * @param typeName the name of the type
   */
  public MissingBean(Integer pkMissing, String code, String nameInStudy, Integer pkType, String typeName) {
    this.pkMissing = pkMissing;
    this.code = code;
    this.nameInStudy = nameInStudy;
    this.pkType = pkType;
    this.typeName = typeName;
  }

  /**
   * a missing bean is empty if there is not code or no type (typeName)
   * 
   * @return true or false
   */
  public boolean isEmpty() {
    return code == null || code.isBlank() || typeName == null || typeName.isBlank();
  }

  /**
   * @param pkType the type pk
   */
  public void setFkMissingtype(Integer pkType) {
    this.pkType = pkType;
  }

  /**
   * @return the code
   */
  public String getCode() {
    return code;
  }

  /**
   * @param code the code to set
   */
  public void setCode(String code) {
    this.code = code;
  }

  /**
   * @return the nameInStudy
   */
  public String getNameInStudy() {
    return nameInStudy;
  }

  /**
   * @param nameInStudy the nameInStudy to set
   */
  public void setNameInStudy(String nameInStudy) {
    this.nameInStudy = nameInStudy;
  }

  /**
   * @return the typeName
   */
  public String getTypeName() {
    return typeName;
  }

  /**
   * @param typeName the typeName to set
   */
  public void setTypeName(String typeName) {
    this.typeName = typeName;
  }

  /**
   * @return the pkMissing
   */
  public Integer getPkMissing() {
    return pkMissing;
  }

  /**
   * @return the pkType
   */
  public Integer getPkType() {
    return pkType;
  }
}
