package square2.modules.model.study;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author henkej
 *
 */
public class ImportLanguageBean {
	private String variable;
	private Map<String, String> map;
	private Integer fkLang;

	/**
	 * create new import language bean
	 */
	public ImportLanguageBean() {
		map = new HashMap<>();
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
		buf.append("@{variable=").append(variable);
		buf.append(",map=").append(map.toString());
		buf.append(",fkLang=").append(fkLang);
		buf.append("}");
		return buf.toString();
	}

	/**
	 * @return the variable
	 */
	public String getVariable() {
		return variable;
	}

	/**
	 * @param variable
	 *          the variable
	 */
	public void setVariable(String variable) {
		this.variable = variable;
	}

	/**
	 * @return the map
	 */
	public Map<String, String> getMap() {
		return map;
	}

	/**
	 * @return the fk lang
	 */
	public Integer getFkLang() {
		return fkLang;
	}

	/**
	 * @param fkLang
	 *          the fk lang
	 */
	public void setFkLang(Integer fkLang) {
		this.fkLang = fkLang;
	}
}
