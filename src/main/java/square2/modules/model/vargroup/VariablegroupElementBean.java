package square2.modules.model.vargroup;

import java.io.Serializable;

import org.jooq.JSONB;

/**
 * 
 * @author henkej
 *
 */
public class VariablegroupElementBean implements Serializable, Comparable<VariablegroupElementBean> {
	private static final long serialVersionUID = 1L;
	
	private final Integer pk;
	private final Integer fkElement;
	private final String name;
	private final JSONB translation;
	private Integer varorder;
	private Boolean dirty;

	/**
	 * create new variable group element bean
	 * 
	 * @param pk
	 *          the id of the variable group
	 * @param fkElement
	 *          the id of the element
	 * @param name
	 *          then name
	 * @param translation
	 *          the translation
	 */
	public VariablegroupElementBean(Integer pk, Integer fkElement, String name, JSONB translation) {
		super();
		this.pk = pk;
		this.fkElement = fkElement;
		this.name = name;
		this.translation = translation;
		this.dirty = false;
	}
	
	@Override
	public int compareTo(VariablegroupElementBean o) {
		return varorder == null || o == null || o.getVarorder() == null ? 0 : varorder.compareTo(o.getVarorder());
	}

	/**
	 * @return the varorder
	 */
	public Integer getVarorder() {
		return varorder;
	}

	/**
	 * @param varorder
	 *          the varorder to set
	 */
	public void setVarorder(Integer varorder) {
		this.varorder = varorder;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @return the fkElement
	 */
	public Integer getFkElement() {
		return fkElement;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the langvalue
	 */
	public JSONB getTranslation() {
		return translation;
	}

	/**
	 * @return the dirty
	 */
	public Boolean getDirty() {
		return dirty;
	}

	/**
	 * @param dirty the dirty to set
	 */
	public void setDirty(Boolean dirty) {
		this.dirty = dirty;
	}
}
