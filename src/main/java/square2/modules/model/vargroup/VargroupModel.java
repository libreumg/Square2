package square2.modules.model.vargroup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;

import square2.db.cache.vargroup.VargroupElementCache;
import square2.db.control.VargroupGateway;
import square2.help.SquareFacesContext;
import square2.modules.control.help.ValidationException;
import square2.modules.model.AclBean;
import square2.modules.model.GroupPrivilegeBean;
import square2.modules.model.ModelException;
import square2.modules.model.SquareModel;
import square2.modules.model.UserApplRightBean;
import square2.modules.model.admin.UsergroupBean;
import square2.modules.model.study.ListElement;
import square2.modules.model.study.VariableBean;
import square2.modules.model.study.VariablegroupBean;
import square2.r.control.SmiGateway;

/**
 * 
 * @author henkej
 *
 */
@Named
@SessionScoped
public class VargroupModel extends SquareModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LogManager.getLogger(VargroupModel.class);

	private Integer newGroup;
	private Integer newUsnr;
	private AclBean newAcl;
	private GroupPrivilegeBean activePrivilege;
	private List<GroupPrivilegeBean> groupPrivileges;
	private List<UsergroupBean> allGroups;

	private List<UserApplRightBean> users;
	private UserApplRightBean newUser;
	private Integer privilegeId;
	private Integer vargroupId;
	private VariableBean variable;
	private List<VariablegroupBean> dataModel;
	private VariablegroupBean variablegroupBean;
	private Integer hierarchyPosition;
	private List<ListElement> breadcrumbs;
	private List<VargroupListElement> children;
	private List<ListElement> variables;
	private VargroupElementCache cache;
	private Date cacheTimestamp;
	private List<VariablegroupElementBean> currentList;

	/**
	 * check if the list is not empty
	 * 
	 * @return true or false
	 */
	public boolean getListIsNotEmpty() {
		return dataModel != null && dataModel.size() > 0;
	}

	@Override
	public void setAllSquareUsers(SquareFacesContext facesContext, Object... params)
			throws ModelException, DataAccessException {
		if (params == null || params.length < 1 || (!(params[0] instanceof Integer))) {
			LOGGER.error("VargroupModel.setAllSquareUsers called with empty params");
			throw new ModelException("params is null, empty or not of class Integer, but should contain the privilegeId");
		}
		Integer privilegeId = (Integer) params[0];
		super.resetAllSquareusers(new VargroupGateway(facesContext).getAllSquareUsersWithoutUserPrivilege(privilegeId));
	}

	/**
	 * reset welcome site
	 * 
	 * @param includeDependings flag to determine if depending objects should be
	 * added
	 * @param facesContext of this current function call
	 * @return true or false
	 */
	public boolean resetWelcome(boolean includeDependings, SquareFacesContext facesContext) {
		this.variablegroupBean = new VariablegroupBean();
		try {
			dataModel = new VargroupGateway(facesContext).getVariablegrouplist(facesContext.getUsnr(), includeDependings);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			dataModel = new ArrayList<>();
			return false;
		}
	}

	/**
	 * prepare the edit user panel
	 * 
	 * @param vargroupId the id of the variable group
	 * @param facesContext of this current function call
	 * @return true or false
	 */
	public boolean resetEditUser(Integer vargroupId, SquareFacesContext facesContext) {
		this.vargroupId = vargroupId;
		newUser = new UserApplRightBean();
		newAcl = new AclBean().setString("rwx");
		newGroup = Integer.valueOf(0);
		try {
			VargroupGateway gw = new VargroupGateway(facesContext);
			privilegeId = gw.getPrivilegeId(vargroupId);
			users = gw.getUserPrivileges(privilegeId);
			groupPrivileges = gw.getAllGroupPrivileges(variablegroupBean.getFkRight());
			allGroups = gw.getAllGroupsExceptOf(groupPrivileges);
			setAllSquareUsers(facesContext, privilegeId);
			return true;
		} catch (DataAccessException | ModelException e) {
			users = new ArrayList<>();
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * reset variable group page content
	 * 
	 * @param id the id of the variable group
	 * @param facesContext of this current function call
	 * @return true or false
	 * @throws DataAccessException if anything went wrong on database side
	 */
	public boolean resetVariablegroup(Integer id, SquareFacesContext facesContext) {
		try {
			variablegroupBean = new VargroupGateway(facesContext).getVariablegroup(id, false);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * reset variable group content page content
	 * 
	 * @param id id of the variable group
	 * @param hierarchyPosition position in the hierarchy to look for children and
	 * parents
	 * @param possibleDirtyVariables list of variables that might be added or
	 * removed in the cache, but not in the database yet
	 * @param refreshCache flag to determine if the cache must be reloaded
	 * @param facesContext context of this function call
	 * @return true if all database operations are done, false otherwise
	 */
	public boolean resetVariablegroupContent(Integer id, Integer hierarchyPosition,
			List<ListElement> possibleDirtyVariables, Boolean refreshCache, SquareFacesContext facesContext) {
		return resetVariablegroupContent(new VargroupGateway(facesContext), id, hierarchyPosition, possibleDirtyVariables,
				refreshCache, facesContext);
	}

	/**
	 * reset variable group content page content
	 * 
	 * @param gw the variable group gateway
	 * @param id id of the variable group
	 * @param hierarchyPosition position in the hierarchy to look for children and
	 * parents
	 * @param possibleDirtyVariables list of variables that might be added or
	 * removed in the cache, but not in the database yet
	 * @param refreshCache flag to determine if the cache must be reloaded
	 * @param facesContext context of this function call
	 * @return true if all database operations are done, false otherwise
	 */
	public boolean resetVariablegroupContent(VargroupGateway gw, Integer id, Integer hierarchyPosition,
			List<ListElement> possibleDirtyVariables, Boolean refreshCache, SquareFacesContext facesContext) {
		this.hierarchyPosition = hierarchyPosition;
		try {
			variablegroupBean = gw.getVariablegroup(id, false);
			currentList = gw.getVariablegroupElements(id);
			boolean doRefresh = cacheTimestamp == null ? true : gw.getRefreshNeeded(cacheTimestamp).after(cacheTimestamp);
			if (refreshCache) {
				cache = null;
			}
			if (cache == null || doRefresh) {
				cacheTimestamp = new Date();
				cache = new VargroupElementCache(gw.getAllElementsOf(facesContext.getUsnr(), id));
			}
			refreshCacheByPossiblyDirtyVariables(possibleDirtyVariables);
			breadcrumbs = cache.getAncestors(hierarchyPosition);
			children = cache.getNonVariableChildren(hierarchyPosition);
			variables = cache.getVariableChildren(hierarchyPosition);
			children.sort((o1, o2) -> o1 == null ? 0 : o1.compareByNameTo(o2));
			variables.sort((o1, o2) -> o1 == null ? 0 : o1.compareByNameTo(o2));
			currentList.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * change the list content
	 * 
	 * @param gw the variable group gateway
	 * @return number of affected database rows
	 * @throws DataAccessException if anything went wrong on database side
	 */
	public Integer changeListContent(VargroupGateway gw) throws DataAccessException {
		return gw.changeVariablegroupContent(cache.getAllDirtyVariables(), variablegroupBean.getVariablegroupId());
	}

	/**
	 * update the definition of a variable group
	 * 
	 * @param facesContext of this current function call
	 * @return true or false
	 */
	public boolean updateDefinition(SquareFacesContext facesContext) {
		try {
			for (VariablegroupBean bean : dataModel) {
				if (variablegroupBean.getName().equals(bean.getName())
						&& !variablegroupBean.getVariablegroupId().equals(bean.getVariablegroupId())) {
					throw new ValidationException(facesContext.translate("validation.vargroup.name.notunique"));
				}
			}
			new VargroupGateway(facesContext).updateDefinition(variablegroupBean);
			return true;
		} catch (DataAccessException | ValidationException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * add a variable group
	 * 
	 * @param facesContext of this current function call
	 * @return true or false
	 */
	public boolean addVariablegroup(SquareFacesContext facesContext) {
		try {
			for (VariablegroupBean bean : dataModel) {
				if (variablegroupBean.getName().equals(bean.getName())) {
					throw new ValidationException(facesContext.translate("validation.vargroup.name.notunique"));
				}
			}
			Integer variablegroupId = new VargroupGateway(facesContext).addVariablegroup(variablegroupBean,
					facesContext.getUsnr());
			variablegroupBean.setVariablegroupId(variablegroupId);
			return true;
		} catch (DataAccessException | ValidationException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * delete the variable group
	 * 
	 * @param id the id of the variable group
	 * @param rightId the id of the privileges
	 * @param facesContext of this current function call
	 * @return true or false
	 */
	public boolean deleteVariablegroup(Integer id, Integer rightId, SquareFacesContext facesContext) {
		try {
			new VargroupGateway(facesContext).deleteVariablegroup(id, rightId);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			return false;
		}
	}

	/**
	 * delete the privileges
	 * 
	 * @param facesContext of this current function call
	 * @return true or false
	 */
	public boolean deletePrivilege(SquareFacesContext facesContext) {
		try {
			GroupPrivilegeBean bean = activePrivilege;
			Set<Integer> privileges = new HashSet<>();
			privileges.add(bean.getFkPrivilege());
			Integer affected = new VargroupGateway(facesContext).removeUserGroupPrivilege(bean.getFkUsnr(), bean.getFkGroup(),
					privileges);
			facesContext.notifyInformation("info.affected.remove", affected);
			return true;
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * set user privilege due to definition; read and write is atomic, and read and
	 * execute also
	 * 
	 * @param facesContext of this current function call
	 * @return true or false
	 */
	public boolean addUserGroupPrivilege(SquareFacesContext facesContext) {
		// atomic read and write as read and execute
		newAcl.setRead(newAcl.getWrite() || newAcl.getExecute());
		try {
			List<Integer> privileges = new ArrayList<>();
			privileges.add(variablegroupBean.getFkRight());
			Integer affected = new VargroupGateway(facesContext).addUserGroupPrivilege(newUsnr, newGroup, privileges, newAcl);
			facesContext.notifyInformation("info.affected.add", affected);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * edit the privileges
	 * 
	 * @param facesContext of this current function call
	 * @return true or false
	 */
	public boolean editPrivilege(SquareFacesContext facesContext) {
		try {
			Integer affected = new VargroupGateway(facesContext).upsertUserGroupPrivilege(activePrivilege);
			facesContext.notifyInformation("info.affected.add", affected);
			return true;
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * add the element to the group
	 * 
	 * @param elementId the id of the element
	 * @return true
	 */
	public boolean addToSelection(Integer elementId) {
		cache.addToSelection(elementId);
		return true;
	}

	/**
	 * remove the element from the group
	 * 
	 * @param elementId the id of the element
	 * @return true
	 */
	public boolean removeFromSelection(Integer elementId) {
		cache.removeFromSelection(elementId);
		return true;
	}

	/**
	 * @return true
	 */
	public boolean toAddVargroup() {
		return true;
	}

	/**
	 * download the variable attributes of the variables in this variable group
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean downloadVariableAttributes(SquareFacesContext facesContext) {
		try (SmiGateway gw = new SmiGateway(facesContext)) {
			byte[] theOdsFile = gw.getVariableAttributesOfVariablegroup(variablegroupBean.getVariablegroupId());
			return generateDownloadStreamFromByteArray(facesContext, "variable_attributes.ods",
					"application/vnd.oasis.opendocument.spreadsheet", theOdsFile);
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * change the list content
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean doChangeListContent(SquareFacesContext facesContext) {
		refreshCacheByPossiblyDirtyVariables(variables);
		try {
			VargroupGateway gw = new VargroupGateway(facesContext);
			Integer numberOfDatabaseLines = changeListContent(gw);
			// this way, a reload of the cache is omitted if no change has happened
			if (numberOfDatabaseLines > 0) {
				resetVariablegroupContent(gw, variablegroupBean.getVariablegroupId(), hierarchyPosition, null, true,
						facesContext);
			}
			facesContext.notifyInformation("information.vargroup.change.content.successful", numberOfDatabaseLines);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * select all current
	 * 
	 * @param chosen flag to filter selection
	 * @return true
	 */
	public boolean doSelectAllCurrent(Boolean chosen) {
		for (ListElement element : variables) {
			element.setChosen(chosen);
		}
		return true;
	}

	/**
	 * load all information for the info panel
	 * 
	 * @param vargroupId the id of the variable group
	 * @param facesContext the context of this function call
	 * @return true if successful, false otherwise
	 */
	public boolean resetInfo(Integer vargroupId, SquareFacesContext facesContext) {
		try {
			variablegroupBean = new VargroupGateway(facesContext).getVariablegroup(vargroupId, true);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * set the current list of the variable group
	 * 
	 * @param facesContext the context of this function call
	 * @param variablegroupId the id of the variable group
	 */
	public void setCurrentList(SquareFacesContext facesContext, Integer variablegroupId) {
		try {
			currentList = new VargroupGateway(facesContext).getVariablegroupElements(variablegroupId);
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			currentList = new ArrayList<>();
		}
	}

	/**
	 * update the current list
	 * 
	 * @param facesContext the context of this function call
	 */
	public void updateCurrentList(SquareFacesContext facesContext) {
		try {
			Integer affected = new VargroupGateway(facesContext).updateVarorder(currentList);
			facesContext.notifyAffected(affected); // TODO: not yet displayed as this is an ajax response
		} catch (DataAccessException e) {
			facesContext.notifyException(e); // TODO: not yet displayed as this is an ajax response
			LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * reorder the variable group
	 * 
	 * @param facesContext the context of this function call
	 */
	public void doAutoOrder(SquareFacesContext facesContext) {
		try {
			new VargroupGateway(facesContext).setOrderOfStudyFormVariable(currentList,
					variablegroupBean.getVariablegroupId());
		} catch (DataAccessException e) {
			facesContext.notifyException(e); // TODO: not yet displayed as this is an ajax response
			LOGGER.error(e.getMessage(), e);
		}
		currentList.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
		Integer counter = 10;
		for (VariablegroupElementBean bean : currentList) {
			bean.setVarorder(counter);
			bean.setDirty(true);
			counter += 10;
		}
	}

	/**
	 * @return true if currentList contains at least one dirty element
	 */
	public boolean getHasDirty() {
		if (currentList == null) {
			return false;
		} else {
			boolean result = false;
			for (VariablegroupElementBean bean : currentList) {
				result = result || bean.getDirty();
			}
			return result;
		}
	}

	/**
	 * set all child variables to chosen
	 * 
	 * @param elementId the id of the element
	 * @param chosen the chosen flag
	 * @return true
	 */
	public boolean setChildrenVariables(Integer elementId, Boolean chosen) {
		cache.setChildrenVariables(elementId, chosen);
		return true;
	}

	/**
	 * refresh cache
	 * 
	 * @param possibleDirtyVariables possible dirty variables
	 */
	public void refreshCacheByPossiblyDirtyVariables(List<ListElement> possibleDirtyVariables) {
		cache.updatePossiblyDirtyVariables(possibleDirtyVariables);
	}

	/**
	 * @return all dirty variables
	 */
	public List<VargroupElementBean> getAllDirtyVariables() {
		return cache.getAllDirtyVariables();
	}

	/**
	 * @return the number of dirty variables
	 */
	public Integer getAmountDirtyVariables() {
		return cache == null ? 0 : cache.getAllDirtyVariables().size();
	}

	/**
	 * @return true or false
	 */
	public Boolean getHasDirtyVariables() {
		return getAmountDirtyVariables() > 0;
	}

	/**
	 * @return the users
	 */
	public List<UserApplRightBean> getUsers() {
		return users;
	}

	/**
	 * @return the new user
	 */
	public UserApplRightBean getNewUser() {
		return newUser;
	}

	/**
	 * @return the privilege id
	 */
	public Integer getPrivilegeId() {
		return privilegeId;
	}

	/**
	 * @return the variable bean
	 */
	public VariableBean getVariable() {
		return variable;
	}

	/**
	 * @return the variable group bean
	 */
	public VariablegroupBean getVariablegroupBean() {
		return variablegroupBean;
	}

	/**
	 * @return the hierarchy position
	 */
	public Integer getHierarchyPosition() {
		return hierarchyPosition;
	}

	/**
	 * @return the bread crumbs
	 */
	public List<ListElement> getBreadcrumbs() {
		return breadcrumbs;
	}

	/**
	 * @return the children
	 */
	public List<VargroupListElement> getChildren() {
		return children;
	}

	/**
	 * @return the variables
	 */
	public List<ListElement> getVariables() {
		return variables;
	}

	/**
	 * @return the id of the variable group
	 */
	public Integer getVargroupId() {
		return vargroupId;
	}

	/**
	 * @return the data model
	 */
	public List<VariablegroupBean> getDataModel() {
		return dataModel;
	}

	/**
	 * @return the new acl
	 */
	public AclBean getNewAcl() {
		return newAcl;
	}

	/**
	 * @param activePrivilege the active privilege
	 */
	public void setActivePrivilege(GroupPrivilegeBean activePrivilege) {
		this.activePrivilege = activePrivilege;
	}

	/**
	 * @return the group privileges
	 */
	public List<GroupPrivilegeBean> getGroupPrivileges() {
		return groupPrivileges;
	}

	/**
	 * @return all groups
	 */
	public List<UsergroupBean> getAllGroups() {
		return allGroups;
	}

	/**
	 * @return the new group
	 */
	public Integer getNewGroup() {
		return newGroup;
	}

	/**
	 * @param newGroup the new group
	 */
	public void setNewGroup(Integer newGroup) {
		this.newGroup = newGroup;
	}

	/**
	 * @return the new user id
	 */
	public Integer getNewUsnr() {
		return newUsnr;
	}

	/**
	 * @param newUsnr the new user id
	 */
	public void setNewUsnr(Integer newUsnr) {
		this.newUsnr = newUsnr;
	}

	/**
	 * @return the current list
	 */
	public List<VariablegroupElementBean> getCurrentList() {
		return currentList;
	}
}