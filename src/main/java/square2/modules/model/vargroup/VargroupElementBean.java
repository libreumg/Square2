package square2.modules.model.vargroup;

import java.io.Serializable;

import org.jooq.JSONB;

/**
 * 
 * @author henkej
 *
 */
public class VargroupElementBean implements Serializable, Comparable<VargroupElementBean> {
	private static final long serialVersionUID = 1L;

	private final Integer keyElement;
	private final JSONB translation;
	private final Integer keyParentElement;
	private String name;
	private Boolean isVariable;
	private Boolean chosen;
	private Boolean dirty;

	/**
	 * create new variable group element bean
	 * 
	 * @param keyElement
	 *          the id of the element
	 * @param translation
	 *          the current translation
	 * @param keyParentElement
	 *          the id of the parent element
	 * @param chosen
	 *          a flag to determine if this one is chosen
	 */
	public VargroupElementBean(Integer keyElement, JSONB translation, Integer keyParentElement, Boolean chosen) {
		super();
		this.keyElement = keyElement;
		this.translation = translation;
		this.keyParentElement = keyParentElement;
		this.chosen = chosen;
		this.dirty = false;
	}

	@Override
	public int compareTo(VargroupElementBean o) {
		return keyElement == null || o == null || o.getKeyElement() == null ? 0 : keyElement.compareTo(o.getKeyElement());
	}

	public String toString() {
		return name;
	}

	/**
	 * @return the dirty
	 */
	public Boolean isDirty() {
		return dirty;
	}

	/**
	 * @return the dirty
	 */
	public Boolean getDirty() {
		return dirty;
	}

	/**
	 * @return the key element
	 */
	public Integer getKeyElement() {
		return keyElement;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return is variable
	 */
	public Boolean getIsVariable() {
		return isVariable;
	}

	/**
	 * @param isVariable
	 *          is variable
	 */
	public void setIsVariable(Boolean isVariable) {
		this.isVariable = isVariable;
	}

	/**
	 * @return the chosen
	 */
	public Boolean getChosen() {
		return chosen;
	}

	/**
	 * @param chosen
	 *          the chosen
	 */
	public void setChosen(Boolean chosen) {
		if (!this.chosen.equals(chosen)) {
			this.dirty = true;
		}
		this.chosen = chosen;
	}

	/**
	 * @return the parent element id
	 */
	public Integer getKeyParentElement() {
		return keyParentElement;
	}

	/**
	 * @return the translation
	 */
	public JSONB getTranslation() {
		return translation;
	}
}
