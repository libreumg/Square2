package square2.modules.model;

import java.io.Serializable;
import java.util.Date;

import de.ship.dbppsquare.square.enums.EnumAccesslevel;
import ship.jsf.components.dataTable.FilterableBean;

/**
 * 
 * @author henkej
 * 
 */
public class UserApplRightBean extends FilterableBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer rightId;
	private String rightName;
	private String forename;
	private String surname;
	private String username;
	private Integer usnr;
	private Integer group;
	private String groupname;
	private AclBean acl;
	private Date lastchange;

	/**
	 * create new empty user appl right bean
	 */
	public UserApplRightBean() {
		super();
		this.acl = new AclBean();
	}

	/**
	 * create new filled user appl right bean
	 * 
	 * @param bean
	 *          the access level bean
	 * @param rightId
	 *          the privilege id
	 * @param group
	 *          the group id
	 * @param rightName
	 *          the right name
	 * @param username
	 *          the user name
	 * @param groupname
	 *          the group name
	 */
	public UserApplRightBean(UserAccessLevelBean bean, Integer rightId, Integer group, String rightName, String username,
			String groupname) {
		super();
		this.rightId = rightId;
		this.rightName = rightName;
		this.forename = bean.getForename();
		this.surname = bean.getSurname();
		this.username = username;
		this.usnr = bean.getUsnr();
		this.group = group;
		this.groupname = groupname;
		this.acl = bean.getAcl();
	}

	/**
	 * @return true if this is a group privilege
	 */
	public Boolean getIsGroupPrivilege() {
		return group != null;
	}

	/**
	 * @return true if this is a user privilege
	 */
	public Boolean getIsUserPrivilege() {
		return usnr != null;
	}

	/**
	 * @return full name of user
	 */
	public String getFullname() {
		return new StringBuilder().append(forename).append(" ").append(surname).toString();
	}

	/**
	 * set accesslevel bean due to enum
	 * 
	 * @param enumAccesslevel
	 *          the acl
	 */
	public void setAcl(String enumAccesslevel) {
		acl.setString(enumAccesslevel);
	}

	/**
	 * set accesslevel bean due to enum
	 * 
	 * @param enumAcl
	 *          the acl
	 */
	public void setAcl(EnumAccesslevel enumAcl) {
		acl.setAcl(enumAcl);
	}

	/**
	 * @return the rightId
	 */
	public Integer getRightId() {
		return rightId;
	}

	/**
	 * @param rightId
	 *          the rightId to set
	 */
	public void setRightId(Integer rightId) {
		this.rightId = rightId;
	}

	/**
	 * @return the rightName
	 */
	public String getRightName() {
		return rightName;
	}

	/**
	 * @param rightName
	 *          the rightName to set
	 */
	public void setRightName(String rightName) {
		this.rightName = rightName;
	}

	/**
	 * @return the forename
	 */
	public String getForename() {
		return forename;
	}

	/**
	 * @param forename
	 *          the forename to set
	 */
	public void setForename(String forename) {
		this.forename = forename;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname
	 *          the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *          the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the usnr
	 */
	public Integer getUsnr() {
		return usnr;
	}

	/**
	 * @param usnr
	 *          the usnr to set
	 */
	public void setUsnr(Integer usnr) {
		this.usnr = usnr;
	}

	/**
	 * @return the group
	 */
	public Integer getGroup() {
		return group;
	}

	/**
	 * @param group
	 *          the group to set
	 */
	public void setGroup(Integer group) {
		this.group = group;
	}

	/**
	 * @return the groupname
	 */
	public String getGroupname() {
		return groupname;
	}

	/**
	 * @param groupname
	 *          the groupname to set
	 */
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	/**
	 * @return the acl
	 */
	public AclBean getAcl() {
		return acl;
	}

	/**
	 * @param acl
	 *          the acl to set
	 */
	public void setAcl(AclBean acl) {
		this.acl = acl;
	}

	/**
	 * @return the lastchange
	 */
	public Date getLastchange() {
		return lastchange;
	}

	/**
	 * @param lastchange
	 *          the lastchange to set
	 */
	public void setLastchange(Date lastchange) {
		this.lastchange = lastchange;
	}
}
