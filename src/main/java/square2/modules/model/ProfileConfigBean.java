package square2.modules.model;

import java.io.Serializable;

import org.jooq.JSONB;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * 
 * @author henkej
 *
 */
public class ProfileConfigBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String key;
	private Object value;
	private final JSONB translation;
	private final Boolean adminOnly;
	private final Boolean isText;
	private final Boolean isColor;
	private final Boolean isNumber;

	/**
	 * create new profile configuration bean
	 * 
	 * @param key the key
	 * @param value the value
	 * @param translation the translation
	 * @param adminOnly if true, allow changes for admin only
	 * @param isText the isText
	 * @param isColor the isColor
	 * @param isNumber the isNumber
	 */
	public ProfileConfigBean(String key, Object value, JSONB translation, Boolean adminOnly, Boolean isText,
			Boolean isColor, Boolean isNumber) {
		super();
		this.key = key;
		this.value = value;
		this.translation = translation;
		this.isText = isText;
		this.isColor = isColor;
		this.isNumber = isNumber;
		this.adminOnly = adminOnly;
	}

	/**
	 * @param locale the locale
	 * @return the translation if found
	 */
	public String getTranslation(String locale) {
		if (translation == null || translation.data().isBlank()) {
			return key;
		} else {
			JsonObject map = new Gson().fromJson(translation.data(), JsonObject.class);
			return map.has(locale) ? map.get(locale).getAsString() : key;
		}
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the isText
	 */
	public Boolean getIsText() {
		return isText;
	}

	/**
	 * @return the isColor
	 */
	public Boolean getIsColor() {
		return isColor;
	}

	/**
	 * @return the isNumber
	 */
	public Boolean getIsNumber() {
		return isNumber;
	}

	/**
	 * @return the adminOnly
	 */
	public Boolean getAdminOnly() {
		return adminOnly;
	}
}
