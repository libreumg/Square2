package square2.modules.model;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import square2.help.SquareFacesContext;

/**
 * 
 * @author henkej
 *
 */
@Named
@SessionScoped
public class SessionVersionBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private String rVersion;
	private String squareControl2Version;
	private String squaremetadatainterfaceVersion;
	private String requiredSquareControl2Version;
	private String requiredSquaremetadatainterfaceVersion;
	private String requiredDbVersion;
	private String requiredSquarereportrendererVersion;
	private String requiredFunctionRegistratorVersion;
	private String squarereportrendererVersion;
	private String functionRegistratorVersion;
	private String dbVersion;
	private String javaVersion;

	/**
	 * check if requiredVersion is lower or equal to givenVersion
	 * 
	 * @param identifier the name of the package for warning messages
	 * @param requiredVersion the expected version
	 * @param givenVersion the found version
	 * @return the error message if requirement is not fulfilled
	 */
	private String requireOrWarn(SquareFacesContext facesContext, String identifier, String requiredVersion,
			String givenVersion, boolean forceEqual) {
		if (forceEqual) {
			if (requiredVersion == null || givenVersion == null) {
				return facesContext.translate("warn.versionwrong", identifier, givenVersion, requiredVersion);
			} else if (requiredVersion.equals(givenVersion)) {
				return null;
			} else {
				return facesContext.translate("warn.versionwrong", identifier, givenVersion, requiredVersion);
			}
		} else {
			LevelNumberBean requiredBean = new LevelNumberBean(requiredVersion);
			LevelNumberBean givenBean = new LevelNumberBean(givenVersion);
			if (!givenBean.greaterOrEqualThan(requiredBean)) {
				// too late for a faces message, maybe too late for translation also? This is
				// called in a getter from the jsf page...
				return facesContext.translate("warn.versiontoolow", identifier, givenVersion, requiredVersion);
			} else {
				return null;
			}
		}
	}

	/**
	 * @return the rVersion
	 */
	public String getrVersion() {
		return rVersion;
	}

	/**
	 * @param rVersion the rVersion to set
	 */
	public void setrVersion(String rVersion) {
		this.rVersion = rVersion;
	}

	/**
	 * @return the dbVersion
	 */
	public String getDbVersion() {
		return dbVersion;
	}

	/**
	 * @param facesContext the faces context
	 * @return the dbVersion
	 */
	public String getDbVersion(SquareFacesContext facesContext) {
		String s = requireOrWarn(facesContext, "db", requiredDbVersion, dbVersion, true);
		return s == null ? dbVersion : s;
	}

	/**
	 * @param dbVersion the dbVersion to set
	 * @param required the required version
	 */
	public void setDbVersion(String dbVersion, String required) {
		this.dbVersion = dbVersion;
		this.requiredDbVersion = required;
	}

	/**
	 * @param dbVersion the dbVersion to set
	 */
	public void setDbVersion(String dbVersion) {
		this.dbVersion = dbVersion;
	}

	/**
	 * @param facesContext the current faces context
	 * @return the squareControl2Version
	 */
	public String getSquareControl2Version(SquareFacesContext facesContext) {
		String s = requireOrWarn(facesContext, "SquareControl2", requiredSquareControl2Version, squareControl2Version,
				false);
		return s == null ? squareControl2Version
				: new StringBuilder(squareControl2Version).append(", ").append(s).toString();
	}

	/**
	 * @param squareControl2Version the squareControl2Version to set
	 */
	public void setSquareControl2Version(String squareControl2Version) {
		this.squareControl2Version = squareControl2Version;
	}

	/**
	 * @param squareControl2Version the version of SquareControl2
	 * @param required the expected version
	 */
	public void setSquareControl2Version(String squareControl2Version, String required) {
		this.squareControl2Version = squareControl2Version;
		this.requiredSquareControl2Version = required;
	}

	/**
	 * @param facesContext the current faces context
	 * @return the version of squaremetadatainterface
	 */
	public String getSquaremetadatainterfaceVersion(SquareFacesContext facesContext) {
		String s = requireOrWarn(facesContext, "squaremetadatainterface", requiredSquaremetadatainterfaceVersion,
				squaremetadatainterfaceVersion, false);
		return s == null ? squaremetadatainterfaceVersion
				: new StringBuilder(squaremetadatainterfaceVersion).append(", ").append(s).toString();
	}

	/**
	 * @param squaremetadatainterfaceVersion the version of squaremetadatainterface
	 * @param required the expected version
	 */
	public void setSquaremetadatainterfaceVersion(String squaremetadatainterfaceVersion, String required) {
		this.squaremetadatainterfaceVersion = squaremetadatainterfaceVersion;
		this.requiredSquaremetadatainterfaceVersion = required;
	}

	/**
	 * @param squaremetadatainterfaceVersion the version of squaremetadatainterface
	 */
	public void setSquaremetadatainterfaceVersion(String squaremetadatainterfaceVersion) {
		this.squaremetadatainterfaceVersion = squaremetadatainterfaceVersion;
	}

	/**
	 * @param functionRegistratorVersion the version of functionRegistrator
	 * @param required the expected version
	 */
	public void setFunctionRegistratorVersion(String functionRegistratorVersion, String required) {
		this.functionRegistratorVersion = functionRegistratorVersion;
		this.requiredFunctionRegistratorVersion = required;
	}

	/**
	 * @param functionRegistratorVersion the version of functionRegistrator
	 */
	public void setFunctionRegistratorVersion(String functionRegistratorVersion) {
		this.functionRegistratorVersion = functionRegistratorVersion;
	}

	/**
	 * @return the javaVersion
	 */
	public String getJavaVersion() {
		return javaVersion;
	}

	/**
	 * @param javaVersion the javaVersion to set
	 */
	public void setJavaVersion(String javaVersion) {
		this.javaVersion = javaVersion;
	}

	/**
	 * @param facesContext the current faces context
	 * @return the version of squarereportrendererVersion
	 */
	public String getSquarereportrendererVersion(SquareFacesContext facesContext) {
		String s = requireOrWarn(facesContext, "squarereportrenderer", requiredSquarereportrendererVersion,
				squarereportrendererVersion, false);
		return s == null ? squarereportrendererVersion
				: new StringBuilder(squarereportrendererVersion).append(", ").append(s).toString();
	}

	/**
	 * @param squarereportrendererVersion the squarereportrendererVersion to set
	 * @param required the expected version
	 */
	public void setSquarereportrendererVersion(String squarereportrendererVersion, String required) {
		this.squarereportrendererVersion = squarereportrendererVersion;
		this.requiredSquarereportrendererVersion = required;
	}

	/**
	 * @param squarereportrendererVersion the squarereportrendererVersion to set
	 */
	public void setSquarereportrendererVersion(String squarereportrendererVersion) {
		this.squarereportrendererVersion = squarereportrendererVersion;
	}

	/**
	 * @param facesContext the current faces context
	 * @return the version of functionRegistratorVersion
	 */
	public String getFunctionRegistratorVersion(SquareFacesContext facesContext) {
		String s = requireOrWarn(facesContext, "functionregistrator", requiredFunctionRegistratorVersion,
				functionRegistratorVersion, false);
		return s == null ? functionRegistratorVersion
				: new StringBuilder(functionRegistratorVersion).append(", ").append(s).toString();
	}

	/**
	 * @return the requiredFunctionRegistratorVersion
	 */
	public String getRequiredFunctionRegistratorVersion() {
		return requiredFunctionRegistratorVersion;
	}
}
