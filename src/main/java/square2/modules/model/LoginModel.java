package square2.modules.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.jooq.exception.DataAccessException;

import square2.db.control.ConfigGateway;
import square2.db.control.LoginGateway;
import square2.help.SquareFacesContext;
import square2.help.ThemeBean;
import square2.modules.control.help.Session;

/**
 * 
 * @author henkej
 *
 */
@Named
@RequestScoped
public class LoginModel extends SquareModel {
	private static final Logger LOGGER = LogManager.getLogger("Square2");

	private String username;
	private String password;
	private String passwordAgain;
	private String configValue;
	private String newParam;
	private List<MaintenanceBean> maintenances;

	@Override
	public void setAllSquareUsers(SquareFacesContext facesContext, Object... params) throws ModelException {
	}

	private boolean loadMaintenance(SquareFacesContext facesContext) {
		try {
			maintenances = new LoginGateway(facesContext).getMaintenances();
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * do the login
	 * 
	 * @param applicationBean the application bean
	 * @param facesContext the faces context
	 * @return true or false
	 */
	public boolean doLogin(SquareFacesContext facesContext, ApplicationBean applicationBean) {
		try {
			facesContext.getExternalContext().getSessionMap().put(Session.PROFILE.get(),
					getProfileBean(applicationBean, facesContext));
			LOGGER.debug("login of {} successful", facesContext.getProfile().getUsername());
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			// do not log here to have no password in log file
			return false;
		}
	}

	/**
	 * check, if login is valid. If so, return profile bean; otherwise throw an
	 * exception
	 * 
	 * @param applicationBean the application bean
	 * @param facesContext the context of this function call
	 * @return the profile bean
	 * @throws DataAccessException if anything went wrong on database side
	 */
	public ProfileBean getProfileBean(ApplicationBean applicationBean, SquareFacesContext facesContext)
			throws DataAccessException {
		ProfileBean bean = new LoginGateway(facesContext).getProfile(username, password);
		Calendar c = new GregorianCalendar();
		c.setTime(bean.getExpireOn());
		Calendar n = new GregorianCalendar();
		n.add(Calendar.DAY_OF_MONTH, 7);
		if (c.before(n)) {
			facesContext.notifyWarning("warn.login.expires",
					new SimpleDateFormat("dd.MM.yyyy, HH:mm").format(bean.getExpireOn()));
		} else {
			facesContext.notifyInformation("warn.login.expires",
					new SimpleDateFormat("dd.MM.yyyy, HH:mm").format(bean.getExpireOn()));
		}
		applicationBean.addProfile(bean, facesContext);
		return bean;
	}

	/**
	 * get the config of a user
	 * 
	 * @param usnr the id of the user
	 * @param facesContext the faces context
	 * @return the config params
	 * @throws DataAccessException if anything went wrong on database side
	 */
	public List<ProfileConfigBean> getConfig(Integer usnr, SquareFacesContext facesContext) throws DataAccessException {
		return new ConfigGateway(facesContext).getConfig(usnr);
	}

	/**
	 * change the password of a user
	 * 
	 * @param facesContext the faces context
	 * @return true or false
	 */
	public boolean changePassword(SquareFacesContext facesContext) {
		if (password.equals(passwordAgain)) {
			try {
				new LoginGateway(facesContext).updatePassword(facesContext.getUsnr(),
						new StrongPasswordEncryptor().encryptPassword(password));
				facesContext.notifyInformation("info.profile.passwords.changed");
				return true;
			} catch (DataAccessException | NullPointerException e) {
				facesContext.notifyException(e);
				// do not log here to have no passwords in logfile
				return false;
			}
		} else {
			facesContext.notifyError("error.profile.passwords.notequal");
			return false;
		}
	}

	/**
	 * set the config param of a user
	 * 
	 * @param key the config key
	 * @param usnr the id of the user
	 * @param value the config value
	 * @param themeBean the theme bean
	 * @param facesContext the faces context
	 * @return true or false
	 */
	public boolean setConfigParam(SquareFacesContext facesContext, String key, Integer usnr, Object value,
			ThemeBean themeBean) {
		try {
			ConfigGateway gw = new ConfigGateway(facesContext);
			gw.upsert(key, value, usnr);
			List<ProfileConfigBean> newConfig = gw.getConfig(usnr);
			ProfileBean profileBean = facesContext.getProfile();
			profileBean.getConfig().clear();
			profileBean.getConfig().addAll(newConfig);
			facesContext.getExternalContext().getSessionMap().put(Session.PROFILE.get(), profileBean);
			themeBean.resetTheme();
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * load all to change the password
	 * 
	 * @param facesContext the faces context
	 * @return true or false
	 */
	public boolean toChangePassword(SquareFacesContext facesContext) {
		try {
			facesContext.getProfile().getConfig().clear();
			facesContext.getProfile().getConfig().addAll(getConfig(facesContext.getUsnr(), facesContext));
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * logout user and terminate session
	 * 
	 * @param facesContext the faces context
	 * @param applicationBean the application bean
	 * @return true
	 */
	public boolean doLogout(SquareFacesContext facesContext, ApplicationBean applicationBean) {
		ProfileBean profileBean = facesContext.getProfile();
		LocaleBean localeBean = facesContext.getLocaleBean(); // rescue locale settings
		profileBean.logoutFromKeycloakIfConnected();
		applicationBean.removeProfile(facesContext.getUsnr());
		facesContext.getExternalContext().invalidateSession();
		facesContext.getExternalContext().getSessionMap().put("lang", localeBean);
		return true;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * the password is not returned for security reasons
	 * 
	 * @return an empty string
	 */
	public String getPassword() {
		return ""; // never give back password
	}

	/**
	 * @param password the password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * the password is never returned for security reasons
	 * 
	 * @return an empty string
	 */
	public String getPasswordAgain() {
		return ""; // never give back password
	}

	/**
	 * @param passwordAgain the password
	 */
	public void setPasswordAgain(String passwordAgain) {
		this.passwordAgain = passwordAgain;
	}

	/**
	 * @return the configuration value
	 */
	public String getConfigValue() {
		return configValue;
	}

	/**
	 * @param configValue the configuration value
	 */
	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	/**
	 * @return the new param
	 */
	public String getNewParam() {
		return newParam;
	}

	/**
	 * @param newParam the new param
	 */
	public void setNewParam(String newParam) {
		this.newParam = newParam;
	}

	/**
	 * @param facesContext the faces context
	 * @return the list of maintenance beans
	 */
	public List<MaintenanceBean> getMaintenances(SquareFacesContext facesContext) {
		if (maintenances == null) {
			boolean result = loadMaintenance(facesContext);
			if (!result) {
				maintenances = new ArrayList<>();
			}
		}
		return maintenances;
	}
}
