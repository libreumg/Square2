package square2.modules.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * @author henkej
 *
 */
public class LangKey {
	private static final Logger LOGGER = LogManager.getLogger(LangKey.class);

	/**
	 * generate a key
	 * 
	 * !!! works only if t_languagekey.name is null !!!
	 * 
	 * @param keyId
	 *          the id of the key
	 * @param keyName
	 *          the name of the key
	 * @return the generated key
	 */
	public static final String generateKey(Integer keyId, String keyName) {
		String generated = (keyName == null || (keyName.trim().length() < 1)) ? ("translation.key." + keyId) : keyName;
		LOGGER.trace("generated key from {} and {} : {}", keyId, keyName, generated);
		return generated;
	}

	/**
	 * generate a key
	 * 
	 * @param key
	 *          the key
	 * @return the generated key
	 */
	public static final String generateKey(Integer key) {
		return LangKey.generateKey(key, null);
	}

	/**
	 * generate a key
	 * 
	 * @param key
	 *          the key
	 * @return the generated key
	 */
	public static final String generateKey(String key) {
		return LangKey.generateKey(null, key);
	}
}
