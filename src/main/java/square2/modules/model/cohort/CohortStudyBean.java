package square2.modules.model.cohort;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jooq.JSONB;

import de.ship.dbppsquare.square.enums.EnumAccesslevel;

/**
 * 
 * @author henkej
 *
 */
public class CohortStudyBean implements Serializable, Comparable<CohortStudyBean> {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer parentId;
	private String name;
	private String uniqueName;
	private String parentName;
	private JSONB translation;
	private JSONB locales;
	private Date startDate;
	private Date designfeature;
	private Integer participants;
	private Integer right;
	private EnumAccesslevel acl;
	private final List<StudyAttributeBean> attributes;
	private final List<StudyAttributeBean> availableAttributes;

	/**
	 * create a new study cohort bean with an empty list of attributes
	 * 
	 * @param availableAttributes lists all available attributes
	 * @param locales the available locales for this study
	 */
	public CohortStudyBean(final List<StudyAttributeBean> availableAttributes, JSONB locales) {
		this.attributes = new ArrayList<>();
		this.availableAttributes = availableAttributes;
	}

	@Override
	public int compareTo(CohortStudyBean bean) {
		return bean == null || bean.getUniqueName() == null ? 0 : bean.getUniqueName().compareTo(uniqueName);
	}

	/**
	 * add the attribute of availableAttributes referenced by key
	 * 
	 * @param key the key
	 */
	public void addAttribute(String key) {
		for (StudyAttributeBean bean : availableAttributes) {
			if (bean.getKey().equals(key) && !attributes.contains(bean)) {
				attributes.add(bean);
			}
		}
	}

	/**
	 * @return the missing attributes
	 */
	public List<StudyAttributeBean> getMissingAttributes() {
		List<StudyAttributeBean> missingAttrs = new ArrayList<>();
		for (StudyAttributeBean bean : availableAttributes) {
			if (!attributes.contains(bean)) {
				missingAttrs.add(bean);
			}
		}
		return missingAttrs;
	}

	/**
	 * @return true if acl contains write = true, false otherwise
	 */
	public boolean getCanWrite() {
		return acl == null ? false : acl.getLiteral().contains("w");
	}

	/**
	 * @return true if acl contains execute = true, false otherwise
	 */
	public boolean getCanUse() {
		return acl == null ? false : acl.getLiteral().contains("x");
	}
	
	/**
	 * get the locales json string
	 * 
	 * @return the json string of the locales
	 */
	public String getLocalesJson() {
		return locales == null ? "{}" : locales.data();
	}
	
	/**
	 * get the json string of the translation
	 * 
 	 * @return the json string
	 */
	public String getTranslationJson() {
		return translation == null ? "{}" : translation.data();
	}
	
	/**
	 * set the json string
	 * 
	 * @param json the json string
	 */
	public void setTranslationJson(String json) {
		translation = json == null ? JSONB.valueOf("{}") : JSONB.valueOf(json);
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the parentId
	 */
	public Integer getParentId() {
		return parentId;
	}

	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the translation
	 */
	public JSONB getTranslation() {
		return translation;
	}

	/**
	 * @param translation the translation to set
	 */
	public void setTranslation(JSONB translation) {
		this.translation = translation;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the designfeature
	 */
	public Date getDesignfeature() {
		return designfeature;
	}

	/**
	 * @param designfeature the designfeature to set
	 */
	public void setDesignfeature(Date designfeature) {
		this.designfeature = designfeature;
	}

	/**
	 * @return the participants
	 */
	public Integer getParticipants() {
		return participants;
	}

	/**
	 * @param participants the participants to set
	 */
	public void setParticipants(Integer participants) {
		this.participants = participants;
	}

	/**
	 * @return the right
	 */
	public Integer getRight() {
		return right;
	}

	/**
	 * @param right the right to set
	 */
	public void setRight(Integer right) {
		this.right = right;
	}

	/**
	 * @return the acl
	 */
	public EnumAccesslevel getAcl() {
		return acl;
	}

	/**
	 * @param acl the acl to set
	 */
	public void setAcl(EnumAccesslevel acl) {
		this.acl = acl;
	}

	/**
	 * @return the unique name
	 */
	public String getUniqueName() {
		return uniqueName;
	}

	/**
	 * @param uniqueName the unique name to set
	 */
	public void setUniqueName(String uniqueName) {
		this.uniqueName = uniqueName;
	}

	/**
	 * @return the attributes
	 */
	public List<StudyAttributeBean> getAttributes() {
		return attributes;
	}

	/**
	 * @return the availableAttributes
	 */
	public List<StudyAttributeBean> getAvailableAttributes() {
		return availableAttributes;
	}

	/**
	 * @return the parentName
	 */
	public String getParentName() {
		return parentName;
	}

	/**
	 * @param parentName the parentName to set
	 */
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	/**
	 * @return the locales
	 */
	public JSONB getLocales() {
		return locales;
	}
	
	/**
	 * @param locales the locales to set
	 */
	public void setLocales(JSONB locales) {
		this.locales = locales;
	}
}
