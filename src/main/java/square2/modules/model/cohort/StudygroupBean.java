package square2.modules.model.cohort;

import org.jooq.JSONB;

/**
 * 
 * @author henkej
 *
 */
public class StudygroupBean {
	private final Integer pk;
	private String name;
	private JSONB translation;
	private JSONB locales;
	private final String studies;

	/**
	 * for jsf, a native constructor is needed on setters of beans; here, we need
	 * that only for the filter operation
	 */
	public StudygroupBean() {
		this.pk = null;
		this.studies = null;
		this.locales = JSONB.valueOf("{}");
	}

	/**
	 * create new study group bean
	 * 
	 * @param pk the id of the element
	 * @param name the name
	 * @param translation the translation
	 * @param studies the study names of this study group as a comma separated list
	 * @param locales the json locales representation, typically like {"de_DE":
	 * "Deutsch", "en": "English", ...}
	 */
	public StudygroupBean(Integer pk, String name, JSONB translation, String studies, JSONB locales) {
		super();
		this.pk = pk;
		this.name = name;
		this.translation = translation;
		this.studies = studies;
		this.locales = locales;
	}
	
	/**
	 * @return the json string of the locales
	 */
	public String getLocalesJson() {
		return locales == null ? "{}" : locales.data();
	}
	
	/**
	 * @param json the json string of the locales
	 */
	public void setLocalesJson(String json) {
		locales = JSONB.valueOf(json == null ? "{}" : json);
	}

	/**
	 * @return the json string of the translation
	 */
	public String getTranslationJson() {
		return translation == null ? "{}" : translation.data();
	}

	/**
	 * @param json the json string of the translation
	 */
	public void setTranslationJson(String json) {
		translation = JSONB.valueOf(json == null ? "{}" : json);
	}

	/**
	 * @return the id of the element
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @return the name of the element
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the studies
	 */
	public String getStudies() {
		return studies;
	}

	/**
	 * @return the translation
	 */
	public JSONB getTranslation() {
		return translation;
	}

	/**
	 * @param translation the translation to set
	 */
	public void setTranslation(JSONB translation) {
		this.translation = translation;
	}

	/**
	 * @return the locales
	 */
	public JSONB getLocales() {
		return locales;
	}

	/**
	 * @param locales the locales to set
	 */
	public void setLocales(JSONB locales) {
		this.locales = locales;
	}
}
