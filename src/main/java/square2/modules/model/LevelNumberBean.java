package square2.modules.model;

import java.io.Serializable;
import java.lang.module.ModuleDescriptor.Version;

/**
 * 
 * @author henkej
 *
 */
public class LevelNumberBean implements Serializable {
	private static final long serialVersionUID = 2L;
	private final Version version;

	/**
	 * @param version the version string
	 */
	public LevelNumberBean(String version) {
		this.version = Version.parse(version);
	}

	@Override
	public String toString() {
		return version.toString();
	}

	/**
	 * compare current level versions
	 * 
	 * @param requiredBean the expected version
	 * @return true or false
	 */
	public boolean greaterOrEqualThan(LevelNumberBean requiredBean) {
		if (requiredBean == null) {
			return true;
		}
		return version.compareTo(requiredBean.getVersion()) > -1;
	}

	/**
	 * @return the version
	 */
	public Version getVersion() {
		return version;
	}
}
