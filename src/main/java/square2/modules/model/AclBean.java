package square2.modules.model;

import java.io.Serializable;

import de.ship.dbppsquare.square.enums.EnumAccesslevel;

/**
 * 
 * @author henkej
 *
 */
public class AclBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private boolean read;
	private boolean write;
	private boolean execute;

	/**
	 * create new acl bean
	 */
	public AclBean() {
		read = false;
		write = false;
		execute = false;
	}

	/**
	 * create new acl bean
	 * 
	 * @param acl
	 *          the acl enum
	 */
	public AclBean(EnumAccesslevel acl) {
		this();
		if (acl != null) {
			read = acl.getLiteral().contains("r");
			write = acl.getLiteral().contains("w");
			execute = acl.getLiteral().contains("x");
		}
	}

	/**
	 * create new acl bean
	 * 
	 * @param s
	 *          the acl string
	 */
	public AclBean(String s) {
		this();
		setString(s);
	}

	/**
	 * create new acl bean
	 * 
	 * @param read
	 *          the read acl
	 * @param write
	 *          the write acl
	 * @param execute
	 *          the execute acl
	 */
	public AclBean(Boolean read, Boolean write, Boolean execute) {
		this();
		this.read = read == null ? false : read;
		this.write = write == null ? false : write;
		this.execute = execute == null ? false : execute;
	}

	/**
	 * @return true or false
	 */
	public boolean isNotEmpty() {
		return read || write || execute;
	}

	/**
	 * combine acl1 and acl2 to one acl
	 * 
	 * @param acl1
	 *          the first acl
	 * @param acl2
	 *          the second acl
	 */
	public void mergeString(String acl1, String acl2) {
		// setString checks for existence of r,w and x; duplicates do not matter
		setString(new StringBuilder().append(acl1).append(acl2).toString());
	}

	/**
	 * combine acl1 and acl2 to one acl
	 * 
	 * @param acl1
	 *          the first acl
	 * @param acl2
	 *          the second acl
	 */
	public void mergeAcl(EnumAccesslevel acl1, EnumAccesslevel acl2) {
		mergeString(acl1 == null ? null : acl1.getLiteral(), acl2 == null ? null : acl2.getLiteral());
	}

	/**
	 * check function to please sonarlint
	 * 
	 * @param condition
	 *          to be checked
	 * @param trueValue
	 *          the value for true
	 * @param falseValue
	 *          the value for false
	 * @return the value
	 */
	private EnumAccesslevel check(boolean condition, EnumAccesslevel trueValue, EnumAccesslevel falseValue) {
		return condition ? trueValue : falseValue;
	}

	/**
	 * @return the acl
	 */
	public EnumAccesslevel getAcl() {
		EnumAccesslevel acl = null;
		if (read) {
			if (write) {
				acl = check(execute, EnumAccesslevel.rwx, EnumAccesslevel.rw);
			} else {
				acl = check(execute, EnumAccesslevel.rx, EnumAccesslevel.r);
			}
		} else {
			if (write) {
				acl = check(execute, EnumAccesslevel.wx, EnumAccesslevel.w);
			} else {
				acl = check(execute, EnumAccesslevel.x, null);
			}
		}
		return acl;
	}

	/**
	 * @param acl
	 *          the acl
	 * @return this class
	 */
	public AclBean setAcl(EnumAccesslevel acl) {
		setString(acl == null ? "" : acl.getLiteral());
		return this;
	}

	/**
	 * @param acl
	 *          the acl
	 * @return this class
	 */
	public AclBean setAcl(AclBean acl) {
		setString(acl == null ? "" : acl.getString());
		return this;
	}

	/**
	 * @return the acl as string
	 */
	public String getString() {
		StringBuilder buf = new StringBuilder();
		buf.append(read ? "r" : "");
		buf.append(write ? "w" : "");
		buf.append(execute ? "x" : "");
		return buf.toString();
	}

	/**
	 * @param acl
	 *          the acl string
	 * @return this class
	 */
	public AclBean setString(String acl) {
		if (acl == null || acl.length() < 1) {
			read = false;
			write = false;
			execute = false;
		} else {
			read = acl.toLowerCase().contains("r");
			write = acl.toLowerCase().contains("w");
			execute = acl.toLowerCase().contains("x");
		}
		return this;
	}

	/**
	 * @return true or false
	 */
	public boolean hasAnyRight() {
		return read || write || execute;
	}

	/**
	 * @return the read
	 */
	public boolean getRead() {
		return read;
	}

	/**
	 * @param read
	 *          the read
	 */
	public void setRead(boolean read) {
		this.read = read;
	}

	/**
	 * @return the write
	 */
	public boolean getWrite() {
		return write;
	}

	/**
	 * @param write
	 *          the write
	 */
	public void setWrite(boolean write) {
		this.write = write;
	}

	/**
	 * @return the execute
	 */
	public boolean getExecute() {
		return execute;
	}

	/**
	 * @param execute
	 *          the execute
	 */
	public void setExecute(boolean execute) {
		this.execute = execute;
	}
}
