package square2.modules.model;

/**
 * 
 * @author henkej
 * 
 */
public class RightBean {
	private Integer rightId;
	private String rightName;

	/**
	 * create new right bean
	 * 
	 * @param rightId
	 *          the id of the right
	 * @param rightName
	 *          the name of the right
	 */
	public RightBean(Integer rightId, String rightName) {
		this.rightId = rightId;
		this.rightName = rightName;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append(rightName).append("(").append(rightId).append(")");
		return buf.toString();
	}

	/**
	 * @return the rightId
	 */
	public Integer getRightId() {
		return rightId;
	}

	/**
	 * @return the rightName
	 */
	public String getRightName() {
		return rightName;
	}
}
