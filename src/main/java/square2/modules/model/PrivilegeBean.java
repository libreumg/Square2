package square2.modules.model;

import java.io.Serializable;

import de.ship.dbppsquare.square.enums.EnumAccesslevel;

/**
 * 
 * @author henkej
 *
 */
public class PrivilegeBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private final String name;
	private final Integer pk;
	private final String acl;

	/**
	 * create new privilege bean
	 * 
	 * @param pk
	 *          the pk
	 * @param name
	 *          the name
	 * @param acl
	 *          the acl
	 */
	public PrivilegeBean(Integer pk, String name, EnumAccesslevel acl) {
		this.pk = pk;
		this.name = name;
		this.acl = acl == null ? null : acl.getLiteral();
	}

	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append(name == null ? pk : name);
		buf.append("=");
		buf.append(acl);
		return buf.toString();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @return the acl
	 */
	public String getAcl() {
		return acl;
	}
}
