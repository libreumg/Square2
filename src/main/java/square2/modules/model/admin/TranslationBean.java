package square2.modules.model.admin;

/**
 * 
 * @author henkej
 *
 */
public class TranslationBean {
	private final String isocode;
	private final String key;
	private String value;

	/**
	 * @param isocode
	 *          the isocode
	 * @param key
	 *          the key
	 * @param value
	 *          the value
	 */
	public TranslationBean(String isocode, String key, String value) {
		super();
		this.isocode = isocode;
		this.key = key;
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *          the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the isocode
	 */
	public String getIsocode() {
		return isocode;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}
}
