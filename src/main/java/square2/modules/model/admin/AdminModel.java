package square2.modules.model.admin;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.servlet.http.Part;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;

import de.ship.dbppsquare.square.tables.records.TMetadatatypeRecord;
import ship.jsf.components.selectFilterableMenu.FilterableList;
import square2.db.control.AdminGateway;
import square2.db.control.SnippletGateway;
import square2.db.control.StatisticGateway;
import square2.db.control.StudyGateway;
import square2.help.ContextKey;
import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.ApplicationBean;
import square2.modules.model.ModelException;
import square2.modules.model.ProfileBean;
import square2.modules.model.SquareModel;
import square2.modules.model.UserApplRightBean;
import square2.modules.model.report.design.SnipplettypeBean;
import square2.modules.model.statistic.FunctioninputtypeBean;
import square2.modules.model.statistic.FunctionoutputtypeBean;
import square2.modules.model.study.MetadatatypeBean;
import square2.r.control.SquareControl2Gateway;

/**
 * 
 * @author henkej
 *
 */
@Named
@SessionScoped
public class AdminModel extends SquareModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LogManager.getLogger(AdminModel.class);

	private List<UserApplRightBean> roles;
	private List<UserBean> square2users;
	private List<ProfileBean> users;
	private FilterableList applRoles;
	private Integer usnr;
	private Integer usnrdel;
	private Integer rightId;
	private Integer pk;
	private String name;
	private AclBean acl;
	private List<String> sessionInspection;
	private List<UserLoginBean> logins;
	private FunctionoutputtypeBean functionoutputtype;
	private List<FunctionoutputtypeBean> functionoutputtypes;
	private List<SnipplettypeBean> snipplettypes;
	private Level level;
	private FunctioninputtypeBean functioninputtype;
	private List<FunctioninputtypeBean> functioninputtypes;
	private List<UsergroupBean> usergroups;
	private List<MaintenanceBean> maintenances;
	private MaintenanceBean maintenance;
	private Part file;
	private List<ImpressumBean> impressums;
	private List<TranslationBean> translations;
	private List<FunctioncategoryBean> functioncategories;
	private FunctioncategoryBean functioncategory;
	private List<MetadatatypeBean> metadatatypes;
	private MetadatatypeBean metadatatypeBean;
  private UserBean userBean;

	/**
	 * generate the model for the administration module
	 */
	public AdminModel() {
		this.acl = new AclBean();
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see square2.modules.model.SquareModel#setAllSquareUsers(square2.help.
	 * SquareFacesContext, java.lang.Object[])
	 */
	@Override
	public void setAllSquareUsers(SquareFacesContext facesContext, Object... params)
			throws ModelException, DataAccessException {
		// not yet implemented
	}

	/**
	 * get all snipplet types from the database
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return true for successful database operation, false otherwise
	 */
	public boolean resetSnippletadmin(SquareFacesContext facesContext) {
		snipplettypes = new ArrayList<>();
		try {
			snipplettypes.addAll(new SnippletGateway(facesContext).getAllTypes());
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * execute square.test(configfile) on squareControl
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return true for valid execution, false otherwise
	 */
	public boolean testR(SquareFacesContext facesContext) {
		try (SquareControl2Gateway gw = new SquareControl2Gateway(facesContext)) {
			String configFile = (String) facesContext.getExternalContext().getApplicationMap()
					.get(ContextKey.RSERVERACCESS.get());
			String msgStr = gw.getTest(configFile);
			facesContext.notifyInformation("{0}", msgStr);
			return true;
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * @param facesContext
	 *          the context of this function call
	 */
	public void loadMaintenances(SquareFacesContext facesContext) {
		try {
			maintenances = new AdminGateway(facesContext).getAllMaintenances();
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * @param facesContext
	 *          the context of this function call
	 * @param pk
	 *          the pk
	 * @return true or false
	 */
	public boolean removeMaintenance(SquareFacesContext facesContext, Integer pk) {
		try {
			new AdminGateway(facesContext).deleteMaintenance(pk);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * @param facesContext
	 *          the context of this function call
	 * @return true or false
	 */
	public boolean mergeMaintenance(SquareFacesContext facesContext) {
		try {
			AdminGateway gw = new AdminGateway(facesContext);
			if (maintenance.getPk() == null) {
				gw.insertMaintenance(maintenance);
			} else {
				gw.updateMaintenance(maintenance);
			}
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * @return the list of snipplet types
	 */
	public List<SnipplettypeBean> getSnipplettypes() {
		return snipplettypes;
	}

	/**
	 * @param id
	 *          the id of the snipplet type
	 * @param facesContext
	 *          the context of this function call
	 * @return true
	 */
	public boolean updateSnipplettype(Integer id, SquareFacesContext facesContext) {
		for (SnipplettypeBean b : snipplettypes) {
			if (b.getId().equals(id)) {
				try {
					new SnippletGateway(facesContext).updateSnippletTypeBean(b);
				} catch (DataAccessException e) {
					facesContext.notifyException(e);
					LOGGER.error(e.getMessage(), e);
				}
			}
		}
		return true;
	}

	/**
	 * @param facesContext
	 *          the context of this function call
	 * @return true or false
	 */
	public boolean showUsers(SquareFacesContext facesContext) {
		try {
			AdminGateway gw = new AdminGateway(facesContext);
			square2users = gw.getSquare2users();
			users = gw.getAllUsers(false);
			userBean = new UserBean();
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			square2users = new ArrayList<>();
			return false;
		}
	}

	/**
	 * @param facesContext
	 *          the context of this function call
	 * @return true
	 */
	public boolean reloadTranslations(SquareFacesContext facesContext) {
		facesContext.reloadTranslations();
		facesContext.notifyInformation("translations reloaded");
		return true;
	}

	/**
	 * @param applicationBean
	 *          the application bean
	 * @param facesContext
	 *          the context of this function call
	 * @return true
	 */
	public boolean showSession(ApplicationBean applicationBean, SquareFacesContext facesContext) {
		sessionInspection = new ArrayList<>();
		sessionInspection.add(applicationBean.toString());
		StringBuilder buf = new StringBuilder("translations: ");
		for (Map.Entry<String, Map<String, String>> translation : facesContext.getTranslations().entrySet()) {
			buf.append(new StringBuilder(translation.getKey()).append(": ").append(translation.getValue().size())
					.append(" entries"));
			buf.append(", ");
		}
		sessionInspection.add(buf.toString());
		translations = new ArrayList<>();
		Map<String, Map<String, String>> t = facesContext.getTranslations();
		for (Entry<String, Map<String, String>> i : t.entrySet()) {
			String isocode = i.getKey();
			for (Entry<String, String> kv : i.getValue().entrySet()) {
				String key = kv.getKey();
				String value = kv.getValue();
				if (isocode != null && key != null) {
					translations.add(new TranslationBean(isocode, key, value));
				} else {
					translations.add(new TranslationBean("error", "one of the values is null",
							new StringBuilder("isocode=").append(isocode).append(",key=").append(key).toString()));
				}
			}
		}
		return true;
	}

	/**
	 * @param applicationBean
	 *          the application bean
	 * @return true
	 */
	public boolean showLogins(ApplicationBean applicationBean) {
		logins = applicationBean.getProfiles();
		return true;
	}

	/**
	 * @param facesContext
	 *          the context of this function call
	 * @param bean
	 *          the function input type bean
	 */
	public void toFunctioninputtype(SquareFacesContext facesContext, FunctioninputtypeBean bean) {
		functioninputtype = bean;
		try {
			functioninputtypes = new StatisticGateway(facesContext).getAllInputtypes();
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			functioninputtypes = new ArrayList<>();
		}
	}

	/**
	 * @param facesContext
	 *          the context of this function call
	 * @return true or false
	 */
	public boolean updateFunctioninputtype(SquareFacesContext facesContext) {
		try {
			new StatisticGateway(facesContext).updateFunctioninputtype(functioninputtype);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * @param facesContext
	 *          the context of this function call
	 */
	public void toFunctionoutputtype(SquareFacesContext facesContext) {
		functionoutputtype = new FunctionoutputtypeBean(null);
		try {
			functionoutputtypes = new StatisticGateway(facesContext).getAllFunctionoutputtypes();
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			functionoutputtypes = new ArrayList<>();
		}
	}

  /**
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean addFunctionoutputtype(SquareFacesContext facesContext) {
    for (FunctionoutputtypeBean bean : functionoutputtypes) {
      if (bean.getName().equals(functionoutputtype.getName())) {
        facesContext.notifyError("error.name.exists", functionoutputtype.getName());
        return false; // interrupt immediately
      }
    }
    try {
      new StatisticGateway(facesContext).addFunctionoutputtype(functionoutputtype);
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

	/**
	 * @param pk
	 *          the function output type id
	 * @param facesContext
	 *          the context of this function call
	 */
	public void deleteFunctionoutputtype(Integer pk, SquareFacesContext facesContext) {
		try {
			new StatisticGateway(facesContext).deleteFunctionoutputtype(pk);
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * @param facesContext
	 *          the context of this function call
	 * @return true or false
	 */
	public boolean createLogin(SquareFacesContext facesContext) {
		String password = new AdminGateway(facesContext).createLogin(usnr);
		facesContext.notifyInformation("new password: {0}", password);
		return true;
	}

	/**
	 * @param facesContext
	 *          the context of this function call
	 * @param usnr
	 *          the id of the user
	 * @param days
	 *          the number of days that this account should be extended to
	 * @return true or false
	 */
	public boolean addExpire(SquareFacesContext facesContext, Integer usnr, Integer days) {
		if (days == null) {
			facesContext.notifyError("days is null");
			return false;
		}
		Date d = new Date();
		for (UserBean bean : square2users) {
			if (bean.getUsnr().equals(usnr)) {
				d = bean.getExpire();
				Long ms = days.longValue() * 86400000l;
				d.setTime(d.getTime() + ms);
			}
		}
		try {
			new AdminGateway(facesContext).extendExpire(usnr, d);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * set the expire date
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param usnr
	 *          id of the user to set the login time
	 * @param daysFromNow
	 *          the number of days from now, might be negative
	 * @return true or false
	 */
	public boolean setExpire(SquareFacesContext facesContext, Integer usnr, Integer daysFromNow) {
		if (daysFromNow == null) {
			facesContext.notifyError("daysFromNow is null");
			return false;
		}
		Date d = new Date();
		Long ms = daysFromNow.longValue() * 86400000l;
		d.setTime(d.getTime() + ms);
		try {
			new AdminGateway(facesContext).extendExpire(usnr, d);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * load user groups
	 * 
	 * @param facesContext
	 *          the context of this function call
	 */
	public void loadUsergroups(SquareFacesContext facesContext) {
		AdminGateway gw = new AdminGateway(facesContext);
		users = gw.getAllSquareUsers();
		usergroups = gw.loadUsergroups();
	}

	/**
	 * @param facesContext
	 *          the context of this function call
	 * @return true for no database exception, false otherwise
	 */
	public boolean showApplroles(SquareFacesContext facesContext) {
		try {
			AdminGateway gw = new AdminGateway(facesContext);
			roles = gw.getUserPrivilegeRoles();
			users = gw.getAllSquareUsers();
			applRoles = gw.getApplRoles();
			usergroups = gw.loadUsergroups();
			acl = new AclBean();
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			roles = new ArrayList<>();
			users = new ArrayList<>();
			applRoles = new FilterableList();
			usergroups = new ArrayList<>();
			return false;
		}
	}

	/**
	 * remove application rights
	 * 
	 * @param usnr
	 *          of the user
	 * @param privilegeId
	 *          of the right
	 * @param facesContext
	 *          the context of this function call
	 * @return true for successful removal, false otherwise
	 */
	public boolean removeApplright(Integer usnr, Integer privilegeId, SquareFacesContext facesContext) {
		try {
			Set<Integer> privs = new HashSet<>();
			privs.add(privilegeId);
			new AdminGateway(facesContext).removeUserGroupPrivilege(usnr, null, privs);
			facesContext.notifyInformation("removed right {0} from user {1}", privilegeId, usnr);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * remove application role from database
	 * 
	 * @param usnr
	 *          of user if any; one of usnr or group must be set
	 * @param group
	 *          of group if any; one of usnr or group must be set
	 * @param rightId
	 *          id of the privilege
	 * @param facesContext
	 *          the context of this function call
	 * @return true for successful changes, false on database error
	 */
	public boolean removeApplrole(Integer usnr, Integer group, Integer rightId, SquareFacesContext facesContext) {
		try {
			Set<Integer> privileges = new HashSet<>();
			privileges.add(rightId);
			new AdminGateway(facesContext).removeUserGroupPrivilege(usnr, group, privileges);
			facesContext.notifyInformation("removed role {0} from user {1} and/or group {2}", rightId, usnr, group);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * add application privilege of current usnr, rightId and acl
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return true for successful db change, false otherwise
	 */
	public boolean addApplright(SquareFacesContext facesContext) {
		try {
			new AdminGateway(facesContext).addUserGroupPrivilege(usnr, null, rightId, acl);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * create role in database for current usnr, pk, rightId and acl
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return true for successful database change, false otherwise
	 */
	public boolean addApplrole(SquareFacesContext facesContext) {
		try {
			new AdminGateway(facesContext).addUserGroupPrivilege(usnr, pk, rightId, acl);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * remove user from database
	 * 
	 * @param usnr
	 *          of user to be removed
	 * @param facesContext
	 *          the context of this function call
	 * @return true if deletion was successful, false otherwise
	 */
	public boolean removeUser(Integer usnr, SquareFacesContext facesContext) {
		try {
			Integer deleted = new AdminGateway(facesContext).deleteUserLogin(usnr);
			if (!deleted.equals(1)) {
				throw new DataAccessException("deleted entries in database: " + deleted);
			}
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	private boolean passCheck(SquareFacesContext facesContext, String langKey, List<String> list) {
		if (list.size() > 0) {
  		StringBuilder buf = new StringBuilder();
  		boolean first = true;
  	  for (String s : list) {
  	  	buf.append(first ? "" : ",").append(s);
  	  	first = false;
  	  }
  		facesContext.notifyError(langKey, buf.toString());
  		return false;
  	} else {
  		return true;
		}
	}
	
  /**
   * delete user from database
   * 
   * @param facesContext
   *          the context of this function call
   * @return true if deletion was successful, false otherwise
   */
  public boolean deleteUser(SquareFacesContext facesContext) {
		try {
			AdminGateway gw = new AdminGateway(facesContext);
			boolean check = passCheck(facesContext, "error.admin.user.delete.isingroups", gw.findUsergroupNamesOfUser(usnrdel));
			check = check && passCheck(facesContext, "error.admin.user.delete.hasrole", gw.findRolenameOfUser(usnrdel));
			if (check) {
				Integer deleted = gw.deletePerson(usnrdel);
				if (!deleted.equals(1)) {
					throw new DataAccessException("deleted entries in database: " + deleted);
				}
				return true;
			} else {
				return false;
			}
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
  }

	/**
	 * get all users with a login in Square²
	 * 
	 * @return list of Square² users
	 */
	public List<ProfileBean> getUsers() {
		return users;
	}

	/**
	 * @return app roles
	 */
	public FilterableList getApplRoles() {
		return applRoles;
	}

	/**
	 * @return list of user beans
	 */
	public List<UserBean> getSquare2users() {
		return square2users;
	}

	/**
	 * @return id of user
	 */
	public Integer getUsnr() {
		return usnr;
	}

	/**
	 * @param usnr
	 *          the id of a user
	 */
	public void setUsnr(Integer usnr) {
		this.usnr = usnr;
	}

	/**
	 * @return the privilege id
	 */
	public Integer getRightId() {
		return rightId;
	}

	/**
	 * @param rightId
	 *          the privilege id
	 */
	public void setRightId(Integer rightId) {
		this.rightId = rightId;
	}

	/**
	 * @return the list of inspections
	 */
	public List<String> getSessionInspection() {
		return sessionInspection;
	}

	/**
	 * @return the list of user logins
	 */
	public List<UserLoginBean> getLogins() {
		return logins;
	}

	/**
	 * @return the acl bean
	 */
	public AclBean getAcl() {
		return acl;
	}

	/**
	 * @return the roles
	 */
	public List<UserApplRightBean> getRoles() {
		return roles;
	}

	/**
	 * @return the function output types
	 */
	public List<FunctionoutputtypeBean> getFunctionoutputtypes() {
		return functionoutputtypes;
	}

	/**
	 * @return the function output type bean
	 */
	public FunctionoutputtypeBean getFunctionoutputtype() {
		return functionoutputtype;
	}

	/**
	 * @param facesContext
	 *          the context of this function call
	 * @param group
	 *          the user group bean
	 * @param user
	 *          the user profile bean
	 */
	public void removeUserFromGroup(SquareFacesContext facesContext, UsergroupBean group, ProfileBean user) {
		try {
			new AdminGateway(facesContext).removeUserFromGroup(group.getPk(), user.getUsnr());
			facesContext.notifyInformation("info.delete.userfromgroup", user.getForename(), user.getSurname(),
					group.getName());
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * @param facesContext
	 *          the context of this function call
	 */
	public void addUserToGroup(SquareFacesContext facesContext) {
		try {
			new AdminGateway(facesContext).addUserToGroup(pk, usnr);
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
	}

	private final void checkValidName(SquareFacesContext facesContext, String name, String field) throws DataAccessException {
	  if (name == null) {
	    throw new DataAccessException(facesContext.translate("error.mustnotbeempty", field));
	  } else if (name.isEmpty()) {
      throw new DataAccessException(facesContext.translate("error.mustnotbeempty", field));
	  } else if (!name.matches("(?U)[\\p{L}\\p{M}\\s'-]+")) {
	    throw new DataAccessException(facesContext.translate("error.isnotavalidname", name));
	  }
	}
	
  /**
   * add user to the database
   * 
   * @param facesContext the faces context
   * @return true or false
   */
  public boolean addUser(SquareFacesContext facesContext) {
    try {
      // check for valid username, restrict to alphabetical values
      checkValidName(facesContext, userBean.getForename(), "admin.table.right.forename");
      checkValidName(facesContext, userBean.getSurname(), "admin.table.right.surname");
      checkValidName(facesContext, userBean.getUsername(), "admin.table.right.username");
      return new AdminGateway(facesContext).addUser(userBean) > 0;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      return false;
    }
  }

	/**
	 * @param facesContext
	 *          the context of this function call
	 * @return true or false
	 */
	public boolean addGroup(SquareFacesContext facesContext) {
		try {
			if (name == null || name.isEmpty()) {
				throw new DataAccessException(facesContext.translate("error.mustnotbeempty", "label.admin.usergroup.name"));
			}
			new AdminGateway(facesContext).addGroup(name);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			return false;
		}
	}

	/**
	 * @param facesContext
	 *          the context of this function call
	 * @param bean
	 *          the user group bean
	 */
	public void deleteGroup(SquareFacesContext facesContext, UsergroupBean bean) {
		try {
			new AdminGateway(facesContext).deleteGroup(bean.getPk());
			facesContext.notifyInformation("info.delete.group", bean.getName());
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * @param facesContext
	 *          the context of this function call
	 */
	public void doFileUpload(SquareFacesContext facesContext) {
		try (InputStream fis = file.getInputStream(); Scanner scanner = new Scanner(fis)) {
			StringBuilder buf = new StringBuilder();
			while (scanner.hasNext()) {
				buf.append("\n").append(scanner.next());
			}
			facesContext.notifyInformation("file: {0}, size: {1}, type: {2}", file.getName(), file.getSize(),
					file.getContentType());
			facesContext.notifyInformation(buf.toString());
		} catch (IOException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * load impressum of current locale
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return true if impressum could be loaded, false otherwise. Note: null is
	 *         also valid for non existing impressums!
	 */
	public Boolean loadImpressum(SquareFacesContext facesContext) {
		try {
			impressums = new AdminGateway(facesContext).getImpressums();
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * @param facesContext
	 *          the context of this function call
	 * @param bean
	 *          the impressum bean
	 * @return true or false
	 */
	public Boolean upsertImpressum(SquareFacesContext facesContext, ImpressumBean bean) {
		try {
			new AdminGateway(facesContext).upsertImpressum(bean);
			facesContext.resetTranslation(bean.getIsocode(), "label.square.impressum", bean.getContent());
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * load all for function category list page
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return true or false
	 */
	public Boolean toFunctioncategoryList(SquareFacesContext facesContext) {
		try {
			functioncategories = new StatisticGateway(facesContext).getFunctioncategories(null);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * load all metadatatypes for list page
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return true or false
	 */
	public Boolean toMetadatatypeList(SquareFacesContext facesContext) {
		try {
			metadatatypes = new StudyGateway(facesContext).getMetadatatypes();
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * prepare editing of function category bean
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param bean
	 *          the function category bean
	 * @return true or false
	 */
	public Boolean toEditFunctioncategory(SquareFacesContext facesContext, FunctioncategoryBean bean) {
		functioncategory = bean;
		functioncategory.setOldName(bean.getName());
		// TODO: load usages of this category from the database
		return true;
	}

	/**
	 * prepare editing metadata type bean
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param bean
	 *          the metadatatype bean
	 * @return true or false
	 */
	public Boolean toEditMetadatatype(SquareFacesContext facesContext, MetadatatypeBean bean) {
		metadatatypeBean = bean;
		return true;
	}

	/**
	 * delete metadatatype
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param bean
	 *          the metadatatype bean
	 * @return true or false
	 */
	public Boolean doDeleteMetadatatype(SquareFacesContext facesContext, TMetadatatypeRecord bean) {
		try {
			Integer found = new StudyGateway(facesContext).deleteMetadatatype(bean);
			facesContext.notifyAffected(found);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error("{}", e.getMessage());
			return false;
		}
	}

	/**
	 * delete function category
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param pk
	 *          the id of the function category
	 * @return true or false
	 */
	public boolean doDeleteFunctioncategory(SquareFacesContext facesContext, Integer pk) {
		try {
			Integer found = new StatisticGateway(facesContext).deleteFunctioncategory(pk);
			facesContext.notifyAffected(found);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error("{}", e.getMessage());
			return false;
		}
	}

	/**
	 * upsert function category
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return true or false
	 */
	public boolean doUpsertFunctioncategory(SquareFacesContext facesContext) {
		try {
			Integer found = new StatisticGateway(facesContext).upsertFunctioncategory(functioncategory);
			facesContext.notifyAffected(found);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error("{}", e.getMessage());
			return false;
		}
	}

	/**
	 * upsert metadatatype
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return true or false
	 */
	public boolean doUpsertMetadatatype(SquareFacesContext facesContext) {
		try {
			Integer found = new StudyGateway(facesContext).upsertMetadatatype(metadatatypeBean);
			facesContext.notifyAffected(found);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error("{}", e.getMessage());
			return false;
		}
	}

	/**
	 * @return the level
	 */
	public Level getLevel() {
		return level;
	}

	/**
	 * @param level
	 *          the level
	 */
	public void setLevel(Level level) {
		this.level = level;
	}

	/**
	 * @return the function input type bean
	 */
	public FunctioninputtypeBean getFunctioninputtype() {
		return functioninputtype;
	}

	/**
	 * @return the list of function input types
	 */
	public List<FunctioninputtypeBean> getFunctioninputtypes() {
		return functioninputtypes;
	}

	/**
	 * @return the list of user groups
	 */
	public List<UsergroupBean> getUsergroups() {
		return usergroups;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk
	 *          the pk
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the list of maintenances
	 */
	public List<MaintenanceBean> getMaintenances() {
		return maintenances;
	}

	/**
	 * @return the maintenance bean
	 */
	public MaintenanceBean getMaintenance() {
		return maintenance;
	}

	/**
	 * @param maintenance
	 *          the maintenance bean
	 */
	public void setMaintenance(MaintenanceBean maintenance) {
		this.maintenance = maintenance;
	}

	/**
	 * @return the file
	 */
	public Part getFile() {
		return file;
	}

	/**
	 * @param file
	 *          the file
	 */
	public void setFile(Part file) {
		this.file = file;
	}

	/**
	 * @return the list of impressums
	 */
	public List<ImpressumBean> getImpressums() {
		return impressums;
	}

	/**
	 * @return the translations
	 */
	public List<TranslationBean> getTranslations() {
		return translations;
	}

	/**
	 * @return the functioncategories
	 */
	public List<FunctioncategoryBean> getFunctioncategories() {
		return functioncategories;
	}

	/**
	 * @return the functioncategory
	 */
	public FunctioncategoryBean getFunctioncategory() {
		return functioncategory;
	}

	/**
	 * @param functioncategory
	 *          the functioncategory to set
	 */
	public void setFunctioncategory(FunctioncategoryBean functioncategory) {
		this.functioncategory = functioncategory;
	}

	/**
	 * @return the metadatatypes
	 */
	public List<MetadatatypeBean> getMetadatatypes() {
		return metadatatypes;
	}

	/**
	 * @return the metadatatypeBean
	 */
	public MetadatatypeBean getMetadatatypeBean() {
		return metadatatypeBean;
	}

	/**
	 * @param metadatatypeBean
	 *          the metadatatypeBean to set
	 */
	public void setMetadatatypeBean(MetadatatypeBean metadatatypeBean) {
		this.metadatatypeBean = metadatatypeBean;
	}

  /**
   * @return the userBean
   */
  public UserBean getUserBean() {
    return userBean;
  }

  /**
   * @return the usnrdel
   */
  public Integer getUsnrdel() {
    return usnrdel;
  }

  /**
   * @param usnrdel the usnrdel to set
   */
  public void setUsnrdel(Integer usnrdel) {
    this.usnrdel = usnrdel;
  }
}
