package square2.modules.model.admin;

import java.util.Date;

/**
 * 
 * @author henkej
 *
 */
public class MaintenanceBean {
	private Integer pk;
	private String message;
	private Date downtimeStart;
	private Date downtimeEnd;

	/**
	 * create new maintenance bean
	 * 
	 * @param pk
	 *          the id
	 * @param message
	 *          the message
	 * @param downtimeStart
	 *          the downtime start
	 * @param downtimeEnd
	 *          the downtime end
	 */
	public MaintenanceBean(Integer pk, String message, Date downtimeStart, Date downtimeEnd) {
		this.pk = pk;
		this.message = message;
		this.downtimeStart = downtimeStart;
		this.downtimeEnd = downtimeEnd;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk
	 *          the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *          the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the downtimeStart
	 */
	public Date getDowntimeStart() {
		return downtimeStart;
	}

	/**
	 * @param downtimeStart
	 *          the downtimeStart to set
	 */
	public void setDowntimeStart(Date downtimeStart) {
		this.downtimeStart = downtimeStart;
	}

	/**
	 * @return the downtimeEnd
	 */
	public Date getDowntimeEnd() {
		return downtimeEnd;
	}

	/**
	 * @param downtimeEnd
	 *          the downtimeEnd to set
	 */
	public void setDowntimeEnd(Date downtimeEnd) {
		this.downtimeEnd = downtimeEnd;
	}
}
