package square2.modules.servlet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * @author henkej
 * 
 */
public class ImageServlet extends HttpServlet {
	private static final Logger LOGGER = LogManager.getLogger("Square2");
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String factoryName = request.getParameter("factory");
		String imageId = request.getParameter("imageId");
		Object o = request.getSession(true).getAttribute(factoryName);
		ImageServletFactory factory = null;
		if (o instanceof ImageServletFactory) {
			factory = (ImageServletFactory) o;
		} else {
			LOGGER.warn(factoryName + " not found in session: " + request.getSession().getAttributeNames().toString());
		}
		if (factory == null) {
			// response.sendError(HttpServletResponse.SC_NOT_FOUND);
			response.reset();
			response.setBufferSize(10240);
			response.setContentType("image/png");
			response.setHeader("Content-Length", "0");
			response.setHeader("Content-Disposition", "inline; filename=\"servletImageEmpty.png\"");
			response.getOutputStream().close();
		} else {
			factory.setImageId(Integer.valueOf(imageId));
			byte[] bytes = factory.getBytes();
			if (bytes == null) {
				response.reset();
				response.setBufferSize(10240);
				response.setContentType("image/png");
				response.setHeader("Content-Length", "0");
				response.setHeader("Content-Disposition", "inline; filename=\"servletImageEmpty.png\"");
				response.getOutputStream().close();
			} else {
				response.reset();
				response.setBufferSize(10240);
				response.setContentType("image/png");
				response.setHeader("Content-Length", String.valueOf(bytes.length));
				response.setHeader("Content-Disposition",
						"inline; filename=\"servletImage".concat(factoryName).concat(imageId).concat(".png\""));
				try (BufferedInputStream input = new BufferedInputStream(new ByteArrayInputStream(bytes), 10240);
						BufferedOutputStream output = new BufferedOutputStream(response.getOutputStream(), 10240)) {
					byte[] buffer = new byte[10240];
					int length;
					while ((length = input.read(buffer)) > 0) {
						output.write(buffer, 0, length);
					}
				}
			}
		}
	}
}
