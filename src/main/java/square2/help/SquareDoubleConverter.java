package square2.help;

import java.text.NumberFormat;
import java.util.regex.Pattern;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * 
 * @author henkej
 *
 */
@FacesConverter("squareDoubleConverter")
public class SquareDoubleConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null) {
			return null;
		} else if (value.trim().isEmpty()) {
			return null;
		} else if (value.contains(".")) {
			if (value.contains(",")) {
				return Double.valueOf(value.replaceAll(Pattern.quote("."), "").replaceAll(",", "."));
			} else {
				return Double.valueOf(value);
			}
		} else if (value.contains(",")) {
			return Double.valueOf(value.replaceAll(",", "."));
		} else {
			return Double.valueOf(value);
		}
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return null;
		}
		NumberFormat numberFormat = NumberFormat.getInstance();
		String s = "";
		try {
			s = numberFormat.format(value);
		} catch (IllegalArgumentException e) {
			s = e.getMessage();
		}
		return s;
	}
}
