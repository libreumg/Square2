package square2.help;

/**
 * 
 * @author henkej
 *
 */
public enum ContextKey {
	/**
	 * translations
	 */
	TRANSLATIONS("translations"),
	/**
	 * latexRoot
	 */
	LATEXROOT("latexroot"),
	/**
	 * latexBufSize
	 */
	LATEX_BUF_SIZE("latex-buf-size"),
	/**
	 * rConfigFile
	 */
	RSERVERACCESS("rserveraccess"),
	/**
	 * r-port
	 */
	R_PORT("r-port"),
	/**
	 * r-host
	 */
	R_HOST("r-host"),
	/**
	 * r-ssl
	 */
	R_SSL("r-ssl"),
	/**
	 * stage
	 */
	STAGE("stage"),
	/**
	 * shell for latex execution
	 */
	SHELL("shell"),
	/**
	 * output path for squarereportrenderer
	 * 
	 * @deprecated replace by SC_OUTPUT_PATH
	 */
	@Deprecated
	SR_OUTPUT_PATH("squarereportrenderer.output_path"),
	/**
	 * output path for sq2psqlrepository-resultio
	 */
	SC_OUTPUT_PATH("sq2psqlrepository.output_path"),
	/**
	 * config file path for opalRepository
	 */
	OR_CONFIG_PATH("opal.configfilename"),
	/**
	 * config section of the SHIP connection details 
	 */
	SHIP_CONFIG_SECTION("ship.configsection");
	
	private final String value;

	private ContextKey(String value) {
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public String get() {
		return value;
	}
}
