package square2.help;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class PrimefacesHelper {
  
  
  /**
   * find needle in haystack and return a list of matches
   * 
   * @param haystack the list of possible candidates
   * @param needle the value
   * @param ignoreCase if true, ignore case sensitive comparison
   * @return a list of found candidates; an empty one at least
   */
  public static final List<String> filter(List<String> haystack, String needle, Boolean ignoreCase){
    if (needle == null || needle.isBlank()) {
      return haystack;
    }
    List<String> list = new ArrayList<>();
    for (String found : haystack) {
      if (found != null) {
        if (ignoreCase) {
          found = found.toLowerCase();
          needle = needle.toLowerCase();
        }
        if (found.contains(needle)) {
          list.add(needle);
        }
      }
    }
    return list;
  }
}
