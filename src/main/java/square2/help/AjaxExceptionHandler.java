package square2.help;

import java.util.Iterator;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

/**
 * 
 * @author henkej
 *
 */
public class AjaxExceptionHandler extends ExceptionHandlerWrapper {
	private ExceptionHandler wrapped;

	/**
	 * @param exception
	 *          the exception handler
	 */
	public AjaxExceptionHandler(ExceptionHandler exception) {
		this.wrapped = exception;
	}

	@Override
	public ExceptionHandler getWrapped() {
		return wrapped;
	}

	@Override
	public void handle() {
		final Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents().iterator();
		while (i.hasNext()) {
			ExceptionQueuedEvent event = i.next();
			ExceptionQueuedEventContext context = (ExceptionQueuedEventContext) event.getSource();
			Throwable t = context.getException();
			final FacesContext fc = FacesContext.getCurrentInstance();
			if (fc == null) {
				String errorPageLocation = "/pages/viewexpired.jsf";
				FacesContext ctx = FacesContext.getCurrentInstance();
				ctx.setViewRoot(ctx.getApplication().getViewHandler().createView(ctx, errorPageLocation));
				ctx.getPartialViewContext().setRenderAll(true);
				ctx.renderResponse();
				return;
			} else {
				StringBuilder buf = new StringBuilder();
				for (StackTraceElement e : t.getStackTrace()) {
					buf.append(e.toString()).append(" ");
				}
				fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, t.getMessage(), buf.toString()));
				fc.getPartialViewContext().setRenderAll(true);
				fc.renderResponse();
			}
		}
		getWrapped().handle();
	}
}
