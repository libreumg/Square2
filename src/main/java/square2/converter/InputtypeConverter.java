package square2.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import square2.modules.model.statistic.FunctioninputtypeBean;

/**
 * 
 * @author henkej
 *
 */
@FacesConverter(value = "inputtype")
public class InputtypeConverter implements Converter<FunctioninputtypeBean> {

  @Override
  public FunctioninputtypeBean getAsObject(FacesContext context, UIComponent component, String value)
      throws ConverterException {
    return new FunctioninputtypeBean().fromJson(value);
  }

  @Override
  public String getAsString(FacesContext context, UIComponent component, FunctioninputtypeBean value)
      throws ConverterException {
    return value.toJson();
  }
}
