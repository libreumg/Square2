package square2.converter;

/**
 * @author henkej
 * @param <E> the class
 *
 */
public interface Selectable<E> {
  /**
   * @return the json representation of the class
   */
  public String toJson();

  /**
   * @param json the json representation
   * @return the class from the json representation
   */
  public E fromJson(String json);
}
