package square2.converter;

import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonSyntaxException;

import square2.help.SquareFacesContext;
import square2.modules.model.study.VariableAttributeRoleBean;

/**
 * 
 * @author henkej
 *
 */
@FacesConverter(value = "VariableAttributeRoleConverter")
public class VariableAttributeRoleConverter implements Converter<VariableAttributeRoleBean>, Serializable {
  private static final long serialVersionUID = 1L;
  private static final Logger LOGGER = LogManager.getLogger(VariableAttributeRoleConverter.class);

  @Override
  public VariableAttributeRoleBean getAsObject(FacesContext context, UIComponent component, String value)
      throws ConverterException {
    LOGGER.debug("try to convert {} to a VariableAttributeRoleBean", value);
    if (value == null) {
      return null;
    } else {
      try {
        VariableAttributeRoleBean bean = new VariableAttributeRoleBean(null, null, null, null, null, null).fromJson(value);
        LOGGER.debug("converted {} to {}", value, bean);
        return bean;
      } catch (JsonSyntaxException e) {
        LOGGER.error("error on converting {} to a VariableAttributeRoleBean: {}", value, e.getMessage());
        SquareFacesContext facesContext = (SquareFacesContext) context;
        facesContext.notifyException(e);
        return null;
      }
    }
  }

  @Override
  public String getAsString(FacesContext context, UIComponent component, VariableAttributeRoleBean value)
      throws ConverterException {
    LOGGER.debug("try to convert {} to json string", value);
    String json = value == null ? null : value.toJson();
    LOGGER.debug("converted {} to {}", value, json);
    return json;
  }
}
