package ship.commandline;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

/**
 * 
 * @author henkej
 * 
 */
public class ConsoleController {
	private String shell;

	/**
	 * Generate a controller for an execution on a shell.
	 * 
	 * @param shell
	 *          shell to be used
	 */
	public ConsoleController(String shell) {
		this.shell = shell;
	}

	/**
	 * Execute consoleCommand on given shell. Results are reported.
	 * 
	 * @param consoleCommand
	 *          to be used
	 * @return result from stderr and stdout
	 * @throws ConsoleException
	 *           for console exceptions
	 */
	public ConsoleResult executeOnShell(ConsoleCommand consoleCommand) throws ConsoleException {
		ConsoleResult consoleResult = new ConsoleResult(consoleCommand);
		try {
			ProcessBuilder pb = new ProcessBuilder(shell);
			pb.directory(consoleCommand.getExecuteFolder());
			Process proc = pb.start();
			BufferedReader log = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			BufferedReader err = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
			proc.getErrorStream();
			PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(proc.getOutputStream())), true);
			out.println(consoleCommand.toCommandString());
			out.println("exit");
			String line;
			while ((line = log.readLine()) != null) {
				consoleResult.getResultLines().add(line);
			}
			while ((line = err.readLine()) != null) {
				consoleResult.getErrorLines().add(line);
			}
			proc.waitFor();
			log.close();
			err.close();
			out.close();
			proc.destroy();
		} catch (IOException e) {
			throw new ConsoleException(e);
		} catch (Exception e) {
			throw new ConsoleException(e.getMessage());
		}
		return consoleResult;
	}
}
