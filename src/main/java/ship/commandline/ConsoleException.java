package ship.commandline;

/**
 * 
 * @author henkej
 *
 */
@SuppressWarnings("serial")
public class ConsoleException extends Exception {
	/**
	 * @param message
	 *          the message
	 */
	public ConsoleException(String message) {
		super(message);
	}

	/**
	 * @param e
	 *          the exception
	 */
	public ConsoleException(Exception e) {
		super(e);
	}
}
