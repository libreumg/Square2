package ship.commandline.thread;

import ship.commandline.ConsoleCommand;
import ship.commandline.ConsoleException;
import ship.commandline.ConsoleResult;

/**
 * 
 * @author henkej
 *
 */
public interface ThreadProcessor {
	/**
	 * execute this implementation when thread is finised;<br>
	 * <br>
	 * use this to put consoleResult error and output to wherever you want
	 * 
	 * @param consoleResult
	 *          the console result
	 */
	public void executeAfterRun(ConsoleResult consoleResult);

	/**
	 * get empty ConsoleResult for error messages;<br>
	 * <br>
	 * may contain ConsoleCommand informations
	 * 
	 * @return ConsoleResult
	 */
	public ConsoleResult getConsoleResult();

	/**
	 * execute this implementation when thread is started
	 * 
	 * @return ConsoleCommand to be executed
	 * @throws ConsoleException
	 *           for console exceptions
	 */
	public ConsoleCommand executeBeforeRun() throws ConsoleException;
}
