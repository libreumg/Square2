package ship.jsf.components.selectFilterableMenu;

import java.util.*;

/**
 * 
 * @author henkej
 *
 */
public interface FilterableListInterface extends List<BeanWrapperInterface> {
}
