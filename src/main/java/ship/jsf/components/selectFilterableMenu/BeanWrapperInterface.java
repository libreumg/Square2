package ship.jsf.components.selectFilterableMenu;

/**
 * 
 * @author henkej
 *
 */
public interface BeanWrapperInterface {
	/**
	 * get key of this entry
	 * 
	 * @return key
	 */
	public Integer getKey();

	/**
	 * get value of this entry
	 * 
	 * @return value
	 */
	public String getValue();
}
