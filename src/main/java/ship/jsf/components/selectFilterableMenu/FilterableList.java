package ship.jsf.components.selectFilterableMenu;

import java.util.*;

/**
 * 
 * @author henkej
 *
 */
public class FilterableList extends ArrayList<BeanWrapperInterface> implements FilterableListInterface {
	private static final long serialVersionUID = 1L;
}
