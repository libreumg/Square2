package ship.jsf.components.filteredSelect;

/**
 * 
 * @author henkej
 * 
 */
public interface MaskInterface {
	/**
	 * mask o to whatever is needed
	 * 
	 * @param o
	 *          to be masked
	 * @return String representation of o
	 */
	public String getMask(Object o);
}
