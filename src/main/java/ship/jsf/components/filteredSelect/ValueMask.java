package ship.jsf.components.filteredSelect;

/**
 * 
 * @author henkej
 * 
 */
public class ValueMask {
	private MaskInterface maskInterface;

	/**
	 * create a new ValueMask
	 * 
	 * @param maskInterface
	 *          the mask interface
	 */
	public ValueMask(MaskInterface maskInterface) {
		this.maskInterface = maskInterface;
	}

	/**
	 * get mask of o
	 * 
	 * @param o
	 *          to be used as key
	 * @return mask
	 */
	public String getMask(Object o) {
		return maskInterface.getMask(o);
	}
}
