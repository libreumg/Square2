package ship.jsf.components.dataTable;

/**
 * 
 * @author henkej
 *
 */
public class EnhancedDataTableModelException extends Exception {
	private static final long serialVersionUID = 297463154262824503L;

	/**
	 * create a new EnhancedDataTableModelException
	 * 
	 * @param message
	 *          the message
	 */
	public EnhancedDataTableModelException(String message) {
		super(message);
	}
}
