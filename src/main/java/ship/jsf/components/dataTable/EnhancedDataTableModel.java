package ship.jsf.components.dataTable;

import java.util.*;

import javax.faces.model.*;

/**
 * 
 * @author henkej
 * @param <T>
 *          the class of this model
 *
 */
public abstract class EnhancedDataTableModel<T> extends DataModel<T> {
	private DataModel<T> model;
	private Integer[] rows;

	/**
	 * create new instance of EnhancedDataTableModel with predefined model<br>
	 * <br>
	 * respect T to extend FilterableBean for full functionallity
	 * 
	 * @param model
	 *          DataModel of objects that extend FilterableBean
	 */
	public EnhancedDataTableModel(DataModel<T> model) {
		this.model = model;
		initRows();
	}

	/**
	 * create new instance of EnhancedDataTableModel with list as model<br>
	 * <br>
	 * respect T to extend FilterableBean for full functionallity
	 * 
	 * @param list
	 *          List of FilterableBeans
	 */
	public EnhancedDataTableModel(List<T> list) {
		this.model = new ListDataModel<T>(list);
		initRows();
	}

	/**
	 * sort data model by columnName ascending or descending
	 * 
	 * @param columnName
	 *          name of column to be sorted
	 * @param ascending
	 *          true for sorting ascending, false for descending
	 * @throws SortException
	 *           on errors
	 */
	public abstract void sort(String columnName, boolean ascending) throws SortException;

	/**
	 * filter data model; used to execute the filter routine
	 * 
	 * @throws EnhancedDataTableModelException
	 *           may happen when internal model doesn't extend FilterableBean
	 */
	public abstract void filter() throws EnhancedDataTableModelException;

	/**
	 * check, if bean fulfills filter condition for all its fields combined by and;
	 * use checkAnd <br>
	 * example:<br>
	 * <b><i>checkAnd(bean.field1, filter.field1) &amp;&amp; checkAnd(bean.field2,
	 * filter.field2)</i></b> ...
	 * 
	 * @param bean
	 *          data container for current operation
	 * @return true or false
	 */
	public abstract boolean getAndFilterResult(T bean);

	/**
	 * check, if bean fulfills filter condition for all its fields combined by or;
	 * use checkOr<br>
	 * <br>
	 * example:<br>
	 * <b><i>checkOr(bean.field1, filter.field1) || checkOr(bean.field2,
	 * filter.field2)</i></b> ...
	 * 
	 * @param bean
	 *          data container for current operation
	 * @return true or false
	 */
	public abstract boolean getOrFilterResult(T bean);

	/**
	 * filter data model combined by combineByAnd
	 * 
	 * @param combineByAnd
	 *          if true, and is used as combinator, otherwise, or is used
	 * @throws EnhancedDataTableModelException
	 *           may be thrown when internal model doesn't contain extended
	 *           FilterableBean
	 */
	public void filter(boolean combineByAnd) throws EnhancedDataTableModelException {
		for (T bean : getWrappedData()) {
			Boolean filterResult = true;
			if (combineByAnd) {
				filterResult = getAndFilterResult(bean);
			} else {
				filterResult = getOrFilterResult(bean);
			}
			if (bean instanceof FilterableBean) {
				((FilterableBean) bean).setDisplay(filterResult);
			} else {
				throw new EnhancedDataTableModelException("wrapped class is not of type FilterableBean");
			}
		}
	}

	/**
	 * check if row is displayed
	 */
	public boolean isRowAvailable() {
		T t = model.getRowData();
		if (t instanceof FilterableBean) {
			return model.isRowAvailable() && ((FilterableBean) model.getRowData()).getDisplay();
		} else {
			return model.isRowAvailable();
		}
	}

	/**
	 * check if o contains filter (case insensitive); return true as default value
	 * 
	 * @param o
	 *          object to be checked
	 * @param filter
	 *          to be used for check
	 * @return true or false
	 */
	public boolean checkAnd(Object o, Object filter) {
		return checkAnd(o == null ? null : o.toString(), filter == null ? null : filter.toString());
	}

	/**
	 * check if o contains filter (case insensitive); return false as default value
	 * 
	 * @param o
	 *          object to be checked
	 * @param filter
	 *          to be used for check
	 * @return true or false
	 */
	public boolean checkOr(Object o, Object filter) {
		return checkOr(o == null ? null : o.toString(), filter == null ? null : filter.toString());
	}

	/**
	 * check if s contains filter (case insensitive); return true as default value
	 * 
	 * @param s
	 *          string to be checked
	 * @param filter
	 *          to be used
	 * @return true or false
	 */
	public boolean checkAnd(String s, String filter) {
		if (s == null || filter == null || s.isEmpty() || filter.isEmpty()) {
			return true;
		} else {
			return s.toLowerCase().contains(filter.toLowerCase());
		}
	}

	/**
	 * check if s contains filter (case insensitive); return false as default value
	 * 
	 * @param s
	 *          string to be checked
	 * @param filter
	 *          to be used
	 * @return true or false
	 */
	public boolean checkOr(String s, String filter) {
		if (s == null || filter == null || s.isEmpty() || filter.isEmpty()) {
			return false;
		} else {
			return s.toLowerCase().contains(filter.toLowerCase());
		}
	}

	/**
	 * initialize rows of data model
	 */
	public void initRows() {
		int rowCount = model.getRowCount();
		if (rowCount != -1) {
			this.rows = new Integer[rowCount];
			for (int i = 0; i < rowCount; ++i) {
				rows[i] = i;
			}
		}
	}

	/**
	 * sort column by comparator
	 * 
	 * @param comparator
	 *          to be used
	 */
	public void sortBy(final Comparator<T> comparator) {
		Comparator<Integer> rowComp = new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				T e1 = getData(o1);
				T e2 = getData(o2);
				return comparator.compare(e1, e2);
			}
		};
		Arrays.sort(rows, rowComp);
	}

	/**
	 * get data of specified row
	 * 
	 * @param row
	 *          to be used for data
	 * @return row data
	 */
	private T getData(int row) {
		int originalRowIndex = model.getRowIndex();
		model.setRowIndex(row);
		T newRowData = model.getRowData();
		model.setRowIndex(originalRowIndex);
		return newRowData;
	}

	@Override
	public int getRowCount() {
		return model.getRowCount();
	}

	@Override
	public T getRowData() {
		return model.getRowData();
	}

	@Override
	public int getRowIndex() {
		return model.getRowIndex();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getWrappedData() {
		return (List<T>) model.getWrappedData();
	}

	@Override
	public void setRowIndex(int rowIndex) {
		if (0 <= rowIndex && rowIndex < rows.length) {
			model.setRowIndex(rows[rowIndex]);
		} else {
			model.setRowIndex(rowIndex);
		}
	}

	@Override
	public void setWrappedData(Object data) {
		model.setWrappedData(data);
		initRows();
	}
}
