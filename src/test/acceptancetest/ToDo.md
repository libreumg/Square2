#### Missing Functionality
- [ ] User and Group Functionality
- [ ] Process Variables
- [ ] Variable Types (Key/Normal/Process)
- [ ] Statistics
- [ ] Cloning of Function Selection and Report
- [ ] The extensive tweaking functionality in report generation
- [ ] Utilities for auto-generating new report document sample material
- [ ] Some Parts of the report document are not mapped to sample data

#### Missing Functionality currently worked on
- [ ] Rights/Roles: _Use_ needs to tackle a module and the subsequent module

#### Refactoring
- [ ] Make Error Checking Independent From Test Cases
- [ ] Dropdown selector and other utils: where to store identifiers? Right now they're in code
- [ ] Check if basic tests really need separate variable files or if it can be done similar to rights/roles-cases
- [ ] User Rights Roles File could have functionality for data table handeling, right now, each rights/roles-file implements almost the same function for that
- [ ] Write setup and teardown for all submodules with ordered screenshot directories per module

#### General ToDo
- [ ] specify the needed environment further (What studies etc. need to exist for the rights/roles tests?)

#### Errors
- [ ] right now, the tests do not run because
      a) there's the bug where logout does not see anything
      b) many things do still not seem to work on different sceen sizes