*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}

*** Variables ***
${SERVER}		  localhost:8380
${BROWSER}        Chrome
${DELAY}          0
${START URL}      http://${SERVER}/Square2
${LOGIN URL}      http://${SERVER}/Square2/pages/login.jsf


*** Keywords ***
Open Browser To Login Page
    IF    ${DEBUG}
        Open Browser    ${START URL}    Firefox
        Set Window Size    1366   768
    ELSE
        Open Firefox Headless    ${START URL}
    END
    # Maximize Browser Window
    Set Selenium Speed    ${DELAY}


Open Chrome Headless
    [Arguments]    ${url}
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    --disable-extensions
    Call Method    ${chrome_options}    add_argument    --headless
    Call Method    ${chrome_options}    add_argument    --disable-gpu
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    Create Webdriver    Chrome    chrome_options=${chrome_options}
    Set Window Size  1024  768
    Go To    ${url}

Open Firefox Headless
    [Arguments]    ${url}
    ${firefox_options}=    Evaluate    sys.modules['selenium.webdriver'].FirefoxOptions()    sys, selenium.webdriver
    Call Method    ${firefox_options}    add_argument    -headless
    Create Webdriver    Firefox    options=${firefox_options}
    #Set Window Size    800    600
    Go To    ${url}
