*** Settings ***
Documentation    Testing variable selection functionality
...
...
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}
Library           SeleniumUtil.py
Library           csvdatareader.py
Resource          ${BASEDIR}/lib/browser.robot
Resource          ${BASEDIR}/lib/study_structure.robot



*** Keywords ***
Load Synthetic Study Data
    ${CSV_DATA}=    Read CSV Data    ${DATA_DIRECTORY}
    Set Suite Variable    ${CSV_DATA}
    ${MISSINGCODES}=    Extract Missinglist    ${CSV_DATA}    MISSING_LIST
    ${JUMPCODES}=       Extract Missinglist    ${CSV_DATA}    JUMP_LIST
    Set Suite Variable    ${MISSINGCODES}
    Set Suite Variable    ${JUMPCODES}

Study Structure Basic Setup
    Set Screenshot Directory    ${SCREENSHOTS}/study_structure_basic
    Close All Browsers
    Load Synthetic Study Data
    Import Variables    ${BASEDIR}/data/data.py
    Import Variables    ${BASEDIR}/data/study_structure.py
    Open Browser And Log In

Study Selection
    User Views Studystructure Welcome
    User Selects Study    ${STUDY_NAME}
    Study Should Be Displayed    ${STUDY_NAME}
    No Error Should Be Notified


Department Creation
    User Views Studystructure Welcome
    User Selects Study    ${STUDY_NAME}
    Create Department    ${DEPT_TECHNAME}
    Department Should Be Displayed    ${DEPT_TECHNAME}
    No Error Should Be Notified


Variable Creation
    User Views Studystructure Welcome
    User Selects Study    ${STUDY_NAME}
    User Enters A Department    ${DEPT_TECHNAME}
    Create Variable    ${VAR_TECHNAME}    ${VAR_COLUMN}    ${VAR_DTYPE}
    Variable Should Be Displayed    ${VAR_TECHNAME}
    No Error Should Be Notified

Variable Deletion
    User Views Studystructure Welcome
    User Selects Study     ${STUDY_NAME}
    User Enters A Department    ${DEPT_TECHNAME}
    Delete Element    ${VAR_TECHNAME}
    Variable Should Not Be Displayed    ${VAR_TECHNAME}
    No Error Should Be Notified

Department Deletion
    User Views Studystructure Welcome
    User Selects Study    ${STUDY_NAME}
    Delete Element    ${DEPT_TECHNAME}
    Department Should Not Be Displayed    ${DEPT_TECHNAME}
    No Error Should Be Notified

Missinglist Creation
    [Tags]    long
    User Views Studystructure Welcome
    User Creates Missinglist And Enters Missingcodes   ${MISSINGLIST_NAME}    ${MISSINGCODES}    ${JUMPCODES}

Data Entering
    [Tags]    long
    User Views Studystructure Welcome
    User Selects Study    ${STUDY_NAME}
    Create Department    ${DATA_DEPARTMENT}
    Enter Data    ${CSV_DATA}
    Data Should Be Displayed     ${CSV_DATA}

Data Deletion
    [Tags]    long    singlemodule
    User Views Studystructure Welcome
    User Selects Study    ${STUDY_NAME}
    Delete Element    ${DATA_DEPARTMENT}

Missinglist Deletion
    [Tags]    long    singlemodule
    User Views Studystructure Welcome    ${STUDY_NAME}
    User Deletes Missinglist    ${MISSINGLIST_NAME}

Study Structure Basic Teardown
    Log Out And Close Browser




