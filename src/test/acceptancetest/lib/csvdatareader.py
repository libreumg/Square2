import csv
from robot.libraries.BuiltIn import BuiltIn


def read_csv_data(path):
    with open(path) as csvfile:
        #    BuiltIn().log_to_console("\nOpened CSV File")
        #   dialect = csv.Sniffer().sniff(csvfile.read())
        reader = csv.DictReader(csvfile)
        data = list()
        for row in reader:
            data.append(row)
        return data


def log_csv_data(data):
    for line in data:
        for key in line.keys():
            BuiltIn().log_to_console(f"{key}: {line[key]}")


def extract_missinglist(data, key):
    missingcodes = set()
    for row in data:
        if key in row:
            codes = row[key]
            for code in codes.split("|"):
                if 'NA' not in code:
                    missingcodes.add(code.strip())
    return sorted(missingcodes)
