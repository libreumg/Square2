*** Settings ***
Documentation    Testing data management functionality
...
...
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}
Library           SeleniumUtil.py
Resource          ${BASEDIR}/lib/browser.robot
Resource          ${BASEDIR}/lib/util.robot
Resource          ${BASEDIR}/lib/topmenu.robot
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/datamanagement   \    ${True}
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/    general    ${False}

*** Keywords ***
User Views Data Management Welcome
    Wait Until Page Contains Element    ${topmenu_button_dataMgmt}
    Click Element     ${topmenu_button_dataMgmt}

Add New Release
    [Arguments]    ${RELEASE_NAME}    ${DATA_DEPARTMENT}    ${SHORT_NAME}    ${DATABASE_TABLE}    ${MERGECOLUMN}    ${DATE}
    Wait Until Page Contains Element    ${formbutton_addRelease}
    Click Element    ${formbutton_addRelease}
    Wait Until Page Contains Element    ${add_button_add}
    Click Element    ${add_dropdown_relatedStudy}
    Scroll Element In Div Into View    ${add_template_dropdownelems_relatedStudy.format('${DATA_DEPARTMENT}')}    //ul[contains(@id,'fkelement')]
    Click Element    ${add_template_dropdownelems_relatedStudy.format('${DATA_DEPARTMENT}')}
    Input Text    ${add_input_releaseName}    ${RELEASE_NAME}
    Input Text    ${add_input_shortName}      ${SHORT_NAME}
    Input Text    ${add_input_date}           ${DATE}
    Click Element    ${general_dialogroot}
    Input Text    ${add_input_datasource}     ${DATABASE_TABLE}
    Click Element    ${add_dropdown_mergecolumn}
    Scroll Element In Div Into View    ${add_template_dropdownelems_relatedStudy.format('${DATA_DEPARTMENT}')}    //ul[contains(@id,'mergecolumn')]
    Click Element    ${add_template_dropdownelems_mergecolumn.format('${MERGECOLUMN}')}
    Click Button    ${add_button_add}
    Wait Until Page Contains Element    ${add_button_cancel}
    No Error Should Be Notified
    Click Button    ${add_button_cancel}
    Wait Until Page Contains Element    ${formbutton_addRelease}
    No Error Should Be Notified

Release Should Be Displayed
    [Arguments]    ${RELEASE_NAME}
    Wait Until Page Contains Element    ${formbutton_addRelease}
    Page Should Contain    ${RELEASE_NAME}

Delete Release
    [Arguments]    ${RELEASE_NAME}
    Wait Until Page Contains Element    ${formbutton_addRelease}
    Click Element    ${general_template_btnDelete.format('${RELEASE_NAME}')}
    Click First Interactable    ${general_button_ApproveDelete}

Release Should Not Be Displayed
    [Arguments]    ${RELEASE_NAME}
    Wait Until Page Contains Element    ${formbutton_addRelease}
    Page Should Not Contain    ${RELEASE_NAME}
