*** Settings ***
Documentation     Resource file exposing keywords specific to testing the rights and roles concept concerning the study overview
...
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}
Library           ${BASEDIR}/lib/SeleniumUtil.py
Resource          ${BASEDIR}/lib/browser.robot
Resource          ${BASEDIR}/lib/topmenu.robot
Resource          ${BASEDIR}/lib/util.robot
Resource          ${BASEDIR}/lib/study_overview.robot
Resource          ${BASEDIR}/lib/study_overview_basic.robot
Resource          ${BASEDIR}/lib/user_rights_roles.robot
Variables         ${BASEDIR}/data/rights_roles.py
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/    general    ${False}
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/study_structure/welcomeinitial    studystructure_welcome    ${True}

*** Variables ***
${STUDYGROUP_NAME}=    "rightsroles-grp"
${STUDY_NAME}=    "rightsroles-study"
${STUDY_START_DATE}=    "01.01.1999"
${STUDY_END_DATE}=    "13.10.2006"
${STUDY_UNIQUE_NAME}=    "rightsroles0"
${STUDY_PARTICIPANT_COUNT}=    100


*** Keywords ***
Study Overview Rights Roles Setup
    Set Screenshot Directory    ${SCREENSHOTS}/study_overview_rights_roles
    Import Variables    ${BASEDIR}/data/rights_roles.py
    Open Browser And Login
    User Views The Study Manager
    Add Studygroup    ${STUDYGROUP_NAME}
    User Is At Study Overview
    Add Data Collection    ${STUDY_NAME}    ${STUDY_START_DATE}    ${STUDY_END_DATE}    ${STUDY_PARTICIPANT_COUNT}    ${STUDY_UNIQUE_NAME}    ${STUDYGROUP_NAME}
    [Teardown]    Log Out And Close Browser

Give User Rights To Study
    [Arguments]    ${study}    ${user}    ${use}=${False}    ${write}=${False}    ${delete_previous}=${False}
    Open Browser And Login
    User Is At Study Overview
    Open Rights and Roles Panel From Datatable Element    ${study}
    IF    ${delete_previous}
        Delete Rights Entry    ${user}
    END
    Give User Rights    ${user}    use=${use}    write=${write}   read_exists=${False}
    Exit Rights And Roles Panel
    Log Out And Close Browser

Check Study Rights
    [Arguments]   ${study}    ${user}    ${password}    ${use}=${False}    ${write}=${False}
    Open Browser And Login As    ${user}    ${password}
    User Is At Study Overview
    IF    ${use}
        Page Should Contain    ${study}
    ELSE
        Page Should Not Contain    ${study}
    END
    IF    ${write}
        Page Should Contain Element    ${general_template_button_edit.format('${study}')}
        Go To Studystructure
        Page Should Contain Element    ${studystructure_welcome_template_button_editProcessVars.format('${STUDY_NAME}')}
    ELSE
        Page Should Not Contain Element    ${general_template_button_edit.format('${study}')}
        Go To Studystructure
        Page Should Not Contain Element    ${studystructure_welcome_template_button_editProcessVars.format('${STUDY_NAME}')}
    END
    Log Out And Close Browser

Study Overview Rights Roles
    Give User Rights To Study    ${STUDY_NAME}   ${RR_FULLNAME}
    Check Study Rights    ${STUDY_NAME}    ${RR_USER}    ${RR_PASSWORD}
    Give User Rights To Study    ${STUDY_NAME}   ${RR_FULLNAME}    use=${True}    write=${False}    delete_previous=${True}
    Check Study Rights    ${STUDY_NAME}      ${RR_USER}    ${RR_PASSWORD}   use=${True}    write=${False}
    Give User Rights To Study    ${STUDY_NAME}   ${RR_FULLNAME}   use=${True}    write=${True}    delete_previous=${True}
    Check Study Rights    ${STUDY_NAME}      ${RR_USER}    ${RR_PASSWORD}   use=${True}    write=${True}

Study Overview Rights Roles Teardown
    Open Browser And Login
    User Views The Study Manager
    Delete Studygroup    ${STUDYGROUP_NAME}
    Log Out And Close Browser

