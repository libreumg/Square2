*** Settings ***
Documentation     Resource file exposing keywords specific to the study overview test suite
...
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}
Library           ${BASEDIR}/lib/SeleniumUtil.py
Resource          ${BASEDIR}/lib/topmenu.robot
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/study_overview    \    ${True}
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/    general    ${False}


*** Keywords ***
Add Studygroup
    [Arguments]    ${STUDYGROUP_NAME}
    Wait Until Page Contains Element    ${studygroup_button_addStudygroup}
    Element Should Be Visible    ${studygroup_button_addStudygroup}
    Element Should Be Visible    ${studygroup_button_cohort}
    Click Button                 ${studygroup_button_addStudygroup}
    Element Should Be Visible    ${studygroup_add_button_addStudygroup}
    Element Should Be Visible    ${add_dialogroot}
    Input Text                   ${studygroup_add_input_studygroupName}        ${STUDYGROUP_NAME}    ${True}
    Click Button                 ${studygroup_add_button_addStudygroup}


Add Data Collection
    [Arguments]    ${STUDY_NAME}    ${STUDY_START_DATE}    ${STUDY_END_DATE}    ${STUDY_PARTICIPANT_COUNT}    ${STUDY_UNIQUE_NAME}    ${STUDYGROUP_NAME}
    Wait Until Page Contains Element    ${button_addCohort}
    Click Button                 ${button_addCohort}
    Wait Until Element Is Visible    ${add_dialogroot}
    Element Should Be Visible    ${add_dialogroot}
    Element Should Be Visible    ${add_button_addStudy}
    Click Element                ${add_dropdown_selectParentId}
    Scroll Element Into View     ${add_dropdown_elementStudyGrp.format('${STUDYGROUP_NAME}', '${STUDYGROUP_NAME}')}
    Click Element                ${add_dropdown_elementStudyGrp.format('${STUDYGROUP_NAME}', '${STUDYGROUP_NAME}')}
    Wait For Ajax Request
    Click Element                ${add_dialogroot}
    Input Text                   ${add_input_studyName}           ${STUDY_NAME}                 ${True}
    Wait For Ajax Request
    Click Element                ${add_dialogroot}
    Press Keys                   ${add_input_startDate}           ${STUDY_START_DATE}
    Wait For Ajax Request
    Click Element                ${add_dialogroot}
    Press Keys                   ${add_input_endDate}             ${STUDY_END_DATE}
    Click Element                ${add_dialogroot}
    Input Text                   ${add_input_participantCount}    ${STUDY_PARTICIPANT_COUNT}    ${True}
    Input Text                   ${add_input_uniqueName}          ${STUDY_UNIQUE_NAME}          ${True}
    Click Button                 ${add_button_addStudy}
    Wait For Ajax Request
    Wait Until Page Does Not Contain Element     ${add_dialogroot}

The Data Collection Should Be Present
    [Arguments]    ${STUDY_NAME}
    Page Should Contain          ${STUDY_NAME}

The Studygroup Should Be Present
    [Arguments]    ${STUDYGROUP_NAME}
    Page Should Contain          ${STUDYGROUP_NAME}

Delete Studygroup
    [Arguments]    ${STUDYGROUP_NAME}
    Wait Until Page Contains Element    ${general_template_btnDelete.format('${STUDYGROUP_NAME}')}
    Click Button                 ${general_template_btnDelete.format('${STUDYGROUP_NAME}')}
    Click First Interactable     ${general_button_approveDelete}

User Is At Study Overview
    Go To Studyoverview

User Views The Study Manager
    Go To Studyoverview
    Click Button                 ${button_manageStudygroup}



