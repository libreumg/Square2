*** Settings ***
Documentation    Testing study structure functionality
...
...
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}
Library           SeleniumUtil.py
Resource          ${BASEDIR}/lib/variable_selection.robot
Resource          ${BASEDIR}/lib/user_rights_roles.robot


*** Keywords ***
Variable Selection Rights Roles Setup
    Set Screenshot Directory    ${SCREENSHOTS}/variable_selection_rights_roles
    Import Variables     ${BASEDIR}/data/rights_roles.py
    Open Browser And Log In
    User Views Variable Selection Welcome
    Create Variable Selection    ${VARIABLESELECTION_NAME}    ${True}
    Log Out And Close Browser

Give User Rights To Selection
    [Arguments]    ${selection}    ${user}    ${read}=${False}    ${write}=${False}   ${use}=${False}      ${delete_previous}=${False}
    Open Browser And Login
    User Views Variable Selection Welcome
    Open Rights and Roles Panel From Datatable Element    ${selection}
    IF    ${delete_previous}
        Delete Rights Entry    ${user}
        Wait Until Page Does Not Contain Element    //td[contains(text(), '${user}')]
    END
    Give User Rights    ${user}    read=${read}    write=${write}   use=${use}    read_exists=${True}
    Exit Rights And Roles Panel
    Log Out And Close Browser

Check Selection Rights
    [Arguments]   ${selection}    ${user}    ${password}    ${read}=${False}    ${write}=${False}    ${use}=${False}
    Open Browser And Login As    ${user}    ${password}
    User Views Variable Selection Welcome
    IF    ${read}
        Page Should Contain    ${selection}
    ELSE
        Page Should Not Contain    ${selection}
    END
    IF    ${write}
        Page Should Contain Element    ${general_template_button_edit.format('${selection}')}
    ELSE
        Page Should Not Contain Element    ${general_template_button_edit.format('${selection}')}
    END
    Log Out And Close Browser
#
# Can only test for rights combinations that satisfy the schema "read <-> (use V write)" where V is inclusive or. Other combinations will get corrected by Square2
#
Variable Selection Rights Roles Test
    Give User Rights To Selection    ${VARIABLESELECTION_NAME}    ${RR_FULLNAME}
    Check Selection Rights    ${VARIABLESELECTION_NAME}    ${RR_USER}    ${RR_PASSWORD}
    Give User Rights To Selection    ${VARIABLESELECTION_NAME}    ${RR_FULLNAME}    read=${True}    write=${False}    use=${True}    delete_previous=${True}
    Check Selection Rights    ${VARIABLESELECTION_NAME}    ${RR_USER}    ${RR_PASSWORD}    read=${True}    write=${False}    use=${True}
    Give User Rights To Selection    ${VARIABLESELECTION_NAME}    ${RR_FULLNAME}    read=${True}    write=${True}    use=${False}    delete_previous=${True}
    Check Selection Rights    ${VARIABLESELECTION_NAME}    ${RR_USER}    ${RR_PASSWORD}    read=${True}    write=${True}    use=${False}
    Give User Rights To Selection    ${VARIABLESELECTION_NAME}    ${RR_FULLNAME}    read=${True}    write=${True}    use=${True}    delete_previous=${True}
    Check Selection Rights    ${VARIABLESELECTION_NAME}    ${RR_USER}    ${RR_PASSWORD}    read=${True}    write=${True}    use=${True}

Variable Selection Rights Roles Teardown
    Open Browser And Log In
    User Views Variable Selection Welcome
    Delete Variable Selection    ${VARIABLESELECTION_NAME}
    Log Out And Close Browser
