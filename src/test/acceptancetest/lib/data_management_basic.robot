*** Settings ***
Documentation    Testing data management functionality
...
...
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}
Library           SeleniumUtil.py
Resource          ${BASEDIR}/lib/data_management.robot


*** Keywords ***
Release Creation
    User Views Data Management Welcome
    Add New Release     ${RELEASE_NAME}    ${STUDY_NAME}    ${SHORT_NAME}    ${DATABASE_TABLE}    ${MERGECOLUMN}    ${DATE}
    Release Should Be Displayed    ${RELEASE_NAME}
    No Error Should Be Notified

Release Deletion
    User Views Data Management Welcome
    Delete Release    ${RELEASE_NAME}
    Release Should Not Be Displayed    ${RELEASE_NAME}
    No Error Should Be Notified

Data Management Basic Setup
    Close All Browsers
    Open Browser And Log In
    Set Screenshot Directory    ${SCREENSHOTS}/datamanagement_basic
    Import Variables    ${BASEDIR}/data/data_management.py
    Import Variables    ${BASEDIR}/data/data.py
