*** Settings ***
Documentation    Testing variable selection functionality
...
...
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}
Library           SeleniumUtil.py
Library           csvdatareader.py
Resource          ${BASEDIR}/lib/browser.robot
Resource          ${BASEDIR}/lib/util.robot
Resource          ${BASEDIR}/lib/topmenu.robot
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/study_structure/welcomeselected    \    ${True}
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/study_structure/welcomeinitial    welcome    ${True}
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/study_structure/missings    missings    ${True}
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/    general    ${False}



*** Keywords ***
User Views Studystructure Welcome
    Go To Studystructure
    Wait Until Page Contains Element    //*[contains(@id, 'structure_welcome')]
    Page Should Contain Element   //*[contains(@id, 'structure_welcome')]

User Selects Study
    [Arguments]     ${STUDY_NAME}
    Wait Until Page Contains Element    ${welcome_template_button_useStudy.format('${STUDY_NAME}')}
    Scroll Until Element Uncovered    ${welcome_template_button_useStudy.format('${STUDY_NAME}')}
    Click Button    ${welcome_template_button_useStudy.format('${STUDY_NAME}')}

Study Should Be Displayed
    [Arguments]    ${STUDY_NAME}
    Page Should Contain     ${STUDY_NAME}
    Page Should Contain Element    ${table_elements}

Department Should Be Displayed
    [Arguments]     ${DEPT_TECHNAME}
    Page Should Contain    ${DEPT_TECHNAME}
    Page Should Contain Element    //*[contains(text(),'${DEPT_TECHNAME}')]

Department Should Not Be Displayed
    [Arguments]     ${DEPT_TECHNAME}
    Page Should Not Contain    ${DEPT_TECHNAME}

User Enters A Department
    [Arguments]     ${DEPT_TECHNAME}
    Click Link    ${template_goToDept.format('${DEPT_TECHNAME}')}
    Wait Until Page Contains Element     ${template_button_breadcrumb.format('${DEPT_TECHNAME}')}
    Page Should Contain Element    ${template_button_breadcrumb.format('${DEPT_TECHNAME}')}


Variable Should Be Displayed
    [Arguments]     ${VAR_TECHNAME}
    Page Should Contain   ${VAR_TECHNAME}

Variable Should Not Be Displayed
    [Arguments]     ${VAR_TECHNAME}
    Page Should Not Contain   ${VAR_TECHNAME}

User Creates Missinglist And Enters Missingcodes
    [Arguments]     ${MISSINGLIST_NAME}    ${missingcodes}  ${jumpcodes}
    Click Button    ${button_editMissings}
    Wait Until Page Contains Element     ${missings_button_addMissinglist}
    Click Button    ${missings_button_addMissinglist}
    Wait Until Page Contains Element     ${missings_add_dropdown_parent}
    Click Element   ${missings_add_dropdown_parent}
    Click Element   ${missings_add_template_elementParent.format('${COHORT_NAME}')}
    Input Text      ${missings_add_input_name}    ${MISSINGLIST_NAME}
    Input Text      ${missings_add_input_description}    This is a generic description
    FOR    ${index}    ${code}    IN ENUMERATE    @{jumpcodes}
        Scroll Element In Div Into View    ${missings_add_input_missingcode}    //*[@id="scrollablebody"]
        Input Text    ${missings_add_input_missingcode}    ${code}
        Input Text    ${missings_add_input_namedInStudy}   ${{"Jumpcode${index}"}}
        Input Text    ${missings_add_input_typeName}    jump
        Click Button    ${missings_add_button_addMissingcode}
        Page Should Contain    ${code}
        Page Should Contain    ${index}
        No Error Should Be Notified
    END
    FOR    ${index}    ${code}    IN ENUMERATE    @{missingcodes}
        Scroll Element In Div Into View    ${missings_add_input_missingcode}    //*[@id="scrollablebody"]
        Input Text    ${missings_add_input_missingcode}    ${code}
        Input Text    ${missings_add_input_namedInStudy}   ${{"Missingcode${index}"}}
        Input Text    ${missings_add_input_typeName}    missing
        Click Button    ${missings_add_button_addMissingcode}
        Page Should Contain    ${code}
        Page Should Contain    ${index}
        No Error Should Be Notified
    END
    Click Button     ${missings_add_button_ok}
    No Error Should Be Notified
    Wait For Ajax Request
    No Error Should Be Notified
    Page Should Contain    ${MISSINGLIST_NAME}

User Deletes Missinglist
    [Arguments]     ${MISSINGLIST_NAME}
    Click Button    ${button_editMissings}
    Wait Until Page Contains Element     ${missings_button_addMissinglist}
    Click Element    ${general_template_btnDelete.format('${MISSINGLIST_NAME}')}
    Click First Interactable    ${general_button_ApproveDelete}
    Wait Until Page Contains Element     ${missings_button_addMissinglist}
    No Error Should Be Notified


Data Should Be Displayed
    [Arguments]     ${CSV_DATA}
    FOR    ${variable}   IN    @{CSV_DATA}
        Page Should Contain     ${variable}[VAR_NAMES]
    END

Create Department
    [Arguments]    ${techname}
    Click Button   ${button_addDept}
    Wait Until Page Contains Element    ${addept_input_techName}
    Scroll Element In Div Into View    ${addept_input_techName}    //*[@id="scrollablebody"]
    Input Text     ${addept_input_techName}   ${techname}
    Click Button   ${addept_button_add}

Create Variable
    [Arguments]     ${techname}    ${column}    ${dtype}    ${value_list}=    ${cancel}=True
    Wait Until Page Contains Element    ${button_addVar}
    Click Button    ${button_addVar}
    Wait Until Page Contains Element    ${addvar_input_techName}
    Scroll Element In Div Into View    ${addvar_input_techName}    //*[@id="scrollablebody"]
    Input Text      ${addvar_input_techName}    ${techname}
    Input Text      ${addvar_input_colNameDb}    ${column}
    Click Element    ${addvar_dropdown_datatype}
    Click Element    ${addvar_template_datatype.format('${dtype}')}
    Input Text      ${addvar_input_valueList}    ${value_list}
    Click Button    ${addvar_button_varaddAddVar}
    IF    ${cancel}
        Click Button    ${addvar_button_vareditCancel}
    END

Enter Data Into Variable Edit
    [Arguments]    ${data}
    Wait Until Page Contains Element    ${editvar_button_update}
    Scroll Element In Div Into View    ${editvar_template_input_translation.format('en')}    //*[@id='form:primodaldialog']
    Input Text    ${editvar_template_input_translation.format('en')}    ${data}[LABEL]
    IF    "${data}[LABEL]" == "PSEUDO_ID"
        Click Element    ${editvar_toggle_makeIdVar}
    END
    Scroll To Bottom    //*[@id="scrollablebody"]
    Click Element    ${editvar_dropdown_missinglist}
    Click Element    ${editvar_template_element_missinglist.format('${MISSINGLIST_NAME}')}
    Click Button    ${editvar_button_update}
    No Error Should Be Notified
    Wait Until Page Contains Element    ${editvar_button_backToOverview}
    Click Button    ${editvar_button_backToOverview}

Delete Element
    [Arguments]    ${techname}
    Click Button                 ${general_template_btnDelete.format('${techname}')}
    Click First Interactable     ${general_button_approveDelete}
    Wait Until Page Contains Element    css:.ui-messages-info
    No Error Should Be Notified

Enter Data
    [Arguments]    ${data}
    ${max}=    Evaluate    len($data)
    ${length}=    Evaluate    0
    FOR    ${index}    ${variable}   IN ENUMERATE    @{data}
        Log To Console    ${{"\b"*$length}}    no_newline=True
        Log To Console    ${index + 1}/${max}    no_newline=True
        ${length}=    Evaluate    len("{}/{}".format($index + 1,$max))
        Create Variable    ${variable}[VAR_NAMES]    ${variable}[VAR_NAMES]   ${variable}[DATA_TYPE]    ${variable}[VALUE_LABELS]    False
        Enter Data Into Variable Edit    ${variable}
        No Error Should Be Notified
    END
    Log To Console    ${{"\b"*$length}}    no_newline=True



