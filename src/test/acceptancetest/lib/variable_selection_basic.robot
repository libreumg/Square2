*** Settings ***
Documentation    Testing study structure functionality
...
...
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}
Library           SeleniumUtil.py
Library           csvdatareader.py
Resource          ${BASEDIR}/lib/variable_selection.robot

*** Keywords ***
Variable Selection Creation
    User Views Variable Selection Welcome
    Create Variable Selection    ${VARIABLESELECTION_NAME}    False
    Add Variables To Open Selection    ${VARIABLE_PATH}
    Wait Until Page Contains    ${VARIABLESELECTION_NAME}
    No Error Should Be Notified
    Variable Selection Should Be Displayed With Variables     ${VARIABLESELECTION_NAME}     ${VARIABLE_TESTNAME}
    No Error Should Be Notified

Variable Selection Deletion
    [Tags]   singlemodule
    User Views Variable Selection Welcome
    Delete Variable Selection    ${VARIABLESELECTION_NAME}
    Page Should Not Contain    ${VARIABLESELECTION_NAME}
    No Error Should Be Notified

Variable Selection Basic Setup
    Set Screenshot Directory    ${SCREENSHOTS}/variable_selection_basic
    Close All Browsers
    Import Variables    ${BASEDIR}/data/variable_selection.py
    Open Browser And Log In
