from robot.libraries.BuiltIn import BuiltIn
from selenium.webdriver.common.actions.action_builder import ActionBuilder
from selenium.webdriver.common.by import By
from selenium.common.exceptions import StaleElementReferenceException, JavascriptException


def click_first_interactable(xpath):
    sl = BuiltIn().get_library_instance("SeleniumLibrary")
    elems = sl.driver.find_elements(By.XPATH, xpath)
    for elem in elems:
        try:
            if elem.is_displayed() and elem.is_enabled():
                elem.click()
                return
        except StaleElementReferenceException:
            BuiltIn().log(f"Encountered stale Element while searching for xpath '{xpath}'. Ignoring.", level="DEBUG")


def move_mouse_to_position(pos_x, pos_y):
    sl = BuiltIn().get_library_instance("SeleniumLibrary")
    action = ActionBuilder(sl.driver)
    action.pointer_action.move_to_location(pos_x, pos_y)
    action.perform()


def scroll_element_in_div_into_view(elem_xpath, scrollable_xpath):
    sl = BuiltIn().get_library_instance("SeleniumLibrary")
    elem = sl.driver.find_element(By.XPATH, elem_xpath)
    scrollable = sl.driver.find_element(By.XPATH, scrollable_xpath)
    elem_id = elem.get_attribute("id")
    scrollable_id = scrollable.get_attribute("id")
    try:
        if not (elem_id == "" or scrollable_id == ""):
            sl.execute_javascript(f"document.getElementById('{scrollable_id}').scrollIntoView(document.getElementById('{elem_id}'));")
        else:
            # If selenium cannot find the element by ID, try to use JS for this, too. This part could be the whole
            # function if I was sure that it would always behave in the same way as the upper
            # which I am as of right now not, of course
            BuiltIn().log(f"Trying to scroll an element that has no id into view "
                          f"using less tested functionality for this", level="DEBUG")
            sl.execute_javascript(f"""
            var scrollable = document.evaluate("{scrollable_xpath}", document, null, 9);
            var elem = document.evaluate("{elem_xpath}", document, null, 9);
            scrollable.singleNodeValue.scrollIntoView(elem.singleNodeValue);
            """)
    except JavascriptException as e:
        BuiltIn().log(f"JavascriptException while scrolling element in div into view. "
                      f"Probably tried to scroll something without a scrollbar. Exception says: {e.msg}", level="DEBUG")
        BuiltIn().log_to_console(f"{scrollable_id}, {type(scrollable_id)}")


def scroll_until_element_uncovered(elem_xpath):
    sl = BuiltIn().get_library_instance("SeleniumLibrary")
    # CAUTION! Replaces "§§§" (first line) with the needed xpath - fstrings will obviously not work,
    # and I could not figure out how the seleniumlibrary built-in syntax for arguments translates into python.
    retval = sl.execute_javascript("""
    var elem = document.evaluate("§§§", document, null, 9).singleNodeValue;
    var scrollpos = 0;
    
    function getElemAtPoint(){
        var rect = elem.getBoundingClientRect();
        var centerX = rect.x + (rect.width/2);
        var centerY = rect.y + (rect.height/2);
        return document.elementFromPoint(centerX,centerY);
    }
    
    function checkPos(){
        if (!(getElemAtPoint(elem)  ==  undefined)){
            if (getElemAtPoint(elem) === elem) {
                return true;
            } else if (getElemAtPoint(elem).offsetParent === elem){
                return true;
            }
        } 
        return false;   
    }
    window.scrollTo(0, 0);
    if (!(checkPos())){
        while (scrollpos <= window.scrollMaxY) {
            window.scrollTo(0,scrollpos);
            if (checkPos()){
                break;
            }
            scrollpos += 10;
        }
    }
    return checkPos()
    """.replace("§§§", elem_xpath))
    if not retval:
        BuiltIn().log(f"Scrolling page until element with xpath {elem_xpath} was the uppermost element at its position"
                      f"did not succeed. This might be because one of its child elements was or "
                      f"because the scrolling routine failed disastrously. "
                      f"Check js in SeleniumUtil.py > scroll_until_element_uncovered", level="DEBUG")


def scroll_to_bottom(xpath):
    sl = BuiltIn().get_library_instance("SeleniumLibrary")
    elem = sl.driver.find_element(By.XPATH, xpath)
    elem_id = elem.get_attribute("id")
    sl.execute_javascript(
        f"document.getElementById('{elem_id}').scrollTo(0,document.getElementById('{elem_id}').scrollHeight)")
