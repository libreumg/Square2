import re
import json
import os

walked = os.walk("./xpaths")
for root, directories, files in walked:
    for file in files:
        data = json.load(open(root + "/" + file))
        for key in data:
            val = data[key]
            newval = re.sub(r"\$\{.*?\}", "{}", val)
            if newval != val:
                data[key] = newval
        json.dump(data, open(root + "/" + file, "w"), sort_keys=True, indent=4)
