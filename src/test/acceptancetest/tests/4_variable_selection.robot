*** Settings ***
Documentation    Testing variable selection functionality
...
...
Resource          ${BASEDIR}/lib/util.robot
Resource          ${BASEDIR}/lib/variable_selection_basic.robot
Resource          ${BASEDIR}/lib/variable_selection_rights_roles.robot


*** Test Cases ***
Variable Selection Basic
    [Setup]    Variable Selection Basic Setup
    Variable Selection Creation
#    Variable Selection Deletion
    [Teardown]   Log Out And Close Browser

Variable Selection Rights Roles
    [Setup]    Variable Selection Rights Roles Setup
    Variable Selection Rights Roles Test
    [Teardown]    Variable Selection Rights Roles Teardown