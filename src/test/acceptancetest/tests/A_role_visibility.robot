*** Settings ***
Documentation     Data driven testing if users with certain roles view the according top menu
...
...
Resource          ${BASEDIR}/lib/browser.robot
Resource          ${BASEDIR}/lib/util.robot
Resource          ${BASEDIR}/lib/topmenu.robot
Resource          ${BASEDIR}/lib/admin.robot
Resource          ${BASEDIR}/lib/role_visibility.robot
Test Template     Check Role Visibility
Suite Setup       Role Visibility Setup

*** Test Cases ***
Reportreader           ${{["reportreader"]}}      ${{{"qualityreports": True}}}       testperson    1234    Test
Vargroup               ${{["vargroup"]}}          ${{{"variableselection": True}}}    testperson    1234    Test
Studyadmin             ${{["studyadmin"]}}        ${{{"studyoverview": True}}}        testperson    1234    Test
Study                  ${{["study"]}}             ${{{"studystructure": True}}}       testperson    1234    Test
Analysis               ${{["analysis"]}}          ${{{"functionselection": True}}}    testperson    1234    Test
Reportbuilder          ${{["reportbuilder"]}}     ${{{"qualityreports": True}}}       testperson    1234    Test
Reportwriter           ${{["reportwriter"]}}      ${{{"qualityreports": True}}}       testperson    1234    Test
Reportdesigner         ${{["reportdesigner"]}}    ${{{"qualityreports": True}}}       testperson    1234    Test
Datamanagement         ${{["datamanagement"]}}    ${{{"datamanagement": True}}}       testperson    1234    Test
Statistic              ${{["statistic"]}}         ${{{"statistics": True}}}           testperson    1234    Test
Statisticadmin         ${{["statisticadmin"]}}    ${{{"statistics": True}}}           testperson    1234    Test
Shiptip                ${{["shiptip"]}}           ${{{}}}                             testperson    1234    Test
Shiptipadmin           ${{["shiptipadmin"]}}      ${{{}}}                             testperson    1234    Test
Admin                  ${{["admin"]}}             ${{{"admin": True}}}                testperson    1234    Test

Cumulative 1           ${{["reportreader", "vargroup", "admin"]}}                               ${{{"qualityreports": True, "variableselection": True, "admin": True}}}                testperson    1234    Test
Cumulative 2           ${{["reportreader", "datamanagement", "admin"]}}                         ${{{"qualityreports": True, "datamanagement": True, "admin": True}}}                   testperson    1234    Test
Cumulative 3           ${{["reportreader", "datamanagement", "admin", "shiptip"]}}              ${{{"qualityreports": True, "datamanagement": True, "admin": True}}}                   testperson    1234    Test
Cumulative 4           ${{["reportreader", "datamanagement", "reportwriter", "analysis"]}}      ${{{"qualityreports": True, "datamanagement": True, "functionselection": True}}}       testperson    1234    Test

