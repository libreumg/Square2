
## Environment Variables
These are all case sensitive!!
##### ROBOT_SERVER
Server where robot looks for Square2, if not set, default "localhost:8380" will be used.
##### ROBOT_TASKS
If not set, all suites in the tests folder will be executed. For running a single suite, for example "tests/study_structure.robot" would work
##### ROBOT_USER
Username for Square2 admin
##### ROBOT_PASSWORD
Password for Square2 admin
##### ROBOT_DEBUG
True or False, will set loglevel to TRACE and make robot run on a visible (non-headless) firefox instance. Also, the extra test cases required for singlemodule testing will be run.
##### ROBOT_SINGLE
True or False, will run Tests tagged singlemodule if set to "True". This makes test modules independent from the other modules and could lead to errors if not used in conjunction with ROBOT_TASKS
##### ROBOT_VARIABLES
Used to add custom Variables. Syntax: "Variable1:Value1,Variable2:Value2". Most important use case is when adding a Delay for being able to visibly debug better: "DELAY:0.5"
##### ROBOT_OPTIONS
Adding command line options to robot framework on a lower level. `--exclude=long` will skip more time-consuming tests

## Requirements

These tests need:
- an Instance of Square2 running on localhost:8380 (or the specified server)
- Username and Password of an admin user with all rights to all the required studies and all roles/privileges in the
  environment-variables ROBOT_USER and ROBOT_PASSWORD.
- the study data from data/study_data.csv in the psql database in a table robot_import.t_synthetic
- A set-up Cohort and Study called ROBOT.ROBOT-0
- Another Study in the ROBOT cohort called ROBOT-1
- A department called "rr_department1" in ROBOT-1 and a variable called "rr_variable1" in that department
- A variable selection called "rr_variableselection" that selects only the variable in ROBOT-1
- a configured user with username = "testperson", password = "1234" and full name = "Testimonie Testa" that has all roles and no specified rights. (For testing privileges - credentials can be changed in `data/rights_roles.py`)
- System python 3.8+ runnable by invoking "python3"
- Installed libraries from requirements.txt (python3 -m pip install -r requirements.txt)
- Newest Firefox installed (You can switch to chrome but expect it to be buggy)
- WebDriver for Firefox (command: "webdrivermanager gecko" - webdrivermanager comes with requirements.txt -  it seems to be important to have the newest geckodriver available this would be "webdrivermanager firefox:v0.31.0" )

## Tags

You can exclude or include different groups of tests by tag.
##### long
by running robot with ROBOT_OPTIONS="--exclude=long", long lasting tests (like loading a bunch of test data into study structure instead of just testing if we can add and delete variables etc.)
##### singlemodule
singlemodule-tagged tests are normally excluded. this can be

## Import Data

I ran:


```

CREATE SCHEMA robot_import;

CREATE TABLE robot_import.t_synthetic (
v00000 INTEGER, v00001 TEXT, v00002 INTEGER,
v00003 INTEGER, v00004 NUMERIC,v00005 NUMERIC, v01003 INTEGER,
v01002 INTEGER, v00103 TEXT, v00006 NUMERIC, 
v00007 INTEGER, v00008 TEXT, v00009 NUMERIC, v00109 INTEGER,
v00010 INTEGER, v00011 TEXT, v00012 TEXT,
v00013 TIMESTAMP WITHOUT TIME ZONE, v00014 NUMERIC, v00015 NUMERIC, v00016 INTEGER, 
v00017 TIMESTAMP WITHOUT TIME ZONE, v000018 INTEGER, v01018 INTEGER, 
v00019 INTEGER, v00020 INTEGER, v00021 INTEGER, v00022 INTEGER, 
v00023 INTEGER, v00024 INTEGER, v00025 INTEGER, v00026 INTEGER, 
v00027 INTEGER, v00028 INTEGER, v00029 INTEGER, v00030 INTEGER,
v00031 INTEGER,v00032 TEXT, v00033 TIMESTAMP WITHOUT TIME ZONE,
v00034 INTEGER, v00035 INTEGER, v00036 INTEGER, v00037 INTEGER,
v00038 INTEGER, v00039 INTEGER, v00040 INTEGER, v00041 INTEGER,
v00042 TIMESTAMP WITHOUT TIME ZONE, v10000 INTEGER, v20000 INTEGER,
v30000 INTEGER, v40000 INTEGER, v50000 INTEGER);

\copy robot_import.t_synthetic (
v00000, v00001, v00002, v00003,v00004, v00005, v01003,
v01002, v00103, v00006, v00007, v00008, v00009, v00109,
v00010, v00011, v00012, v00013, v00014, v00015, v00016, 
v00017, v000018, v01018, v00019, v00020, v00021, v00022, 
v00023, v00024, v00025, v00026, v00027, v00028, v00029, 
v00030, v00031, v00032, v00033, v00034, v00035, v00036, 
v00037, v00038, v00039, v00040, v00041, v00042, v10000, 
v20000, v30000, v40000, v50000)
FROM “/home/peschkee/Downloads/study_data.csv”
WITH (FORMAT CSV, DELIMITER ',',NULL 'NA', HEADER);

CREATE UNIQUE INDEX t_synthetic_id_key ON robot_import.t_synthetic (v00001);

ALTER TABLE robot_import.t_synthetic ADD CONSTRAINT t_synthetic_id_key UNIQUE USING INDEX t_synthetic_id_key;

```