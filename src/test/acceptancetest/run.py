import sys
import os
import robot
import time

def main():
    tasks = ['tests']
    server = "localhost:8380"
    basedir = os.getcwd()
    user = "test"
    password = "test"
    debug = "False"
    single = "False"
    test = []
    screenshotdir = f"{basedir}/Screenshots-{str(time.ctime()).replace(' ', '_').replace(':', '_')}"
    os.mkdir(screenshotdir)
    variables = ["--variable", f"SCREENSHOTS:{screenshotdir}"]

    # geckodriver seems to adjust the resolution according to these environment variables
    # os.environ["MOZ_HEADLESS_WIDTH"] = "1920"
    # os.environ["MOZ_HEADLESS_HEIGHT"] = "1080"

    if os.environ.get("ROBOT_SERVER"):
        server = os.environ.get("ROBOT_SERVER")
    if os.environ.get("ROBOT_TASKS"):
        tasks = str(os.environ.get("ROBOT_TASKS")).split(",")
    if os.environ.get("ROBOT_TEST"):
        test = ["-t", os.environ.get("ROBOT_TEST")]
    if os.environ.get("ROBOT_USER"):
        user = os.environ.get("ROBOT_USER")
    if os.environ.get("ROBOT_PASSWORD"):
        password = os.environ.get("ROBOT_PASSWORD")
    if os.environ.get("ROBOT_SINGLE"):
        single = os.environ.get("ROBOT_SINGLE")
    if os.environ.get("ROBOT_DEBUG"):
        debug = os.environ.get("ROBOT_DEBUG")
        if debug == "True":
            variables = variables + ["--loglevel", "TRACE"]
    if os.environ.get("ROBOT_VARIABLES"):
        tmp_variables = str(os.environ.get("ROBOT_VARIABLES")).split(",")
        for var in tmp_variables:
            variables = variables + ["--variable", var]
    if not debug == "True" and not single == "True":
        variables = variables + ["--exclude", "singlemodule"]

    print(f"Python {sys.version} is starting robot tests in {basedir} \n ! Using server: \"{server}\" \n Variables: {variables}")
    robot.run_cli(['--variable', f'SERVER:{server}', '--variable', f'BASEDIR:{basedir}', '--variable', f'USER:{user}',
                   '--variable', f'PASSWORD:{password}', "--variable", f"DEBUG:{debug}"] + variables + test + tasks, exit=False)

    if not os.listdir(screenshotdir):
        os.rmdir(screenshotdir)

    robot.rebot_cli(['output.xml'])


if __name__ == '__main__':
    main()
