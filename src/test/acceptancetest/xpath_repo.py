import json
import argparse
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException


"""very hacky and awful and slow way to find out if my xpath is correct"""
def validate_xpath_constructor(driver, xpath):
    try:
        driver.find_element(By.XPATH, xpath)
    except NoSuchElementException:
        return True
    return True


def main():
    options = webdriver.ChromeOptions()
    options.add_argument("--headless")
    driver = webdriver.Chrome(options=options)
    driver.get('https://google.com')

    def validate_xpath(xpath):
        return validate_xpath_constructor(driver, xpath)

    parser = argparse.ArgumentParser()
    parser.add_argument('--remove', action="store_const", const=True,  default=False)
    parser.add_argument('file', type=str)
    parser.add_argument('variables', type=str, nargs="+")

    args = parser.parse_args()
    path = args.file
    if not path.endswith(".json"):
        if path.endswith("/"):
            path = path + "elements.json"
        else:
            path = path + "/elements.json"
    vardict = json.load(open(path))

    if args.remove:
        print("Removing!")
        for var in args.variables:
            if var.split(":")[0] in vardict:
                del vardict[var.split(":")[0]]
    else:
        print("Adding!")
        for var in args.variables:
            key = var.split(":")[0]
            value = var.split(":")[1]
            if validate_xpath(value):
                vardict.update({key: value})
    print(f"{args.file} will look like this: ")
    print(vardict)
    if input("Are you sure? Y/N: ") in ["y", "Y", "yes", "Yes"]:
        with open(path, 'w') as f:
            json.dump(vardict, f, sort_keys=True, indent=4)


if __name__ == '__main__':
    main()

