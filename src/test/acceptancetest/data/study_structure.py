# Study Overview
COHORT_NAME = "ROBOT"
STUDY_NAME = "ROBOT-0"
STUDY_UNIQUE_NAME = "robot0"
DEPT_TECHNAME = "testdept"
VAR_TECHNAME = "testvar"
VAR_COLUMN = "thisisdefsarealcolumn"
VAR_DTYPE = "string"
DATA_DIRECTORY = "data/meta_data.csv"

MISSINGLIST_NAME = "testmissings"

