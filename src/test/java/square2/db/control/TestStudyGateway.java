package square2.db.control;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestStudyGateway {
	/**
	 * test
	 */
	@Test
	public void testTrimTo() {
		assertEquals(null, StudyGateway.trimTo(null, 32));
		assertEquals("this is crap", StudyGateway.trimTo("this is crap", 32));
		assertEquals("this is ...", StudyGateway.trimTo("this is crap", 11));
		assertEquals("this is crap", StudyGateway.trimTo("this is crap", 12));
	}
}
