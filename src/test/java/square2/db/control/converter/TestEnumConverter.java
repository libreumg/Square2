package square2.db.control.converter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import de.ship.dbppsquare.square.enums.EnumDatatype;

/**
 * 
 * @author henkej
 *
 */
public class TestEnumConverter {

  /**
   * test
   * 
   * @throws IOException on unexpected exceptions
   */
  @Test
  public void testToDatatypeEnum() throws IOException {
    try {
      EnumConverter.toDatatypeEnum("dummy");
    } catch (IOException e) {
      assertEquals("dummy is no well known enum", e.getMessage());
    }
    assertNull(EnumConverter.toDatatypeEnum(null));
    assertEquals(EnumDatatype.datetime, EnumConverter.toDatatypeEnum("datetime"));
    assertEquals(EnumDatatype.float_, EnumConverter.toDatatypeEnum("float"));
    assertEquals(EnumDatatype.integer, EnumConverter.toDatatypeEnum("integer"));
    assertEquals(EnumDatatype.string, EnumConverter.toDatatypeEnum("string"));
  }
}
