package square2.db.control;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import square2.modules.model.AclBean;

/**
 * 
 * @author henkej
 *
 */
public class TestReportGateway {
	/**
	 * test
	 * 
	 * @throws SQLException
	 *           for any error
	 */
	@Test
	public void testGetJsonMapFromString() throws SQLException {
		JsonElement json = new JsonParser().parse("[{\"usnr\": 123, \"acl\": \"rwx\"}, {\"usnr\": 987, \"acl\": \"rx\"}]");
		Map<Integer, AclBean> map = new ReportGateway(null).getJsonMapFromString(json);
		assertTrue(map.get(123).getRead());
		assertTrue(map.get(123).getWrite());
		assertTrue(map.get(123).getExecute());
		assertTrue(map.get(987).getRead());
		assertFalse(map.get(987).getWrite());
		assertTrue(map.get(987).getExecute());
	}
}
