package square2.db.control;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestAnalysisGateway {
	
	/**
	 * test
	 */
	@Test
	public void testCheckUniqueNames() {
		AnalysisGateway gw = new AnalysisGateway(null);
		List<String> list = new ArrayList<>();
		list.add("SHIP3.intro_cons_mrt");
		list.add("SHIP3.mrt_allg_auf");
		
		assertTrue(gw.checkUniqueNames(list, null));
		assertTrue(gw.checkUniqueNames(list, ""));
		assertTrue(gw.checkUniqueNames(list, "SHIP3.intro_cons_mrt"));
		assertTrue(gw.checkUniqueNames(list, "SHIP3.mrt_allg_auf"));
		assertTrue(gw.checkUniqueNames(list, "SHIP3.intro_cons_mrt,"));
		assertTrue(gw.checkUniqueNames(list, ",SHIP3.intro_cons_mrt"));
		assertTrue(gw.checkUniqueNames(list, "SHIP3.intro_cons_mrt,,"));
		assertTrue(gw.checkUniqueNames(list, "SHIP3.intro_cons_mrt,SHIP3.mrt_allg_auf"));
		assertFalse(gw.checkUniqueNames(list, "bla"));
		assertFalse(gw.checkUniqueNames(list, "bla,SHIP3.intro_cons_mrt"));
		assertFalse(gw.checkUniqueNames(list, "SHIP3.intro_cons_mrt,bla"));
	}
}
