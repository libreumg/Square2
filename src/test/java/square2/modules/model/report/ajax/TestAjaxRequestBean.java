package square2.modules.model.report.ajax;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import square2.json.gson.SmiJsonConverter;

/**
 * 
 * @author henkej
 *
 */
public class TestAjaxRequestBean {

  /**
   * test
   */
  @Test
  public void testCompareTo() {
    AjaxRequestBean bean1 = new AjaxRequestBean(new JsonBean(SmiJsonConverter.JSON_VERSION, null), "three", null, null, null, 4711d);
    AjaxRequestBean bean2 = new AjaxRequestBean(new JsonBean(SmiJsonConverter.JSON_VERSION, null), "one", null, null, null, 42d);
    AjaxRequestBean bean3 = new AjaxRequestBean(new JsonBean(SmiJsonConverter.JSON_VERSION, null), "two", null, null, null, 815d);
    List<AjaxRequestBean> list = new ArrayList<>();
    list.add(bean1);
    list.add(bean2);
    list.add(bean3);
    list.sort((l1, l2) -> l1 == null ? 0 : l1.compareTo(l2));
    
    assertNotNull(list);
    assertEquals(3, list.size());
    assertNotNull(list.get(0));
    assertNotNull(list.get(1));
    assertNotNull(list.get(2));
    assertEquals("one", list.get(0).getCardinality());
    assertEquals("two", list.get(1).getCardinality());
    assertEquals("three", list.get(2).getCardinality());
  }
}
