package square2.modules.model.statistic;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestSerializables {
	private FunctionBean bean;

	/**
	 * prepare all tests
	 */
	@BeforeEach
	public void setUp() {
		FunctionInputBean input1 = new FunctionInputBean(null);
		input1.setName("crap");
		input1.setFkType(new FunctioninputtypeBean(42, null, null));

		FunctionBean reference = new FunctionBean(4711, null, true, null);

		bean = new FunctionBean(42, null, true, null);
		bean.setActivated(true);
		bean.getInput().add(input1);
		bean.getReference().add(reference);

		reference.getReference().add(bean);
	}

	private FunctionBean xstream(FunctionBean bean) {
		return bean;
		// return (FunctionBean) new XStream().fromXML(new XStream().toXML(bean));
	}

	/**
	 * test serializing function bean
	 * 
	 * @throws IOException
	 *           for any error
	 * @throws ClassNotFoundException
	 *           for any error
	 */
	@Test
	public void test() throws IOException, ClassNotFoundException {
		bean = xstream(bean);

		assertEquals(Integer.valueOf(42), bean.getRight());
		assertTrue(bean.getActivated());
		assertTrue(bean.getInput().size() > 0);
		assertTrue(bean.getReference().size() > 0);
		assertTrue(bean.getReference().get(0).getReference().size() > 0);

		FunctionInputBean inputFound1 = bean.getInput().get(0);
		assertEquals(Integer.valueOf(42), inputFound1.getFkType().getPk());

		FunctionBean referenceFound1 = bean.getReference().get(0);
		assertEquals(Integer.valueOf(4711), referenceFound1.getRight());

		FunctionBean referencedReferenceFound1 = referenceFound1.getReference().get(0);
		assertEquals(Integer.valueOf(42), referencedReferenceFound1.getRight());
	}
}
