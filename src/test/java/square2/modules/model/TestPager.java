package square2.modules.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestPager {

	/**
	 * test
	 */
	@Test
	public void testGetPageOfItem() {
		Pager pager = new Pager();
		pager.setOffset(1);
		assertEquals(Integer.valueOf(0), pager.getPageOfItem(0));
		pager.setPagelength(4);
		assertEquals(Integer.valueOf(1), pager.getPageOfItem(0));
		assertEquals(Integer.valueOf(1), pager.getPageOfItem(1));
		assertEquals(Integer.valueOf(1), pager.getPageOfItem(2));
		assertEquals(Integer.valueOf(1), pager.getPageOfItem(3));
		assertEquals(Integer.valueOf(1), pager.getPageOfItem(4));
		assertEquals(Integer.valueOf(2), pager.getPageOfItem(5));
		assertEquals(Integer.valueOf(2), pager.getPageOfItem(6));
		assertEquals(Integer.valueOf(2), pager.getPageOfItem(7));
		assertEquals(Integer.valueOf(2), pager.getPageOfItem(8));
		assertEquals(Integer.valueOf(3), pager.getPageOfItem(9));
	}
}
