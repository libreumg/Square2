package square2.help;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestTypeRestrictionHelper {

	/**
	 * test the findBoolean method
	 */
	@Test
	public void testFindBoolean() {
		assertFalse(TypeRestrictionHelper.findBoolean("", "k"));
		assertFalse(TypeRestrictionHelper.findBoolean("{}", "k"));
		assertFalse(TypeRestrictionHelper.findBoolean("[]", "k"));
		assertFalse(TypeRestrictionHelper.findBoolean("{\"a\":1}", "k"));
		assertFalse(TypeRestrictionHelper.findBoolean("[\"a\"]", "k"));
		assertFalse(TypeRestrictionHelper.findBoolean("[1, 2]", "k"));
		assertFalse(TypeRestrictionHelper.findBoolean("[\"k\"]", "k"));
		assertFalse(TypeRestrictionHelper.findBoolean("{\"k\": null}", "k"));
		assertFalse(TypeRestrictionHelper.findBoolean("{\"k\": false}", "k"));
		assertTrue(TypeRestrictionHelper.findBoolean("{\"k\": true}", "k"));
		assertTrue(TypeRestrictionHelper.findBoolean("{\"k\": true}", "k"));
		assertTrue(TypeRestrictionHelper.findBoolean("{\"a\": 1, \"k\": true}", "k"));
		assertTrue(TypeRestrictionHelper.findBoolean("{\"a\": 1, \"k\": [true]}", "k"));
		assertFalse(TypeRestrictionHelper.findBoolean("{\"a\": 1, \"k\": [true, false]}", "k"));
	}
	
	/**
	 * test the findInteger method
	 */
	@Test
	public void testFindInteger() {
		assertNull(TypeRestrictionHelper.findInteger("", "x"));
		assertNull(TypeRestrictionHelper.findInteger("{}", "x"));
		assertNull(TypeRestrictionHelper.findInteger("[]", "x"));
		assertNull(TypeRestrictionHelper.findInteger("{\"a\":1}", "x"));
		assertNull(TypeRestrictionHelper.findInteger("[\"a\"]", "x"));
		assertNull(TypeRestrictionHelper.findInteger("[1, 2]", "x"));
		assertNull(TypeRestrictionHelper.findInteger("[\"x\"]", "x"));
		assertNull(TypeRestrictionHelper.findInteger("{\"x\": null}", "x"));
		assertEquals(42, TypeRestrictionHelper.findInteger("{\"x\": 42}", "x"));
		assertEquals(42, TypeRestrictionHelper.findInteger("{\"y\": 4711, \"x\": 42}", "x"));
		assertEquals(42, TypeRestrictionHelper.findInteger("{\"x\": [42]}", "x"));
		assertNull(TypeRestrictionHelper.findInteger("{\\\"x\\\": [42, 4711]}", "x"));
	}
	
	/**
	 * test the findRange method
	 */
	@Test
	public void testFindRange() {
		assertEquals("-∞ - ∞", TypeRestrictionHelper.findRange("", "a", "b"));
		assertEquals("-∞ - ∞", TypeRestrictionHelper.findRange("{}", "a", "b"));
		assertEquals("-∞ - ∞", TypeRestrictionHelper.findRange("[]", "a", "b"));
		assertEquals("1 - ∞", TypeRestrictionHelper.findRange("{\"a\":1}", "a", "b"));
		assertEquals("-∞ - ∞", TypeRestrictionHelper.findRange("[\"a\"]", "a", "b"));
		assertEquals("-∞ - ∞", TypeRestrictionHelper.findRange("[1, 2]", "a", "b"));
		assertEquals("-∞ - ∞", TypeRestrictionHelper.findRange("[\"x\"]", "a", "b"));
		assertEquals("-∞ - ∞", TypeRestrictionHelper.findRange("{\"a\": null}", "a", "b"));
		assertEquals("-∞ - 1", TypeRestrictionHelper.findRange("{\"b\": 1}", "a", "b"));
		assertEquals("0 - 100", TypeRestrictionHelper.findRange("{\"b\": 100, \"a\": 0}", "a", "b"));
	}	
}