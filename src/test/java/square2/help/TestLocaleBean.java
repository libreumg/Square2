package square2.help;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import square2.modules.model.LangKey;
import square2.modules.model.LocaleBean;

/**
 * 
 * @author henkej
 *
 */
public class TestLocaleBean {
	/**
	 * test
	 */
	@Test
	public void testGetTranslation() {
		Map<String, String> map = new HashMap<>();
		map.put(LangKey.generateKey(42, null), "only the number");
		map.put(LangKey.generateKey(null, "name.only"), "only the name");
		map.put(LangKey.generateKey(7, "name.seven"), "seven");
		assertEquals("only the number", LocaleBean.getTranslation(map, 42, null));
		assertEquals("only the name", LocaleBean.getTranslation(map, null, "name.only"));
		assertEquals("seven", LocaleBean.getTranslation(map, 7, "name.seven"));
		assertEquals("the.key", LocaleBean.getTranslation(map, null, "the.key"));
		assertEquals("translation.key.5", LocaleBean.getTranslation(map, 5, null));
		assertEquals("false", LocaleBean.getTranslation(map, 5, "false"));
	}
}
