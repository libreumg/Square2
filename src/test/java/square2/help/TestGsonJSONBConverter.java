package square2.help;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.jooq.JSONB;
import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestGsonJSONBConverter {
	/**
	 * test the getKey method
	 */
	@Test
	public void testGetKey() {
		GsonJSONBConverter c = new GsonJSONBConverter(null);
		assertNull(c.get("de_DE"));
		
		c = new GsonJSONBConverter(JSONB.valueOf("{\"de_DE\": \"deutsche Übersetzung\", \"en\": \"English translation\"}"));
		assertEquals("deutsche Übersetzung", c.get("de_DE"));
		assertEquals("English translation", c.get("en"));
		assertNull(c.get("ro"));
	}
}
